// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCPFN1kB5u-duklcRS2Nns9j6wGDYmuhhU",
    authDomain: "futurestarr-174216.firebaseapp.com",
    databaseURL: "https://futurestarr-174216.firebaseio.com",
    projectId: "futurestarr-174216",
    storageBucket: "futurestarr-174216.appspot.com",
    messagingSenderId: "774989409251"
 }
};
// export const environment = {
//   production: false,
//   firebase: {
//     apiKey: "AIzaSyCge6GUw9_FUMc2_y9esTAHiUYNWR7-dXw",
//     authDomain: "cryptic-portal-216507.firebaseapp.com",
//     databaseURL: "https://cryptic-portal-216507.firebaseio.com",
//     projectId: "cryptic-portal-216507",
//     storageBucket: "cryptic-portal-216507.appspot.com",
//     messagingSenderId: "128701184462"
//  }
// };

