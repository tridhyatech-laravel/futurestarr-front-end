import { Component, OnInit } from '@angular/core';
import {  RouterModule, Router, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized } from '@angular/router';

@Component({
  selector: 'app-buyer-layout',
  templateUrl: './buyer-layout.component.html',
  styleUrls: ['./buyer-layout.component.css']
})
export class BuyerLayoutComponent implements OnInit {

  public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
  public buyerPassword = false;

  constructor(public router:Router) {
    // console.log(this.currentUser);

    router.events.subscribe((val) => {
      this.checkUrl(val);
    });
  }

  ngOnInit() {
  //  console.log("@@@@@@@@@@@@@");
    //console.log(this.router);

    if (this.router.url === '/buyer') {
      this.router.navigate(['/buyer/dashboard']);
    }
  }

  checkUrl(val){
    this.buyerPassword = false;
  //  console.log(val.url);
    if(val.url === '/buyer/change-password-buyer' ){
    //  console.log("buyer change password..")
      this.buyerPassword = true;
    }
  }

}
