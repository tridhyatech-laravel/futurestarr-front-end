import { Component, OnInit } from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';

declare var $;

@Component({
  selector: 'app-refund-policy',
  templateUrl: './refund-policy.component.html',
  styleUrls: ['./refund-policy.component.css']
})
export class RefundPolicyComponent implements OnInit {

  constructor(private _location: Location) { }

  ngOnInit() {
    this.topFunction();
  }

  goBack(){
    this._location.back();
  }

  topFunction() {
    $('html, body').animate({ scrollTop: 0 }, 'fast');
  }

}
