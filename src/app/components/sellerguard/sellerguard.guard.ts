import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

@Injectable()
export class SellerguardGuard implements CanActivate {

  public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));

  constructor(public router : Router) { }

  canActivate() {
      if(this.currentUser && this.currentUser.role_id == 'seller'){
        return true;
      } 
      else{
        this.router.navigate(['/']);
      }
  }
}
