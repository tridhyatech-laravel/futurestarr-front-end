import { TestBed, async, inject } from '@angular/core/testing';

import { SellerguardGuard } from './sellerguard.guard';

describe('SellerguardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SellerguardGuard]
    });
  });

  it('should ...', inject([SellerguardGuard], (guard: SellerguardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
