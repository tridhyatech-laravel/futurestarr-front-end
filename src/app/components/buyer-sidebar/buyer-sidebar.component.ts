import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../services/shared.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-buyer-sidebar',
  templateUrl: './buyer-sidebar.component.html',
  styleUrls: ['./buyer-sidebar.component.css']
})

export class BuyerSidebarComponent implements OnInit {

  public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
  public TotalPurchsedProduct:any={};
  public latestImage;

  constructor(public sharedService : SharedService,
              public toastr:ToastrService,
              public router:Router
            )
  {
        this.getBuyerPurchaseItem()

        var temp = sharedService.imagePathAnnounced$.subscribe(res => {
            this.getBuyerPurchaseItem();
          });

      
  }

  ngOnInit() {
    // this.toastr.su

  }

  getBuyerPurchaseItem() {
      this.sharedService.getBuyerPurchaseItems().then(res => {

          this.TotalPurchsedProduct = res;
          this.latestImage = this.TotalPurchsedProduct.profile_pic;
            // console.log("success: " + JSON.stringify(this.TotalPurchsedProduct));
            console.log(this.TotalPurchsedProduct);

      }).catch(error => {
          // console.log("error value: " + error);
      });
  }

}
