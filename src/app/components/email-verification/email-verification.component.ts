import { Component, OnInit, ViewContainerRef } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { SharedService } from '../../services/shared.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-email-verification',
  templateUrl: './email-verification.component.html',
  styleUrls: ['./email-verification.component.css']
})

export class EmailVerificationComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute,
              public sharedService:SharedService,
              public router: Router,
              public toastr:ToastrService) {
  }

  ngOnInit() {
     // subscribe to router event
     this.activatedRoute.params.subscribe((params: Params) => {
         let token = params['token'];
         this.verifyUser(token);
        //  console.log(token);
       });
   }

   verifyUser(token){
     this.sharedService.verifyUserEmail(token).then(user_email =>{
      // console.log("below user to send thank you mail");
      // console.log(user_email);
      this.router.navigate(['']);
      this.getSuccess();
      this.thankMail(user_email);
    }).catch(error =>{
      // console.log(error._body);
        this.router.navigate(['']);
        this.getError();
    });
   }

   getSuccess(){
       this.toastr.success('You have been verified successfully!', 'Success!');
   }
   getError(){
     this.toastr.error('Link got expired!', 'Oops!');
   }

   thankMail(user_email){
       this.sharedService.sendThankMail(user_email).then(result =>{
        //  console.log(result);
       }).catch(error =>{
        //  console.log(error);
       });
   }
}
