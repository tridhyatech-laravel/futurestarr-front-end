import { Component, OnInit, PLATFORM_ID, Inject } from '@angular/core';
import { Router, RouterLinkActive } from '@angular/router';
import { LoginService } from '../../modules/login/service/login.service';
import { Location } from '@angular/common';
import { SharedService } from '../../services/shared.service';
import { SellerSidebarService } from '../seller-sidebar/services/seller-sidebar.service';
import { LoadingModule } from 'ngx-loading';
import { AuthService, FacebookLoginProvider, GoogleLoginProvider, LinkedinLoginProvider } from 'ng-social-login-module';
// import * as $ from 'jquery';

import { isPlatformBrowser } from '@angular/common';

declare var jquery: any;
declare var $: any;

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    public user: any;
    public cartCount;
    public showSearchInput = false;
    public query = '';
    public searchResult;
    public searchTalentsCount = 0;
    public searchCategoriesCount = 0;
    public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
    public checkUrlName;
    public loading: boolean = false;

    constructor(public router: Router,
        public loginService: LoginService,
        public location: Location,
        public sharedService: SharedService,
        private authService: AuthService,
        @Inject(PLATFORM_ID) private platform: Object,
        public sellerSidebarService: SellerSidebarService) {
        var temp = sharedService.askToLogInAnnounced$.subscribe(res => {
            // console.log(res);
            this.sharedService.askToPreserveUrl(res);
            var showLoginBox = document.getElementById('showLoginBox');
            showLoginBox.click();
        });
        var temp1 = sharedService.cartItemDeletedAnnounced$.subscribe(res => {
            this.getCartCount();
        });
        var temp2 = sharedService.logOutAnnounced$.subscribe(res => {
            this.logoutUser();
        });

        router.events.subscribe((val) => {
            this.checkUrl(val);
        });


        var temp = sharedService.usernameAnnounced$.subscribe(res => {
            this.getCurrentUser1();
        });

        this.getCurrentUser();
    }

    ngOnInit() {
        if (isPlatformBrowser(this.platform)) {

            $(document).on("scroll", function () { // check if scroll event happened
                if ($(document).scrollTop() > 10) { // check if user scrolled more than 50 from top of the browser window
                    $(".navbar-fixed-top").css("background-color", "#151829"); // if yes, then change the color of class "navbar-fixed-top" to white (#f8f8f8)
                } else {
                    $(".navbar-fixed-top").css("background-color", "transparent"); // if not, change it back to transparent
                }
            });

            // $('.navbar-collapse a').click(function (e) {
            //     $('.navbar-collapse').collapse('toggle');
            // });

            if (this.currentUser) {
                this.getCartCount();
            }
        }
    }

    openDashboard() {
        if (isPlatformBrowser(this.platform)) {
            $(".navbar-collapse").collapse('hide');
            if (this.user && this.user[0].role == 'seller') {
                this.router.navigate(['/seller/dashboard']);
            }
            if (this.user && this.user[0].role == 'buyer') {
                this.router.navigate(['/buyer/dashboard']);
            }
        }
    }

    closeMenu() {
        if (isPlatformBrowser(this.platform)) {
            $(".navbar-collapse").collapse('hide');
        }
    }

    getUrl(re) {

    }

    checkUrl(val) {
        this.checkUrlName = val.url;
    }

    getCurrentUser() {

        if (this.currentUser == null) {
            // this.router.navigate(['/']);
        } else {
            this.loading = true;
            this.sellerSidebarService.sellerProfile().then(res => {
                this.user = res;
                this.loading = false;
            }).catch(error => {
                this.loading = false;

            });
        }
    }

    getCurrentUser1() {
        this.loading = true;
        this.sellerSidebarService.sellerProfile().then(res => {
            this.user = res;
            //this.latestImage = this.user[0].profile_pic;
            this.loading = false;
        }).catch(error => {
            this.loading = false;

        });
    }



    //socialLogin() {
    //    this.loginService.socialLogin().then(res => {
    //       console.log("social login API: " + JSON.stringify(res));
    //    }).catch(error => {
    //        console.log("error value: " + error);
    //    });
    //}

    getCartCount() {
        this.loading = true;
        var user_id = this.currentUser.id;
        this.sharedService.getCartCount(user_id).then(res => {
            this.cartCount = res;
            this.loading = false;
        }).catch(error => {
            this.loading = false;
        });
    }

    logoutUser() {
        this.loginService.logout().then(res => {

            // if (this.currentUser.token == null) {
            localStorage.removeItem('currentUser');
            this.router.navigateByUrl('/');
            location.reload();
            // } else {
            //     localStorage.removeItem('currentUser');
            //     this.router.navigate(['/']);
            // }
        }).catch(error => {
            // console.log("error value: " + error);
            // if (this.currentUser == null || error) {
            localStorage.removeItem('currentUser');
            this.router.navigateByUrl('/');
            location.reload();
            // }
        });
    }

    activeSearchBar() {
        this.showSearchInput = true;
    }

    getSearchResults(query) {
        this.query = query;
        this.sharedService.getSearchResults(query).then(res => {
            this.searchResult = res;
            this.searchTalentsCount = this.searchResult.talents.length;
            this.searchCategoriesCount = this.searchResult.categories.length;

        }).catch(error => {

        });

    }

    onClick() {

    }
}
