import { Component, OnInit, PLATFORM_ID, Inject } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

declare var $: any;


@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {

  constructor( @Inject(PLATFORM_ID) private platform: Object,) { }

  ngOnInit() {
    if (isPlatformBrowser(this.platform)) {
    $(".navbar-fixed-top").css("background-color", "#151829");
    }
  }

}
