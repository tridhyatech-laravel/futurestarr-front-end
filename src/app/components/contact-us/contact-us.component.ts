import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedService } from '../../services/shared.service';
import { ToastrService } from 'ngx-toastr';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Meta, Title } from '@angular/platform-browser';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
declare var jquery: any;
declare var $: any;
declare var google:any;



@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  lat: number = 33.958501;
  lng: number = -84.304254;
  title: string = "FUTURESTARR MEDIA LLC.";
  public successFlag: boolean = false;
  public body: any = {};
  public user_name;
  public loading = false;

  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;

  constructor(public router: Router,
    public sharedService: SharedService,
    public toastr: ToastrService,
    private meta: Meta, private title1: Title,
    private _location: Location)
    {
      
    // this.title1.setTitle("Contact Us");

    // this.meta.updateTag({ name: 'author', content: 'Future Starr' });
    // this.meta.updateTag({ name: 'keyword', content: 'Contact US' });
    // this.meta.updateTag({ name: 'description', content: 'Contact Us Futuer Starr' });
  }

  contactQuery(body) {
    this.loading = true;
    this.body = body.value;
    this.sharedService.sendContactInfo(this.body).then(res => {
      console.log("send contact");
      this.loading = false;
      // this.successFlag = true;
      // this.user_name = this.body.name;
      // body.reset();
      this.toastr.success('Form has successfully submtted.', 'Success!');
      this.router.navigate(['/thank-you'])
      // this.setTimeOut();
    }).catch(error => {
      this.loading = false;
      this.toastr.error('Connection Error' + '!', 'Oops!');
    });
  }
  ngOnInit() {

var myLatLng = {lat: 33.958501, lng:-84.304254};

    var map = new google.maps.Map(this.gmapElement.nativeElement, {
        zoom: 16,
        center: myLatLng
      });
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'FUTURESTARR MEDIA LLC.'
      });

    this.title1.setTitle("Contact Us");
    this.meta.updateTag({ name: 'author', content: 'Future Starr' });
    this.meta.updateTag({ name: 'keyword', content: 'Contact US' });
    this.meta.updateTag({ name: 'description', content: 'Contact Us Futuer Starr' });


  }


  topFunction() {
    $('html, body').animate({ scrollTop: 0 }, 'fast');
  }

  goBack() {
    this._location.back();
  }

  // setTimeOut() {
  //   var t = setTimeout(() => {
  //     this.successFlag = false;
  //   }, 15000);
  // }


}