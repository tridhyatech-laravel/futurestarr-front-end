import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuardUserService {
  public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
  constructor(public router : Router) { 

  }

  canActivate() {
    if(this.currentUser != null && this.currentUser.role_id == 'buyer'){
      return true;
    } 
    else{
      this.router.navigate(['/']);
    }
    
  }
}
