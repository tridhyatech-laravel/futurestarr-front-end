import { Component, OnInit } from '@angular/core';
import { Location, isPlatformBrowser } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-thankyoucontact',
  templateUrl: './thankyoucontact.component.html',
  styleUrls: ['./thankyoucontact.component.css']
})
export class ThankyoucontactComponent implements OnInit {

  constructor(private _location: Location,) { }

  ngOnInit() {
    
  }
  goBack() {
    this._location.back();
  }
  // topFunction() {
  //   if (isPlatformBrowser(this.platform)) 
  //   $('html, body').animate({ scrollTop: 0 }, 'fast');
  // }


}
