import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThankyoucontactComponent } from './thankyoucontact.component';

describe('ThankyoucontactComponent', () => {
  let component: ThankyoucontactComponent;
  let fixture: ComponentFixture<ThankyoucontactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThankyoucontactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThankyoucontactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
