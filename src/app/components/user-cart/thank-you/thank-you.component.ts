import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { SharedService } from '../../../services/shared.service';
import { Router } from '@angular/router';


declare var $: any;

@Component({
  selector: 'app-thank-you',
  templateUrl: './thank-you.component.html',
  styleUrls: ['./thank-you.component.css']
})
export class ThankYouComponent implements OnInit {

  public redirect;
  public loading = false;
  public thankyou_shopping;
  public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));

  constructor(private _location: Location, public sharedService: SharedService, public router: Router) { }

  ngOnInit() {
    this.redirect = false;
    //  this.thankyou_shopping = false;
    this.setTimeOut();
    // console.log(this.router.url);
    $(".navbar-fixed-top").css("background-color", "#151829");
    if (this.router.url === '/cart/thankyou') {
      // console.log('inside yrl');
    }
  }

  setTimeOut() {
    var t = setTimeout(() => {
      this.redirect = true;
      this.setTimeOut2();
    }, 3000);
  }

  setTimeOut2() {
    var t = setTimeout(() => {
      this.redirect = false;
      // this.thankyou_shopping = true;
      // this.router.navigate(['/']);
      this.router.navigate(['/buyer']);
    }, 2000);
  }


}
