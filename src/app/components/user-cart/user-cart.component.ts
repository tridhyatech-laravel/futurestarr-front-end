import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { SharedService } from '../../services/shared.service';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-user-cart',
  templateUrl: './user-cart.component.html',
  styleUrls: ['./user-cart.component.css']
})
export class UserCartComponent implements OnInit {

  public cartItems: any = {};
  public loading = false;
  public itemLength: number = 0;
  public thankyou_shopping;
  public redirect;
  public pay: any = {};
  public payLink: any = {};
  public editCart: any = {};
  public cart: any = {};
  public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
  constructor(private _location: Location, public sharedService: SharedService, public router: Router) {
  }

  ngOnInit() {
    this.getItems(this.currentUser.id);
    this.thankyou_shopping = false;
    this.redirect = false;
    this.topFunction();
  }

  openCheckoutModal() {
    $('#checkoutModal').modal({ backdrop: 'static' });
  }

  goBack() {
    this._location.back();
  }

  topFunction() {
    $('html, body').animate({ scrollTop: 0 }, 'fast');
  }

  isImage(new_product_img_path){
    let images = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'GIF', 'gif'];
    if (new_product_img_path)
    {
      let fileExtension = new_product_img_path.replace(/^.*\./, '');
      if( images.includes(fileExtension)){
        return true;
      }
    }
  }

  getItems(user_id) {
    this.loading = true;
    this.sharedService.getCartItems(user_id).then(res => {
      this.cartItems = res;
      // console.log(this.cartItems);
      this.cart = this.cartItems.carts;
      this.itemLength = this.cartItems.items.length;
      this.sharedService.announcecartItem();
      this.loading = false;
    }).catch(error => {
      // console.log("error value: " + error);
    });
  }

  deleteItem(talentId) {
    this.loading = true;
    this.sharedService.deleteCartItems(talentId).then(res => {
      this.sharedService.announcecartItem();
      this.loading = false;
      this.getItems(this.currentUser.id);
    }).catch(error => {
      // console.log("error value: " + error);
    });
  }

  changeQuantity(itemId, flag, qty, price, cart_id) {
    // console.log(this.cart);
    // console.log(flag);
    if (flag == true) {
      qty++;
    }
    else {
      qty--;
    }
    this.editCart.id = itemId;
    this.editCart.qty = qty;
    this.editCart.price = price;
    this.editCart.cart_id = cart_id;
    this.editCart.user_id = this.currentUser.id;
    this.sharedService.editQuantity(this.editCart).then(res => {
      this.sharedService.announcecartItem();
      // this.loading = false;
      this.getItems(this.currentUser.id);
    }).catch(error => {
      // console.log("error value: " + error);
    });
  }

  checkedOut() {
    this.pay.amount = this.cart.total_amount;
    this.pay.quantity = this.cart.quantity;
    this.pay.title = this.cart.title;
    // console.log(this.cartItems.items);

    // let price_dist = []
    //  this.cartItems.items.forEach(element => {
    //    let ownerPrice = ((element.price*25)/100);
    //    let sellerPrice = ((element.price*75)/100);
    //    let arr = {
    //      id: element.productId,
    //      admin : ownerPrice,
    //      seller_id : "",
    //      seller_amount : sellerPrice
    //    }
    //   price_dist.push(arr);
    // });


    // this.sharedService.payMoneyByPayPal(this.pay).then(res => {
    //     this.payLink = res;
    //     console.log("payment linbk ???????" + this.payLink);
    //     window.open(this.payLink, "_blank");
    //   this.sharedService.announcecartItem();
    //   this.loading = false;
    //   $('#checkoutModal').modal('toggle');
    //   this.router.navigate(['/cart/thankyou']);
    //   this.thankyou_shopping = true;
    //   this.getItems(this.currentUser.id);
    // }).catch(error => {
    //   console.log("error value: " + error);
    // });
    this.loading = true;
    this.sharedService.processToCheckout(this.cartItems.items).then( data=>{
      this.loading = false;
      if(data['success']){
        document.location.href = data['redirect_url'];
      }
      else{
        console.log(data['message']);
      }
    })
    .catch( error=>{
      this.loading = false;
      console.log(error);
    })
    // this.loading = true;

    // let dataToSend = {
    //   items: this.cartItems.items,
    // }
    // document.location.href = "https://www.futurestarr.com/paypal/package_three.php?items=" + JSON.stringify(dataToSend);
    // document.location.href = "http://localhost/paypal/package_three.php?items=" + JSON.stringify(dataToSend);
  }

  setTimeOut() {
    var t = setTimeout(() => {
      this.redirect = true;
      this.setTimeOut2();
    }, 4000);
  }

  setTimeOut2() {
    var t = setTimeout(() => {
      this.redirect = false;
      this.thankyou_shopping = false;
      this.router.navigate(['/']);
    }, 2000);
  }



}
