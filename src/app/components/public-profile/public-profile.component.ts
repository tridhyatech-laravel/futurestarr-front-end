import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap,RouterLinkActive } from '@angular/router';
import { SharedService } from '../../services/shared.service';
import { Title, Meta } from '@angular/platform-browser';

declare var $: any;

@Component({
  selector: 'app-public-profile',
  templateUrl: './public-profile.component.html',
  styleUrls: ['./public-profile.component.css']
})
export class PublicProfileComponent implements OnInit {
  sub:any;
  user_id;any;
  user_profile:any;
  talents:any;
  public p: number = 1;

  constructor(private _location: Location,private router:Router,public sharedService:SharedService, private route: ActivatedRoute,private titleService: Title, public meta: Meta) {
    // if(!sharedService.currentUser){
    //   this.router.navigate(['/']);
    // }
    // this.sub = this.route.params.subscribe(params => {
    //   this.user_id = params['user_id'];
    //   console.log(this.user_id);
      this.getProfileInfo(1);
    // });
  }

  ngOnInit() {
    this.titleService.setTitle("public profile page");
    this.meta.updateTag({ name: 'description', content: "public profile pagecontent" });
  }

  getProfileInfo(user_id){
    this.sharedService.getProfileInfo(user_id).then(data=>{
      if(data['success']){
        this.user_profile = data['user_profile'];
        this.talents = data['talents'];
      }
      else{
        this.router.navigate(['/']);
      }
    })
    .catch(error=>{
      console.log(error);
      this.router.navigate(['/']);
    })
  }

  showVideoControls(productid){
    let videoTagId = document.getElementById("video"+productid);
    if(!videoTagId.hasAttribute("controls")){
      videoTagId.setAttribute("controls","controls") 
    }
  }

  hideVideoControls(productid){
    let videoTagId = document.getElementById("video"+productid);
    if(videoTagId.hasAttribute("controls")){
      videoTagId.removeAttribute("controls");   
    }
  }

  goBack() {
    this._location.back();
  }

  topFunction() {
      $('html, body').animate({ scrollTop: 0 }, 'fast');
  }

}
