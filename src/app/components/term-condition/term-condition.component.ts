import { Component, OnInit } from '@angular/core';

import {Location} from '@angular/common';

declare var $;

@Component({
  selector: 'app-term-condition',
  templateUrl: './term-condition.component.html',
  styleUrls: ['./term-condition.component.css']
})
export class TermConditionComponent implements OnInit {

  constructor(private _location: Location) { }

  ngOnInit() {
    this.topFunction();
  }

  goBack(){
    this._location.back();
  }

  topFunction() {
    $('html, body').animate({ scrollTop: 0 }, 'fast');
  }

}
