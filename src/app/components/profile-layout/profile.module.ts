import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ProfileLayoutComponent } from './profile-layout.component';
import { RouterModule, Router, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized } from '@angular/router';
import { CommonModule } from '@angular/common';
import {ProfileService} from './service/profile.service';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    ProfileLayoutComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
    {
      path: '',
      component: ProfileLayoutComponent

    }
  ])
],
providers: [ProfileService],

})
export class SellerModule { }
