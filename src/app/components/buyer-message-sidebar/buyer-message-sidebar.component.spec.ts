import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerMessageSidebarComponent } from './buyer-message-sidebar.component';

describe('SellerMessageSidebarComponent', () => {
  let component: SellerMessageSidebarComponent;
  let fixture: ComponentFixture<SellerMessageSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerMessageSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerMessageSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
