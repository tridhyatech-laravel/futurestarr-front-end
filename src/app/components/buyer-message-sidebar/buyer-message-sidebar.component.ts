import { Component, OnInit } from '@angular/core';
import { SharedService ,image_path_url } from '../../services/shared.service';
import { Router } from '@angular/router';
declare var $:any;
@Component({
  selector: 'app-buyer-message-sidebar',
  templateUrl: './buyer-message-sidebar.component.html',
  styleUrls: ['./buyer-message-sidebar.component.css'],
  providers:[SharedService]
})
export class SellerMessageSidebarComponent implements OnInit {
  unread_count_value:any;
  current_user_id:any;
  all_users:any;
  image_path:any;

  public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
  constructor(
    public sharedService : SharedService,
    public router:Router,
  ) { 
    this.current_user_id=this.currentUser.id;
  }

  ngOnInit() {
    this.get_unread_count();
    this.get_all_user();
  }

  get_unread_count(){
    this.sharedService.get_unread_count().then(res=>{
      this.unread_count_value=res['unread_count'];
    }).catch(error => {
        //    console.log("error value: " + error);
        })
    }

  get_all_user(){
      this.sharedService.get_all_users().then(res=>{
          this.all_users=res;
          this.image_path=image_path_url;
  }).catch(error => {
      //    console.log("error value: " + error);
      })
  }

  inbox_click(user_id,first_names,last_names){
    this.router.navigate(['./message'],{ queryParams: { user_id: user_id,first_names:first_names,last_names:last_names } });
  }

}
