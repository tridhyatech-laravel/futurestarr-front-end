import { Component, OnInit, PLATFORM_ID, Inject } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';

declare var jquery: any;
declare var $: any;


@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

    constructor(@Inject(PLATFORM_ID) private platform: Object) { }

    ngOnInit() {

        this.setFooterSize();
    }

    setFooterSize() {
        if (isPlatformBrowser(this.platform)) {
            window.setInterval(function () {
                // console.log($("#main-content").height() +' main-c ')
                var ht = window.innerHeight;
                var f = $('#main-footer').height();
                var h = $('#main-header').height();
                if (h > $("#main-content").height()) {
                    var container = ht - (h + f) - 40;
                    $("#main-content").css({ "height": container + 'px' });
                }
                // console.log(ht + ' :layout:: ' + f + ' >> ' + h)
            }, 500)

        }
    }

}
