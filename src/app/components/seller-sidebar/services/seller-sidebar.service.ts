import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Http, Response } from '@angular/http';
import { SharedService } from '../../../services/shared.service';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
declare var $:any;

@Injectable()
export class SellerSidebarService {

    headers:Headers;
    options:RequestOptions;
    //public user:{};
    public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));

    constructor(
        public http:Http,
        public sharedService:SharedService) {

        // console.log('seller service connected =========== ' + this.currentUser);

        // this.headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Bearer '+ this.currentUser.token});
        this.headers = new Headers({'Content-Type': 'application/json'});
        this.options = new RequestOptions({headers: this.headers});
    }

     sellerProfile():Promise<Object> {
         return this.http.get(this.sharedService.apiUrl + '/api/seller/account?user_id='+this.currentUser.id, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    sellerTotalCount():Promise<Object> {
        return this.http.get(this.sharedService.apiUrl + '/api/seller/total-count?user_id='+this.currentUser.id, this.options).toPromise()
           .then(this.extractData)
           .catch(this.handleErrorPromise);

   }

    private extractData(res:Response) {
       // console.log(res)
        let body = res.json();
        // console.log("login page : below is the extracted data ******* " + body);
        //
        return body || {};
    }

    // below function can be used when deal with Observabled
    private handleErrorPromise(error:Response | any) {
        console.log("error value :: " + JSON.stringify(error));
        // console.error(error.message || error);
        return Promise.reject(error.message || error);
    }

    private handleErrorObservable(error:Response | any) {
        // console.error(error.message || error);
        return Observable.throw(error.message || error);
    }


}
