import { Component, OnInit ,ElementRef, Input, Injectable, ViewChild} from '@angular/core';
import { FileUploader , FileLikeObject} from 'ng2-file-upload';
import { ToastrService } from 'ngx-toastr';
import {SellerSidebarService} from './services/seller-sidebar.service';
import { SharedService } from '../../services/shared.service';
import { LoadingModule } from 'ngx-loading';
//import {1MessageComponent} from '../../modules/seller/message/message.component';
import { RouterModule, Router, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized } from '@angular/router';

declare var $:any;
 const URL = 'http://192.168.1.238/futurestarr/api/upload/profile';

@Component({
    selector: 'app-seller-sidebar',
    templateUrl: './seller-sidebar.component.html',
    styleUrls: ['./seller-sidebar.component.css']
})
export class SellerSidebarComponent implements OnInit {


    public uploader:FileUploader = new FileUploader({url: URL, itemAlias: 'photo', removeAfterUpload: true});
    public image:any;
    public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
    public btnValue:string = 'Submit';
    public errorMessage:string = '';
    public users:any;
    public unread_count_value:any;
    public dashboardSidebar:boolean = false;
    public commonSidebar:boolean = false;
    public messageSidebar:boolean = false;
    public latestImage;
    public loading:boolean = false;
    public awd:any={};
    public sale:any={};

    //@ViewChild(MessageComponent) MessageComponent:MessageComponent;



        // image variables
    public allowedImageType = ['image/png', 'image/gif', 'image/jpeg'];
    public maxImageSize = 8 * 1024 * 1024; // 8 MB
    public imageUploader:FileUploader = new FileUploader({
        url: URL,
        itemAlias: 'image',
        allowedMimeType: this.allowedImageType,
        maxFileSize: this.maxImageSize,
        autoUpload: false,
        removeAfterUpload: true
    });

    constructor(
          private el:ElementRef,
          public sellerSidebarService:SellerSidebarService,
          public router:Router,
          public sharedService : SharedService,
          public toaster:ToastrService) {

 // image uploader functions
        this.imageUploader.onWhenAddingFileFailed = (item, filter, options) => this.onUploadImageFailed(item, filter, options);
        this.imageUploader.onAfterAddingFile = (fileItem)=> {
            fileItem.withCredentials = false;

            //console.log("item value of image :: " + fileItem);
          //  console.log(fileItem.file.type);
            this.uploadImages();
        };
        this.imageUploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
            //console.log("ImageUpload:uploaded:", item, status, response);
        };

         this.sellerUser();
         this.sellerTotalCount();


           if(this.router.url === '/seller/dashboard'){
                   this.dashboardSidebar = true;
                   this.commonSidebar = false;
                   this.messageSidebar = false;
             }


             if(this.router.url === '/seller/message') {

                // console.log("message sidebar");
                   this.dashboardSidebar = false;
                   this.commonSidebar = false;
                   this.messageSidebar = true;
             }


             if(this.router.url != '/seller/message' && this.router.url != '/seller/dashboard') {

                // console.log("common sidebar");
                   this.dashboardSidebar = false;
                   this.commonSidebar = true;
                   this.messageSidebar = false;
             }
            //  var temp = sharedService.imagePathAnnounced$.subscribe(res => {
            //     this.imagePath = res;
            //   });

            var temp = sharedService.imagePathAnnounced$.subscribe(res => {
                this.updatedSellerUser();
              });

    }


    ngOnInit() {
        this.get_unread_count();

        // $(".navbar-fixed-top").css("background-color", "#151829");
        //override the onAfterAddingfile property of the uploader so it doesn't authenticate with //credentials.

          this.router.events.subscribe((val) => {
        // see also
     //   console.log(val instanceof NavigationEnd);

         if((val instanceof NavigationEnd) === true){
            // console.log("true");
           //  console.log("get url name :: " + this.router.url);

            if(this.router.url === '/seller/dashboard'){
                this.dashboardSidebar = true;
                this.commonSidebar = false;
                this.messageSidebar = false;
            }

            if(this.router.url === '/seller/message') {

               //  console.log("message sidebar");
                   this.dashboardSidebar = false;
                   this.commonSidebar = false;
                   this.messageSidebar = true;
             }


             if(this.router.url != '/seller/message' && this.router.url != '/seller/dashboard') {

                 //console.log("common sidebar");

                   this.dashboardSidebar = false;
                   this.commonSidebar = true;
                   this.messageSidebar = false;
             }


         }else {
           //  console.log("not true");

         }


    });


    }
    // getImage(){
    //     this.imagePath = this.currentUser.profile_pic;
    //     console.log(this.imagePath);
    //     return this.imagePath;
    // }



    //ngAfterViewInit() {
    //     console.log("side bar is called :: " + this.router.url);
    //    this.MessageComponent.getUrl();
    //}

     //this.uploader.onAfterAddingFile = (file)=> {
     //       file.withCredentials = false;
     //   };
     //   //override the onCompleteItem property of the uploader so we are
     //   //able to deal with the server response.
     //   this.uploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
     //       console.log("ImageUpload:uploaded:", item, status, response);
     //   };


     showSidebar() {

     }


    //------------get unread message count---------
    get_unread_count(){
        this.sharedService.get_unread_count().then(res=>{
          this.unread_count_value=res;
        }).catch(error => {
            //    console.log("error value: " + error);
            })
        }

     sellerUser() {
        // console.log("seller *************");
            this.loading = true;
            this.sellerSidebarService.sellerProfile().then(res => {
            this.users = res;
            // console.log(this.users);
            this.latestImage = this.users[0].profile_pic;
            this.loading = false;
        }).catch(error => {
            this.loading = false;
            // console.log("error value: " + error);

        });
    }

    sellerTotalCount()
    {
        this.loading = true;
        this.sellerSidebarService.sellerTotalCount().then(res => {
        // console.log("side bar ::" + JSON.stringify(res));
        this.awd = res;
        this.sale = res;
        this.loading = false;
        }).catch(error => {
            this.loading = false;
            // console.log("error value: " + error);
        });
    }


    onUploadImageFailed(item:FileLikeObject, filter:any, options:any) {
        // console.log(item.type);
        switch (filter.name) {
            case 'fileSize':
                this.errorMessage = 'Maximum upload size 8 MB allowed';
               // console.log(this.errorMessage);
                break;
            case 'mimeType':
                const allowedTypes = this.allowedImageType.join();
                this.errorMessage = 'Type ' + item.type + ' is not allowed. Allowed types:' + allowedTypes;
               // console.log(this.errorMessage);
                break;
            default:
                this.errorMessage = 'Unknown error filter is ' + filter.name;
            //    console.log(this.errorMessage);
        }
    }


    uploadImages() {
        this.imageUploader.uploadAll();
    }
    // getImagePath(){
    //     return this.latestImage;
    // }



    updatedSellerUser() {
        this.loading = true;
        this.sellerSidebarService.sellerProfile().then(res => {
        this.users = res;
        this.latestImage = this.users[0].profile_pic;
        this.loading = false;
        }).catch(error => {
            this.loading = false;
            // console.log("error value: " + error);
        });
    }





}
