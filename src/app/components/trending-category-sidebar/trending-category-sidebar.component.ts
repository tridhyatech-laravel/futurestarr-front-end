import { Component, OnInit, EventEmitter,Input, Output } from '@angular/core';
import { SharedService } from '../../services/shared.service';
import { LoadingModule } from 'ngx-loading';
import { TrendingCategorySidebarService } from './service/trending-category-sidebar.service'
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-trending-category-sidebar',
  templateUrl: './trending-category-sidebar.component.html',
  styleUrls: ['./trending-category-sidebar.component.css']
})

export class TrendingCategorySidebarComponent implements OnInit {

  public categories;
  public loading = false;
  public selectedItem:any;


   @Output() onClick = new EventEmitter<any>();
   @Output() onclickCategory = new EventEmitter<any>();

  constructor( public sharedService:SharedService ) { 
    this.sharedService.blogCategoryAnnounced$.subscribe(res => {
      // console.log("inside subscribe - "+res);
      this.selectedItem = res; 
    });
  }



  ngOnInit() {
    this.getSideBarCategories();
    // this.callCategoryFunction(0,author)

    
    
  }

  // callCategoryFunction(e, s){
  //      console.log("date range changed in line component "+ e +" source "+s);
  //      this.onclickCategory.emit(s);
  //  }

  callCategoryFunction(id,name){
      // $(".active_category").removeClass("active");
     // $("#").addClass("active");
      //  console.log("date range changed in line component "+ id + "name " +name);
       this.onclickCategory.emit({id, name});     
       this.selectedItem=id;  
   }

  getSideBarCategories(){

    this.loading = true;
    this.sharedService.getSideBarCategories().then(res => {
      // console.log(res);
      this.categories = res;
      this.selectedItem=res['categories'][0].id;
      // // this.selectedItem=0; 
      // this.load_cat_id=this.categories.categories[0].id;
      this.loading = false;
     }).catch(error => {

     });
  }


}
