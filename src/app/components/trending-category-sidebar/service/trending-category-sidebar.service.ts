import { Injectable } from '@angular/core';
import { Http,Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { SharedService } from '../../../services/shared.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
declare var $:any;

@Injectable()
export class TrendingCategorySidebarService {

  headers:Headers;
  options:RequestOptions;

  public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
  public serverIp;

  constructor(public http:Http, public sharedService:SharedService) {
      this.serverIp = this.sharedService.apiUrl;
      this.headers = new Headers({'Content-Type': 'application/json'});
      //this.headers  = new Headers({'Authorization': 'Bearer '+ this.currentUser.token});
      this.options = new RequestOptions({headers: this.headers});
  }


  talentCategories():Promise<Object> {
      return this.http.get(this.sharedService.apiUrl + '/api/seller/categories', this.options).toPromise()
          .then(this.extractData)
          .catch(this.handleErrorPromise);
  }

  blogTalentCategories(category_id):Promise<Object> {
      return this.http.get(this.sharedService.apiUrl + '/api/blog?category_id='+category_id, this.options).toPromise()
          .then(this.extractData)
          .catch(this.handleErrorPromise);
  }

  getDefaultCategory():Promise<Object> {
      return this.http.get(this.sharedService.apiUrl + '/api/default-blog', this.options).toPromise()
          .then(this.extractData)
          .catch(this.handleErrorPromise);
  }

  getBlogData(id):Promise<Object> {
      return this.http.get(this.sharedService.apiUrl + '/api/blog-data?id='+id, this.options).toPromise()
          .then(this.extractData)
          .catch(this.handleErrorPromise);
  }


  private handleError(error:any):Promise<any> {
      console.error('An error occurred', error); // for demo purposes only
      return Promise.reject(error.message || error);
  }

  private extractData(res:Response) {
      let body = res.json();
      return body || {};
  }

  private handleErrorObservable(error:Response | any) {
      console.error(error.message || error);
      return Observable.throw(error.message || error);
  }

  private handleErrorPromise(error:Response | any) {
      console.error(error.message || error);
      return Promise.reject(error.message || error);
  }

}
