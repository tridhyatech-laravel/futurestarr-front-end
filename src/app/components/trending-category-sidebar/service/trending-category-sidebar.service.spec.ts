import { TestBed, inject } from '@angular/core/testing';

import { TrendingCategorySidebarService } from './trending-category-sidebar.service';

describe('TrendingCategorySidebarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TrendingCategorySidebarService]
    });
  });

  it('should be created', inject([TrendingCategorySidebarService], (service: TrendingCategorySidebarService) => {
    expect(service).toBeTruthy();
  }));
});
