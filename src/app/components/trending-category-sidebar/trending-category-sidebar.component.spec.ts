import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendingCategorySidebarComponent } from './trending-category-sidebar.component';

describe('TrendingCategorySidebarComponent', () => {
  let component: TrendingCategorySidebarComponent;
  let fixture: ComponentFixture<TrendingCategorySidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrendingCategorySidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendingCategorySidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
