import { Component, OnInit } from '@angular/core';
declare var jquery:any;
declare var $:any;


@Component({
    selector: 'app-admin-dashboard',
    templateUrl: './admin-dashboard.component.html',
    styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {

    constructor() {

    }

    toggleMenu() {
        $('#sidebar').toggleClass('active');
        $('#sidebarCollapse').toggleClass('active');
    }

    ngOnInit() {
    }

}
