import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
import { SharedService } from '../../services/shared.service';
import { ToastrService } from 'ngx-toastr';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';

declare var $:any;
@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.css']
})
export class PrivacyPolicyComponent implements OnInit {

  constructor(
        // private router: Router,
        public sharedService : SharedService,
        public toastr:ToastrService,
        private _location: Location,
        private titleService: Title, public meta: Meta
  ) {

    this.titleService.setTitle("Privacy Policy");

    this.meta.updateTag({ name: 'author', content: 'Future Starr' });
    this.meta.updateTag({ name: 'keyword', content: 'Privacy policy' });
    this.meta.updateTag({ name: 'description', content: 'Privacy Policy Future Starr' });
   }

  ngOnInit() {
    this.topFunction();
    // this.titleService.setTitle("my foo page");
    // this.meta.updateTag({ name: 'description', content: "my foo pagecontent" });
  }

  goBack(){
    this._location.back();
  }

  topFunction() {
    $('html, body').animate({ scrollTop: 0 }, 'fast');
  }
 

}
