import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, RouterState } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuardService implements CanActivate {

  public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
  constructor(public router : Router) { }
  canActivate() {
    if(this.currentUser && this.currentUser.role_id == 'buyer'){
      return true;
    } 
    else{
      this.router.navigate(['/']);
    }
    
  }

}
