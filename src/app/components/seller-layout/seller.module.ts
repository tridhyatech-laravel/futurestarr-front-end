import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
// import { SellerLayoutComponent } from './seller-layout.component';
import { RouterModule, Router, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SellerService } from './service/seller.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    // SellerLayoutComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    // RouterModule.forChild([
    //   {
    //     path: '',
    //     component: SellerLayoutComponent

    //   }
    // ])
  ],
  providers: [SellerService],

})
export class SellerModule { }
