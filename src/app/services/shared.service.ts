import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
//import {HttpClient} from '@angular/common/http';
declare var $: any;

// export let image_path_url='http://localhost:8080/future_sar_backend/public/';
export let image_path_url = 'http://localhost/future/public/';
// export let image_path_url='https://www.futurestarr.com:444/future/public/';

@Injectable()
export class SharedService {
  // public apiUrl = 'http://localhost:8080/future_sar_backend/';
   public apiUrl = 'http://localhost/future';
  // public apiUrl = 'https://www.futurestarr.com/future';
  // public apiUrl = 'https://www.futurestarr.com:444/future'
  public web_url = 'http://localhost:4200/';
  // public web_url = 'https://www.futurestarr.com/future_star/';
  // public web_url = 'https://www.futurestarr.com/';


  //*********************************  Important Notice  ****************************************** //
  //pls change url also in message.component.html for image profile pic
  //*********************************  Important Notice  ****************************************** //

  //public apiUrl = 'http://18.218.29.239/futurestarr-backend';
  public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
  public dddata: any;
  headers: Headers;
  options: RequestOptions;



  constructor(public http: Http) {
    //  console.log('Shared service connected');
    this.headers = new Headers({ 'Content-Type': 'application/json' });
    this.options = new RequestOptions({ headers: this.headers });

  }

  verifyUserEmail(token: string): Promise<any> {
    return this.http.get(this.apiUrl + '/api/user/verify_user/' + token, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  sendThankMail(email: string): Promise<any> {
    return this.http.get(this.apiUrl + '/api/thank/' + email, this.options).toPromise()
      .then(res => res.json().user)
      .catch(this.handleErrorPromise);
  }

  sendContactInfo(body: Object): Promise<Object> {
    return this.http.post(this.apiUrl + '/api/contact-us', body, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  saveAutomaticMessageSetting(body: Object): Promise<Object> {
    return this.http.post(this.apiUrl + '/api/autoreply-setting', body, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  getCartItems(user_id): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/cart/items?user_id=' + user_id, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  deleteCartItems(id): Promise<Object> {
    return this.http.put(this.apiUrl + '/api/cart/items/edit?id=' + id, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  getCartCount(user_id): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/cart/items/count?user_id=' + user_id, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  // editQuantity(id, qty, price): Promise<Object> {
  //   return this.http.put(this.apiUrl + '/api/cart/qty/edit?id='+id+'&qty='+qty+'&price='+price, this.options).toPromise()
  editQuantity(body): Promise<Object> {
    return this.http.put(this.apiUrl + '/api/cart/qty/edit', body, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  payMoneyByPayPal(body): Promise<Object> {
    return this.http.post(this.apiUrl + '/api/paypal?user_id=' + this.currentUser.id, body, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  getSideBarCategories(): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/social-buzz/categories', this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }
  getSearchResults(query): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/search?query=' + query, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }
  getProfileInfo(user_id): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/public-profile/' + user_id, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  //buyer sidebar api call function
  getBuyerPurchaseItems(): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/buyer/purchase/count?user_id=' + this.currentUser.id, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  latestBlogs(): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/latest_blog', this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }


  private extractData(res: Response) {
    let body = res.json();
    // console.log("below is the extracted data");
    // console.log(body);
    return body || {};
  }

  private handleErrorPromise(error: Response | any) {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
  }

  private imagePathSource = new Subject<string>();
  imagePathAnnounced$ = this.imagePathSource.asObservable();
  // Service message commands
  announceImagePath() {
    this.imagePathSource.next();
  }

  private usernameSource = new Subject<string>();
  usernameAnnounced$ = this.usernameSource.asObservable();
  // Service message commands
  announceUsername() {
    this.usernameSource.next();
  }

  private askToLogIn = new Subject<string>();
  askToLogInAnnounced$ = this.askToLogIn.asObservable();
  // Service message commands
  announceAskToLogIn(reirectUrl = null) {
    //console.log(reirectUrl);
    this.askToLogIn.next(reirectUrl);
  }

  private redireturl = new Subject<string>();
  preserveRedirectUrl$ = this.redireturl.asObservable();
  askToPreserveUrl(url){
    //console.log(url);
    this.redireturl.next(url);
  }

  private cartItemDeleted = new Subject<string>();
  cartItemDeletedAnnounced$ = this.cartItemDeleted.asObservable();
  // Service message commands
  announcecartItem() {
    this.cartItemDeleted.next();
  }

  private logOut = new Subject<string>();
  logOutAnnounced$ = this.logOut.asObservable();
  // Service message commands
  announcelogOut() {
    this.logOut.next();
  }

  public blogCategory = new Subject<string>();
  blogCategoryAnnounced$ = this.blogCategory.asObservable();
  // Service message commands
  announceBlogCategory(e) {
    this.blogCategory.next(e);
  }

  public blogSelectedCategory = new Subject<string>();
  blogSelectedCategoryAnnounced$ = this.blogSelectedCategory.asObservable();
  announceSelectedCategory(e) {
    //console.log("asObservable - " + e);
    this.blogSelectedCategory.next(e);
  }

  processToCheckout(data): Promise<Object> {
    let dataToSend = {
      items: data,
      //  name:"praveen"
    }
    let options = new RequestOptions({
      body: dataToSend
    });
    //console.log(options);
    let paypal_url = 'https://www.futurestarr.com/paypal/package_three.php'
    // let paypal_url = 'http://localhost/paypal/package_three.php'
    return this.http.post(paypal_url, dataToSend).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }


  get_all_users(): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/message/getalluser/' + this.currentUser.id, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }
  //---------read message-----------
  get_read_msg(): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/message/getreadmsg/' + this.currentUser.id, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  //---------unread message-----------
  get_unread_msg(): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/message/getunreadmsg/' + this.currentUser.id, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  get_all_user_msg(chat_id): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/message/getUserMsg/' + this.currentUser.id + '/' + chat_id, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  getSellerTalentAwardUsers(): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/seller/talentAwardUsers/' + this.currentUser.id, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  store_message(formData): Promise<Object> {
    // return this.http.get(this.apiUrl + '/api/message/store_msg/'+this.currentUser.id+'/'+receiverId+'/'+message_send,this.options).toPromise()
    //   .then(this.extractData)
    //   .catch(this.handleErrorPromise);
    //console.log(formData);
    return this.http.post(this.apiUrl + '/api/message/store_msg', formData).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  insertAdsViews(formData): Promise<Object> {
    return this.http.post(this.apiUrl + '/api/add_views_count', formData).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  // for checking user is favrate or not 
  check_is_favorite(receiverId): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/message/check_is_favrate/' + this.currentUser.id + '/' + receiverId, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  add_to_fav(favrate_ids): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/message/add_to_favrate/' + this.currentUser.id + '/' + favrate_ids, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  delete_from_fav(favrate_ids): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/message/delete_from_favrate/' + this.currentUser.id + '/' + favrate_ids, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  // get all favrate contact
  get_all_fav_contact(): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/message/get_all_fav_contact/' + this.currentUser.id, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  searchContact(inputString): Promise<Object> {
    let param = {
      name:inputString
    }
    return this.http.get(this.apiUrl + '/api/contact_search/' + this.currentUser.id, { params:param }).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  get_all_contact(): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/message/get_all_contact/' + this.currentUser.id, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  get_unread_count(): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/message/get_unread_count/' + this.currentUser.id, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  //-----------delete message---------

  delete_mesasge(messageid): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/message/delete_message/' + this.currentUser.id + "/" + messageid, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  delete_allmesasge(): Promise<Object> {
    return this.http.get(this.apiUrl + '/api/message/delete_allmessage/' + this.currentUser.id, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }
}




// registerUser (body: Object): Promise<Object> {
//
//      let headers      = new Headers({ 'Content-Type': 'application/json' });
//      let options       = new RequestOptions({ headers: headers });
//
//      return this.http.post(this.apiUrl, body, options).toPromise()
//                       .then(res => res.json().user)
//                       .catch(this.handleError);
//  }
//
//  private handleError(error: any): Promise<any> {
//   console.error('An error occurred', error); // for demo purposes only
//   return Promise.reject(error.message || error);
// }
