import { Component, ViewContainerRef, PLATFORM_ID, Inject, } from '@angular/core';

import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import 'rxjs/add/operator/mergeMap';
// import * as $ from 'jquery';
// import { } from 'emojionearea';

import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { DOCUMENT } from '@angular/common';


// declare var jquery: any;
declare var $: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {

    constructor(vcr: ViewContainerRef, private router: Router,
        private activatedRoute: ActivatedRoute,
        private titleService: Title, public meta: Meta,
        @Inject(PLATFORM_ID) private platform: Object,
        @Inject(DOCUMENT) private document: any
    ) {


    }

    title = 'app';

    ngOnInit() {
        this.setFooterSize();
        if (isPlatformBrowser(this.platform)) {
            $(document).on("scroll", function () {
                if ($(document).scrollTop() > 10) {
                    let myBtn = document.getElementById("myBtn")
                    if (myBtn)
                        document.getElementById("myBtn").style.display = "block";
                } else {
                    document.getElementById("myBtn").style.display = "none";
                }
            });
        }

        // this.router.events
        //     .filter(event => event instanceof NavigationEnd)
        //     .map(() => this.activatedRoute)
        //     .map(route => {
        //         while (route.firstChild) route = route.firstChild;
        //         return route;
        //     })
        //     .filter(route => route.outlet === 'primary')
        //     .mergeMap(route => route.data)
        //     .subscribe((event) => {
        //         console.log(event);
        //         let currentUrl = this.router.url;
        //         let search = currentUrl.search("star-search/about");
        //         if (search > 0) {
        //             this.setStarSearchCategoryData(currentUrl);
        //         }
        //         else {
        //             // console.log(event);
        //             if (event['title']) {
        //                 this.titleService.setTitle(event['title'])
        //             }
        //             else {
        //                 let title = "Future Starr | Talent Marketplace | Sell your Talent Online";
        //                 this.titleService.setTitle(title);
        //             }
        //             this.updateMetaInfo(event['description'], event['keywords']);
        //         }
        //     });

        // this.meta.updateTag({
        //     name: 'description',
        //     content: 'content'
        // })

    }
    updateMetaInfo(content, category, author = '') {
        if (content) {
            // this.meta.updateTag({ name: 'description', content: content });
        }
        else {
            let contents = "Future Starr promotes promising talent from around the world. Our platform helps you in becoming a famous celebrity. Only requirement is; just upload your talent videos on Future Starr and showcase your talent to the world. Sell your Talent Free - Make Sales and Be your Own Boss!";
            // this.meta.updateTag({ name: 'description', content: contents });
        }

        if (category) {
            // this.meta.updateTag({ name: 'keywords', content: category });
        }
        else {
            // let categorys = "advertise yourself, future rising stars, entertainment careers, online portfolios, online talent marketplace"
            // this.meta.updateTag({ name: 'keywords', content: categorys });
        }

        // this.meta.updateTag({ name: 'author', content: author });
    }

    setStarSearchCategoryData(url) {
        let data = url.split("/");
        let categoryId = parseInt(data[data.length - 1]);
        if (categoryId) {
            // console.log("inside if");
            switch (categoryId) {
                case 1:
                    var title = 'Promote & Advertise yourself Online as an Author | Future Starr';
                    var description = 'Sign Up with Future Starr and become a successful & well known Author. Future Starr is a platform where you can share your vision as well as your quality work to the audience around the globe. So Sign up with Future Starr, upload your work, make a great impact and target and reach millions of readers/audience worldwide.';
                    var keywords = 'Book marketing, self promotions, promote your book online, promote yourself as an author, promote yourself as an writer, promote yourself as an publisher';
                    this.titleService.setTitle(title);
                    this.updateMetaInfo(description, keywords);
                    break;
                case 2:
                    var title = 'Brand and Promote Yourself as an Entertainer | Future Starr';
                    var description = 'Entertainer! Sign Up Today with Future Starr and promote yourself as a Celebrity and also boost up your reputation among your followers. With Future Starr you can have access to top vendors and get connected with your valuable audiences all over the globe. Hurry up, Sign Up Today!';
                    var keywords = 'branding for entertainers, personal branding, ';
                    this.titleService.setTitle(title);
                    this.updateMetaInfo(description, keywords);
                    break;
                case 3:
                    var title = '';
                    var description = '';
                    var keywords = '';
                    this.titleService.setTitle(title);
                    this.updateMetaInfo(description, keywords);
                    break;
                case 4:
                    var title = 'Sell Promote and Earn more with your Music | Future Starr';
                    var description = 'Join Future Starr community and let your music speak for you. Whatever your genre may be; Punk, Rap, Hip Hop, EDM, Pop, Metal, Rock; just upload your music and promote yourself. Future Starr will help you get connected to various labels, brands, publishers and to a wider fan community. Sign Up Today!';
                    var keywords = 'sell your music online, promote your music, sell music online, Digital Music Distribution, upload my song online free';
                    this.titleService.setTitle(title);
                    this.updateMetaInfo(description, keywords);
                    break;
                case 5:
                    var title = 'Share, Display and Sell your Photos Online | Future Starr';
                    var description = 'Future Starr is a global marketplace for the photographers and artists to display, share and sell their images. Whatever your niche may be including travel, fashion, cityscapes landmarks, weddings, portraitures, wildlife or more; you just need to upload your passionate projects and turn it into hard cash.Sign Up Today!';
                    var keywords = 'fine art photography, sell your photographs online, professional photographer, sell images online';
                    this.titleService.setTitle(title);
                    this.updateMetaInfo(description, keywords);
                    break;
                case 6:
                    var title = 'Standup Comedy Online Platform | Future Starr';
                    var description = 'Sign Up with Future Starr and get connected to the global audiences. Upload your Best Comic works, events, video clips from your T.V. shows, podcasts and expand your fan community way beyond and be a global star. Hurry Up! Promote your comedy event and get the word out to the right people and make yourself successful.';
                    var keywords = 'art of comedy, comedian actors portfolio, comedy club,';
                    this.titleService.setTitle(title);
                    this.updateMetaInfo(description, keywords);
                    break;
                case 7:
                    var title = 'Online Model Portfolio | Modelling Career | Future Starr';
                    var description = 'Sign Up Today and outshine in the Modeling Industry. Upload your various projects and photoshoots, showcasing your lovely features and artistic vision and make your presence impressive. To create and promote a high-quality online modeling portfolio join Future Starr team today.';
                    var keywords = 'online model portfolio, online modeling agency, become a model, best model portfolio websites, Create an Online Portfolio';
                    this.titleService.setTitle(title);
                    this.updateMetaInfo(description, keywords);
                    break;
                case 8:
                    var title = 'Marketplace for Online Personal Fitness Trainer | Future Starr';
                    var description = 'Sell Your Talent with Future Starr Today! Either you are a personal trainer or fitness guru you need a platform that can get you connected with the global audience and help you attract clients worldwide. Future Starr will help you promote and sell your services of fitness products instantly. Read more.';
                    var keywords = 'online personal trainer, online gym trainer, online exercise trainer, fitness training career, ';
                    this.titleService.setTitle(title);
                    this.updateMetaInfo(description, keywords);
                    break;
                case 9:
                    var title = 'National Geographic Careers & Talent Hunt | Future Starr';
                    var description = 'Future Starr enables you to get connected with the global audience. Upload videos of your training sessions, meetings, rallies or marches and engage the global audience to create awareness about various environmental issues and attract non-profit organizations, or other activists who can support you.';
                    var keywords = 'natgeo, wildlife photographer, national geographic photos, national geographic careers';
                    this.titleService.setTitle(title);
                    this.updateMetaInfo(description, keywords);
                    break;
                case 10:
                    var title = 'Science Employment Opportunities | Future Starr';
                    var description = 'Upload your profile and be accessible and searchable with Future Starr. Upload your actual classroom teaching visual clips or experiments in biology, chemistry, engineering or you can also upload charts graphs, or tables to provide detailed knowledge about various scientific concepts. Upload Your Classroom Teaching Today.';
                    var keywords = 'science talent search, science careers, science careers jobs, science careers options, science employment';
                    this.titleService.setTitle(title);
                    this.updateMetaInfo(description, keywords);
                    break;
                case 11:
                    var title = 'Advertise Your Cooking Talent & Skills | Future Starr';
                    var description = 'Advertise your Food Making Skills with Future Starr Today! Promote and share your culinary expertise with a like-minded community of cooks, culinary fans, and food lovers. Create galleries through pictures of your recipes video tutorials and reach worldwide food enthusiasts.';
                    var keywords = 'cooking talent, advertise cooking skills, market and brand cooking talent, professional chefs, advertising website';
                    this.titleService.setTitle(title);
                    this.updateMetaInfo(description, keywords);
                    break;
                case 12:
                    var title = 'Nutritionists & Dietitians Talent | Future Starr';
                    var description = 'Whether you are a registered dietitian or nutritionist, With Future Starr, you can target your specialized audience by uploading your blogs, or videos to share your healthy cooking recipes or your opinions about some important nutrition topics. Sign Up Today!';
                    var keywords = 'Nutrition Professionals, nutritionists and dietitians talent, dietitian skills, nutritionist careers';
                    this.titleService.setTitle(title);
                    this.updateMetaInfo(description, keywords);
                    break;
                case 13:
                    var title = 'Careers Opportunities for Maths Tutors Online | Future Starr';
                    var description = 'Get rid of costly Advertisements! If you are a calculus or trigonometry expert in writing tricky math problems and want to be more accessible to your students, get connected with Future Starr. Join future starr today and express your math knowledge and skills. Just upload your work and help the students.';
                    var keywords = 'Career Opportunities for Mathematics, best online tutoring sites, maths teacher jobs in usa, Math Tutors and Teachers Online';
                    this.titleService.setTitle(title);
                    this.updateMetaInfo(description, keywords);
                    break;
                case 14:
                    var title = 'Career Opportunities in Beauty & Cosmetic Industry | Future Starr';
                    var description = 'Sign Up with Future Starr Today. Whether you deal in Cosmetics, Fragrance, Skin Care Products, Hair Care Products or Salon Services, linking up with Future Starr will earn your ultimate loyal fanbase and clientele. Join us Today ! Future Starr fulfills your dreams and also make the dreams of your clients come true.';
                    var keywords = 'cosmetic industry, beauty job opportunities,beauty business, online cosmetic industry, Industrial Models  ';
                    this.titleService.setTitle(title);
                    this.updateMetaInfo(description, keywords);
                    break;
                case 15:
                    var title = 'Fashion Designer Careers, Talent & Opportunities | Future Starr';
                    var description = 'Join hands with Future Starr, a dedicated Fashion Design Platform for Freelance Fashion Designers. Upload your clothing or your catalogs, displaying size and colors available and offer bespoke service to your customers. Share the competitions you organized or celebrations you made and make your customers your partners.';
                    var keywords = 'fashion designing, fashion industry, fashion careers, fashion career opportunity';
                    this.titleService.setTitle(title);
                    this.updateMetaInfo(description, keywords);
                    break;
                case 16:
                    var title = 'Professional Tattooists | Tattoo Artist Careers | Future Starr';
                    var description = 'Sign Up with Future Starr, and get connected with reputable tattoo studios, can book your consultations easily and that too around the world. Upload your curated art galleries, articles, blogs, client’s reviews, artists’ recommendations, tattoo making videos. ';
                    var keywords = 'Professional Tattooist, Professional Tattooing, Tattoo artist, Custom Tattoo Designs, professional and experienced tattoo artist ';
                    this.titleService.setTitle(title);
                    this.updateMetaInfo(description, keywords);
                    break;
                default:
                    // var title = 'default called';
                    var title = 'Future Star Search | Premier Talent Marketing Platform ';
                    var description = 'Future Starr, an Online Talent Distribution Platform where you can promote and turn your talent into a full-time career. Whether you are fitness trainer, an artist, author, entertainer, photographer or a comedian our Starr Search feature will help you to promote and market your talent. Sign up Today with Future Starr.';
                    var keywords = 'star search, Online career fairs, talent marketing platform, build successful career, advertise online';
                    this.titleService.setTitle(title);
                    this.updateMetaInfo(description, keywords);
            }
        }
    }

    setFooterSize() {
        if (isPlatformBrowser(this.platform)) {
            window.setInterval(function () {
            // console.log($("#main-content").height() + ' main-c ')
            var ht = window.innerHeight;
            var f = $('#main-footer').height();
            var h = $('#main-header').height();

            if (h > $("#main-content").height()) {
                var container = ht - (h + f) - 40;
                $("#main-content").css({ "height": container + 'px' });
            }
            // console.log(ht + ' ::: ' + f + ' >> ' + h)
            },500)
        }
    }

}
