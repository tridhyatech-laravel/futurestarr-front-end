// import {ToastOptions} from 'ng2-toastr';

export class CustomOption {
  animate = 'fade'; // you can override any options available
  newestOnTop = false;
  showCloseButton = true;
  positionClass = 'toast-top-center';
  dismiss = 'auto';
  toastLife = 3000;
  maxShown = 1;
}
