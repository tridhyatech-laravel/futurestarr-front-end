import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap, RouterLinkActive } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from './service/login.service';
import { SharedService } from '../../services/shared.service';
import { Response } from '@angular/http';
import { AuthService, FacebookLoginProvider, GoogleLoginProvider, LinkedinLoginProvider, SocialUser } from 'ng-social-login-module';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

declare var $: any;

// var url_redirect = "http://localhost:4200/";
// var url_redirect = "https://www.futurestarr.com/future_star/";
var url_redirect = "https://www.futurestarr.com/";
// var url_redirect = "http://futurestarr.com:4010/";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public showAuthError: boolean = false;
  public errorMsg: string = '';
  public toggleButton: boolean = true;
  public btnValue: string = 'Submit';
  public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
  public socialUserDeatil: any;
  public socialUser: any = {};
  public loading: boolean = false;
  public socialInfo: any = {};


  public user: SocialUser;
  private loggedIn: boolean;
  public link: any = {};
  public user_type;
  redirectURL; any;
  userData:any;

  constructor(
    private authService: AuthService,
    public loginService: LoginService,
    public router: Router,
    private _firebaseAuth: AngularFireAuth,
    public shareSerice: SharedService,
    public toastr: ToastrService) {
    this.shareSerice.preserveRedirectUrl$.subscribe(res => {
      // console.log(res)
      this.redirectURL = res;
    })
  }

  ngOnInit() {
     
    // console.log("init call");
    //this.user = this.currentUser;
    ////console.log.log("value of current user ::" + JSON.stringify(this.currentUser));
    this.authService.authState.subscribe((user) => {
      this.user = user;
      // console.log(this.user);
      if(this.user){
        this.checkUserExist(this.user);
      }
      
      //console.log.log("User ngonit details="+JSON.stringify( this.user));
      this.loggedIn = (user != null);
    });

  }

  openLoginModal() {
    $('#myModal').modal('show');
    // console.log("openLoginModal call");
  }

  showLoginModal() {
    $('#myModal').modal('show');
    // console.log("showLoginModal call");
  }

  showNewRegister() {
    $('#register_my_model').modal('show');
  }

  model_toggle_close() {
    this.router.navigate(['/register']);
    $('#register_my_model').modal('toggle');
  }

  hideLoginModal() {
    $('#myModal').modal('hide');
  }

  loginUser(formData) {

    this.loading = true;
    this.loginService.login(formData.value).then(user1 => {
      formData.reset();
      let usersData = JSON.parse(JSON.stringify(user1));
      this.hideLoginModal();

      if (this.redirectURL) {
        window.location.href = this.redirectURL;
      }
      else {
        if (usersData.role_id == "buyer") {
          if(usersData.email == "")
          {
            window.location.href = url_redirect + 'buyer/account-setting';
          }

          else{
            window.location.href = url_redirect + 'buyer';
          }
         
        } else if (usersData.role_id == "seller") {

          if(usersData.email == "")
          {
            window.location.href = url_redirect + '/seller/account-setting';
          }
          else{
            window.location.href = url_redirect + 'seller/dashboard';
           }

        }
      }

      // if (usersData.role_id == "buyer") {
      //   window.location.href = url_redirect + 'buyer';
      // } else if (usersData.role_id == "seller") {
      //   window.location.href = url_redirect + 'seller/dashboard';
      // }

      //  //console.log.log("User info: " + JSON.stringify(user1));

      // this.router.navigate(['/register']);
      // window.location.href = 'http://localhost:4200/buyer';
      // this.router.navigate(['/']);

    }).catch(error => {
      this.loading = false;
      // //console.log.log(error._body);
      // //console.log.log("error value: " + error);
      this.showAuthError = true;
      this.errorMsg = 'Incorrect username/email or password';
      // this.router.navigate(['/register']);
    });
  }

  showForgotPasswordModal() {
    this.hideLoginModal();
    $('#forgotModal').modal('show');
  }

  hideForgotPasswordModal() {
    $('#forgotModal').modal('hide');
  }

  forgotPassword(formData) {

    //console.log.log(formData.value);

    this.toggleButton = false;
    this.btnValue = 'Processing...';
    this.loginService.forgot(formData.value).then(res => {
      formData.reset();
      this.btnValue = 'Submit';
      this.toggleButton = true;
      this.hideForgotPasswordModal();
      //console.log.log("yoyo");
      //console.log.log(res);
      this.toastr.success(res + '!', 'Success!');
    }).catch(error => {
      //  //console.log.log(error._body);
      formData.reset();
      this.toggleButton = true;
      this.btnValue = 'Submit';
      this.hideForgotPasswordModal();
      this.toastr.error(error._body + '!', 'Oops!');
    });
  }

  socialLoginDetails() {
    //formData=JSON.stringify(formData);
    //console.log.log("called...****");
    this.loginService.socialLoginDetails().then(res => {
      //this.router.navigate(res);
      //console.log.log("User info:" + res);
      //console.log.log("User info: " + JSON.stringify(res));

    }).catch(error => {
      // //console.log.log(error._body);
      //console.log.log("error value: " + error);

    });
  }

  signInWithGoogle(): void {
    try {
      // this.loading = true;
      // console.log(GoogleLoginProvider.PROVIDER_ID);
      this.authService.signIn(GoogleLoginProvider.PROVIDER_ID)
      // console.log(this.user);
      setTimeout(() => {
        // console.log(this.user);
      }, 3000);
      // this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(res => {
      //   console.log(res);
      //   this.loading = false;
      //   this.user = res;
      //   this.loggedIn = (this.user != null);
      //   if (this.user != null) {
      //     //console.log.log('not null');
      //     this.checkUserExist(res);

      //     this.loading = false;
      //   }
      // }).catch(err => {
      //   console.log(err);
      //   this.loading = false;
      //   // this.signOut();
      //   //console.log.log(err);
      // });
    } catch (error) {
      // console.log("try catch error")
    }
  }

  checkUserExist(user) {
    this.loading = true;
    this.loginService.checkUserExist(user).then(data => {
      this.loading = false;
      // console.log(data);
      if (data["success"]) {
        localStorage.setItem("currentUser", JSON.stringify(data["user_data"]));
        let usersData = JSON.parse(JSON.stringify(data["user_data"]));
        // console.log(usersData);
        if (usersData.role_id == "buyer") {
          if(usersData.email == "")
          {
            window.location.href = url_redirect + 'buyer/account-setting';
          }
          else{
            window.location.href = url_redirect + 'buyer';
          }
       
         } 
        else if (usersData.role_id == "seller") {
          if(usersData.email == "")
          {
            window.location.href = url_redirect + '/seller/account-setting';
          }
          else
          {
            window.location.href = url_redirect + 'seller/dashboard';
           }
        }
      }
      else {
        this.openSellerOrBuyerModal(user);
      }
    })
      .catch(error => {
        this.signOut();
        this.loading = false;
        // console.log(error);
        this.hideLoginModal();
        this.toastr.error("something went wrong, Please try again");
        this.router.navigate(['/register']);

      })
  }

  signInWithFB(): void {
    this.loading = true;
    //console.log.log(FacebookLoginProvider.PROVIDER_ID);
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID)
    // try {
    //   this.loading = true;
    //   this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(res => {
    //     console.log(res);
    //     this.loading = false;
    //     this.user = res;
    //     this.loggedIn = (this.user != null);
    //     if (this.user != null) {
    //       // //console.log.log('not null');
    //       this.checkUserExist(res);

    //     }
    //   }).catch(err => {
    //     //console.log.log(err);
    //     this.loading = false;
    //     console.log(err)
    //     // this.signOut();
    //   });
    // }
    // catch (error) {
    //   this.loading = false;
    //   console.log("catch");
    //   // this.signOut();
    // }

    // this.authService.authState.subscribe((user) => {
    //     this.user = user;
    //      //console.log.log(this.user);
    //    // //console.log.log("User ngonit details="+JSON.stringify(this.user));
    //     this.loggedIn = (user != null);
    //  //  this.socialLoginDetails(this.user);
    //   });
  }

  signInWithLinkedIN(): void {
    this.authService.signIn(LinkedinLoginProvider.PROVIDER_ID)


    
    // this.loading = true;
    // this.authService.signIn(LinkedinLoginProvider.PROVIDER_ID).then(res => {
    //   console.log(res);
    //   this.loading = false;
    //   this.user = res;
    //   this.loggedIn = (this.user != null);
    //   if (this.user != null) {
    //     //  //console.log.log('not null');
    //     this.checkUserExist(res);
    //     this.loading = false;
    //   }
    // }).catch(err => {
    //   this.loading = false;
    //   console.log(err);
    //   // this.signOut();
    // });
  }

  signOut(): void {
    this.authService.signOut().then(res => {
      // console.log(res);
    }).catch(err => {
      // console.log(err);
    });
  }

  openSellerOrBuyerModal(res) {
    this.hideLoginModal();
    this.socialUserDeatil = res;
    $('#askToJoinAsBuyerOrSeller').modal({ backdrop: 'static' });

  }

  userRegisterBySocial(user_type) {
    this.loading = true;
    this.socialInfo.provider_id = this.socialUserDeatil.id;
    this.socialInfo.email = this.socialUserDeatil.email;
    this.socialInfo.name = this.socialUserDeatil.name;
    this.socialInfo.provider = this.socialUserDeatil.provider;
    this.socialInfo.username = (this.socialUserDeatil.username)?this.socialUserDeatil.username:'';
    this.loginService.registerUser(user_type, this.socialInfo).then(res => {
      //console.log.log(res);
      this.socialUser = res;
      this.toastr.success('You have been logged in', 'Success!');
      //console.log.log(this.socialUser.role_id);
      location.reload();
      this.loading = false;
    }).catch(error => {
      this.hideLoginModal();
      //console.log.log(error._body);
      this.toastr.error(error._body + '!', 'Oops!');
      this.signOut();
      this.loading = false;
    });
  }

  sellerLogout() {
    this.loading = true;
    this.shareSerice.announcelogOut();
    this.loginService.logout().then(res => {
      $('#register_my_model').modal('hide');
      this.loading = false;
      localStorage.removeItem('currentUser');
      // this.router.navigateByUrl('/register');
      window.location.href = this.shareSerice.web_url + "register";

    }).catch(error => {
      $('#register_my_model').modal('hide');
      this.loading = false;
      localStorage.removeItem('currentUser');
      // this.router.navigateByUrl('/');
      window.location.href = this.shareSerice.web_url + "register";

    });
  }

  

  openForBuyerLoginModal() {
    this.loading = true;
    this.shareSerice.announcelogOut();
    this.loginService.logout().then(res => {
      $('#register_my_model').modal('hide');
      this.loading = false;
      localStorage.removeItem('currentUser');
      this.openLoginModal();

    }).catch(error => {
      $('#register_my_model').modal('hide');
      this.loading = false;
      localStorage.removeItem('currentUser');
      this.openLoginModal();

    });
  }

  signInWithTwitter() {
    this._firebaseAuth.auth.signInWithPopup(new firebase.auth.TwitterAuthProvider()).then( data=>{
     console.log(data);
      this.userData = {
        name: data.additionalUserInfo.profile['name'],
        email: data.additionalUserInfo.profile['email'],
        provider_id: data.additionalUserInfo.providerId,
        provider: "Twitter",
        username: data.additionalUserInfo.username
      }
      
   console.log(this.userData);
   if(this.userData){
    this.checkUserExist(this.userData);
  }
  
  //console.log.log("User ngonit details="+JSON.stringify( this.user));
  
    })
    .catch( error=>{
      console.log(error);
    })
    
  }
   
 

}
