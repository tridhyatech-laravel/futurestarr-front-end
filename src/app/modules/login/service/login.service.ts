import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Http, Response } from '@angular/http';
import { SharedService } from '../../../services/shared.service';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
declare var $:any;

@Injectable()
export class LoginService {

    headers:Headers;
    options:RequestOptions;
    public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
    headers1:Headers;
    options1:RequestOptions;

    constructor(
        public http:Http,
        public sharedService:SharedService) {

        // console.log('data service connected' + this.currentUser);

        this.headers = new Headers({'Content-Type': 'application/json'});
        this.options = new RequestOptions({headers: this.headers});

        this.headers1 = new Headers({'Content-Type': 'text/html'});
        this.options1 = new RequestOptions({headers: this.headers1});
    }

    login(body:Object):Promise<Object> {
    //   console.log(body);
        return this.http.post(this.sharedService.apiUrl + '/api/login', body, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);
    }

    forgot(user:Object):Promise<Object> {
        return this.http.post(this.sharedService.apiUrl + '/api/forgot', user, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);
    }

    logout():Promise<Object> {
        return this.http.get(this.sharedService.apiUrl + '/api/logout?token='+this.currentUser.token, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);
    }

    checkUserExist(body){
        return this.http.post(this.sharedService.apiUrl+'/api/social_user_check', body, this.options).toPromise()
                .then(this.extractData1)
                .catch(this.handleErrorPromise);
    }

    registerUser(user_type:string,body):Promise<Object> {
    //   console.log(body);
        return this.http.post(this.sharedService.apiUrl+'/api/user/social/'+user_type, body, this.options).toPromise()
                .then(this.extractData)
                .catch(this.handleErrorPromise);
     }


    socialLoginDetails():Promise<Object> {
       // console.log("social *********** :: " + this.http.post(this.sharedService.apiUrl + '/api/social-login-details?service='+'LINKEDIN'+'&user=buyer', this.options).toPromise());

        // return this.http.post(this.sharedService.apiUrl + '/api/socialLoginDetails?service='+'LINKEDIN'+'&user=buyer', body, this.options).toPromise()
        //     .then(this.extractData)
        //     .catch(this.handleErrorPromise);

        return this.http.get(this.sharedService.apiUrl + '/api/social-login?service='+'FACEBOOK'+'&user=buyer', this.options1).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);
    }

    private extractData1(res: Response) {
        let body = res.json();
        // console.log("below is the extracted data");
        // console.log(body);
        return body || {};
      }



    private extractData(res:Response) {
        let body = res.json();
        //console.log("Body data="+JSON.stringify(JSON.parse(body)));
         if (body) {

            localStorage.setItem('currentUser', JSON.stringify(body));
        }
        return body || {};
    }

    // below function can be used when deal with Observabled
    private handleErrorPromise(error:Response | any) {
        console.error(error.message || error);
        return Promise.reject(error.message || error);
    }



    private handleErrorObservable(error:Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    }



}
