import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { LoginComponent } from './login.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {LoginService} from './service/login.service';
import { FormsModule }   from '@angular/forms';
// import { LoadingModule } from 'ngx-loading';


@NgModule({
  declarations: [
    LoginComponent,
    // LoadingModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    FormsModule,
    CommonModule,
    RouterModule.forChild([
    {
      path: 'login',
      component: LoginComponent

    }
  ])
],
  providers: [LoginService],
  bootstrap: [LoginComponent],
  exports:[LoginComponent]
})
export class LoginModule { }
