import { Component, OnInit ,ElementRef, Input} from '@angular/core';
// import { FileUploader , FileLikeObject} from 'ng2-file-upload';
// import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
// import { Router } from '@angular/router';
// import { ToastrService } from 'ngx-toastr';
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';
declare var $:any;


@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.css']
})
export class DownloadComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

    showDownloadModal(){
      // console.log("show download modal");
      $('#downloadModal').modal('show');
    }

    hideDownloadModal() {
        $('#downloadModal').modal('hide');
    }

}
