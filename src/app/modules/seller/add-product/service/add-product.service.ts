import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { SharedService } from '../../../../services/shared.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
declare var $:any;

@Injectable()
export class AddProductService {

    headers:Headers;
    options:RequestOptions;

    public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
    public serverIp;
    constructor(public http:Http, public sharedService:SharedService) {
       // console.log('data service connected');
       this.serverIp = this.sharedService.apiUrl;
       this.headers = new Headers({'Content-Type': 'application/json'});
        //this.headers  = new Headers({'Authorization': 'Bearer '+ this.currentUser.token});
       this.options = new RequestOptions({headers: this.headers});
    }


    talentCategories():Promise<Object> {
        //console.log("seller promote products service function called ******) " + this.currentUser);

        return this.http.get(this.sharedService.apiUrl + '/api/seller/categories', this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

     addProduct(body:Object):Promise<Object> {
       // console.log("seller add products service function called ******) " + this.currentUser);

        return this.http.post(this.sharedService.apiUrl + '/api/seller/talent?user_id='+this.currentUser.id ,body, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    //uploadProfile(body:Object):Promise<Object> {
    //    console.log(body);
    //    return this.http.post(this.sharedService.apiUrl + '/api/upload/profile', body, this.options).toPromise()
    //        .then(res => res.json().user)
    //        .catch(this.handleError);
    //}




    private handleError(error:any):Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }


    // postUserWithPromise(user:Object): Promise<Object>{
    //   let headers      = new Headers({ 'Content-Type': 'application/json'});
    //   let options       = new RequestOptions({ headers: headers });
    //
    //   return this.http.post(this.apiUrl, user, options).toPromise()
    //            .then(this.extractData)
    //                  .catch(this.handleErrorPromise);
    //
    // }
    // postUserWithObservable(user:Object): Observable<Object> {
    //      let headers = new Headers({ 'Content-Type': 'application/json' });
    //      let options = new RequestOptions({ headers: headers });
    //      return this.http.post(this.apiUrl, user, options)
    //                 .map(this.extractData)
    //                 .catch(this.handleErrorObservable);
    //  }
    //
    private extractData(res:Response) {
        let body = res.json();
       // console.log("below is the extracted data of categories ::" + JSON.stringify(body));
        // console.log(body);
        return body || {};
    }

    private handleErrorObservable(error:Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    }

    private handleErrorPromise(error:Response | any) {
        console.error(error.message || error);
        return Promise.reject(error.message || error);
    }

    // login (body: Object): Promise<Object> {
    //
    //      let headers      = new Headers({ 'Content-Type': 'application/json' });
    //      let options       = new RequestOptions({ headers: headers });
    //
    //      return this.http.post(this.apiUrl, body, options).toPromise()
    //                       .then(res => res.json().user)
    //                       .catch(this.handleError);
    //  }

}
