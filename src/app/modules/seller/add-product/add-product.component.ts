import { Component, OnInit ,ElementRef, Input, Output, EventEmitter, HostListener} from '@angular/core';
import { FileUploader , FileLikeObject, FileItem, ParsedResponseHeaders} from 'ng2-file-upload';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { AddProductService } from './service/add-product.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

declare var $:any;
//const URL = 'http://192.168.1.238/futurestarr/api/upload/profile2';
//const URL = 'http://18.217.7.143/futurestarr-backend/api/seller/image';


@Component({
    selector: 'app-add-product',
    templateUrl: './add-product.component.html',
    styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

    showPriceError:boolean = false;
    public categories:any;
    public product:any;
    public talent_id:any;
    public selectedCategory = 'Author';
    public listOfCategories:any = {};
    public loading:boolean = false;
    commercialFileRequired:boolean = false;
    // ----------31/07/2018------------
    errors: Array<string> =[];
    dragAreaClass: string = 'dragarea';
    @Input() projectId: number = 0;
    @Input() sectionId: number = 0;
    @Input() fileExt: string = "JPG, GIF, PNG";
    @Input() maxFiles: number = 5;
    @Input() maxSize: number = 5; // 5MB
    @Output() uploadStatus = new EventEmitter();

    // ----------31/07/2018------------
    // image variables
    public allowedImageType = ['image/png', 'image/gif', 'image/jpeg', 'video/mp4'];
    public maxImageSize = 100 * 1024 * 1024 ; // 100 MB
    public imageUploader:FileUploader = new FileUploader({
        url: this.addProductService.serverIp+'/api/seller/image',
        itemAlias: 'image',
        method: "POST",
        // allowedMimeType: this.allowedImageType,
        allowedFileType: ['video', 'image'],
        maxFileSize: this.maxImageSize,
        autoUpload: false,
        removeAfterUpload: true,
        queueLimit: 1
    });


    // video variables
    public allowedVideoType = ['video/mp4', 'audio/mp3'];
    public maxVideoSize = 100 * 1024 * 1024 ; // 100 MB
    public videoUploader:FileUploader = new FileUploader({
        url: this.addProductService.serverIp+'/api/seller/video',
        itemAlias: 'video',
        removeAfterUpload: false,
        // allowedMimeType: this.allowedVideoType,
        allowedFileType: ['video', 'audio'],
        maxFileSize: this.maxVideoSize,
        autoUpload: false,
        method: "POST",
        //queueLimit: 1
        //headers: [{name: 'Authorization', value: 'Bearer ' + 'token-here'}, {
        //    name: 'Content-Type',
        //    value: 'multipart/form-data'
        //}],

    });

    // pdf/doc variables
    public allowedDocType = ['application/pdf', 'image/jpeg','video/mp4', 'audio/mp3'];
    public maxDocSize = 100 * 1024 * 1024; // 100 MB
    public pdfUploader:FileUploader = new FileUploader({
        url: this.addProductService.serverIp+'/api/seller/pdf',
        removeAfterUpload: false,
        itemAlias: 'pdf',
        // allowedMimeType: this.allowedDocType,
        allowedFileType: ['video','pdf', 'doc', 'image', 'audio'],
        maxFileSize: this.maxDocSize,
        autoUpload: false,
        method: "POST",
        //queueLimit: 1
        //headers: [{name: 'Authorization', value: 'Bearer ' + 'token-here'}, {
        //    name: 'Content-Type',
        //    value: 'multipart/form-data'
        //}],

    });


    errorMessage:string;

    constructor(private _location:Location,
                private el:ElementRef,
                public addProductService:AddProductService,
                public router:Router,
                public toastr:ToastrService) {

        // image uploader functions
        this.imageUploader.onWhenAddingFileFailed = (item, filter, options) => this.onUploadImageFailed(item, filter, options);

        this.imageUploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
            //console.log("ImageUpload:uploaded:", item, status, response);
            ////console.log("test data ++++++++++" + item.formData[0].talent_id);
        };

        this.imageUploader.onAfterAddingFile = (fileItem)=> {
            fileItem.withCredentials = false;
            //console.log("item value of image :: " + fileItem);
            //console.log(fileItem.file.type);
        };

        // video uploader functions
        this.videoUploader.onWhenAddingFileFailed = (item, filter, options) => this.onUploadVideoFailed(item, filter, options);
        this.videoUploader.onAfterAddingFile = (fileItem)=> {
            fileItem.withCredentials = false;
            //console.log(fileItem.file.type);
            //console.log("data is added......");
            //this.uploadVideos();
        };
        this.videoUploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
            //console.log("Video:uploaded:", item, status, response);
        };

        // pdf/doc uploader functions
        this.pdfUploader.onWhenAddingFileFailed = (item, filter, options) => this.onUploadPdfFailed(item, filter, options);
        this.pdfUploader.onAfterAddingFile = (fileItem)=> {
            fileItem.withCredentials = false;
            //console.log(fileItem.file.type);
            // this.uploadDocs();
        };
        this.pdfUploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
            //console.log("pdfUploaded:uploaded:", item, status, response);
        };

        this.talentCategory();

        this.imageUploader.onErrorItem = (item, response, status, headers) => this.onImageErrorItem(item, response, status, headers);
        this.imageUploader.onSuccessItem = (item, response, status, headers) => this.onImageSuccessItem(item, response, status, headers);

        this.videoUploader.onErrorItem = (item, response, status, headers) => this.onVideoErrorItem(item, response, status, headers);
        this.videoUploader.onSuccessItem = (item, response, status, headers) => this.onVideoSuccessItem(item, response, status, headers);

        this.pdfUploader.onErrorItem = (item, response, status, headers) => this.onPdfErrorItem(item, response, status, headers);
        this.pdfUploader.onSuccessItem = (item, response, status, headers) => this.onPdfuccessItem(item, response, status, headers);

    }

    fileChange(){
        // console.log(this.imageUploader.queue);
    }

    onImageSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let data = JSON.parse(response); //success server response
        // this.imageUploader.queue = [];
        if(this.imageUploader.queue.length>0 || this.videoUploader.queue.length>0 ||
            this.pdfUploader.queue.length>0)
        {
            this.loading = true;
        }
        else{
            
            this.loading = false;
            this.toastr.success("Product added successfully");
            this.router.navigate(['/seller/dashboard']);
        }
    }
    
    onImageErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let error = JSON.parse(response); //error server response
        if(this.imageUploader.queue.length>0 || this.videoUploader.queue.length>0 ||
            this.pdfUploader.queue.length)
        {
            this.loading = true;
        }
        else{
            this.loading = false;
            this.toastr.error("Product could not added");
        }
    }

    onVideoSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let data = JSON.parse(response); //success server response
        // this.videoUploader.queue = [];
        if(this.imageUploader.queue.length>0 || this.videoUploader.queue.length>0 ||
            this.pdfUploader.queue.length>0)
        {
            this.loading = true;
        }
        else{
            this.loading = false;
            this.router.navigate(['/seller/dashboard']);
            this.toastr.success("Product added successfully");
        }
    }
    
    onVideoErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let error = JSON.parse(response); //error server response
        if(this.imageUploader.queue.length>0 || this.videoUploader.queue.length>0 ||
            this.pdfUploader.queue.length)
        {
            this.loading = true;
        }
        else{
            this.loading = false;
            this.toastr.error("Product could not added");
        }
    }

    onPdfuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let data = JSON.parse(response); //success server response
        // this.pdfUploader.queue = [];
        if(this.imageUploader.queue.length>0 || this.videoUploader.queue.length>0 ||
            this.pdfUploader.queue.length>0)
        {
            this.loading = true;
        }
        else{
            this.loading = false;
            this.router.navigate(['/seller/dashboard']);
            this.toastr.success("Product added successfully");
        }
    }
    
    onPdfErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let error = JSON.parse(response); //error server response
        if(this.imageUploader.queue.length>0 || this.videoUploader.queue.length>0 ||
            this.pdfUploader.queue.length)
        {
            this.loading = true;
        }
        else{
            this.loading = false;
            this.toastr.error("Product could not added");
        }
    }

    ngOnInit() {
       // //console.log("*****By Piyush*****"+this.addProductService.serverIp)
    }

    topFunction() {
        $('html, body').animate({ scrollTop: 0 }, 'fast');
    }

    ngAfterContentInit() {

    }



    goBack() {
        this._location.back();
    }

    //change($event){
    //
    //      $event.preventDefault();
    //     //console.log("@@@@@@@@@@@@@@ :: "  + $event.target.value);
    //
    //    //console.log(JSON.stringify(this.categories));
    //
    //    this.categories.category = $event.target.value;
    //
    //    //console.log('this.product: ' + this.categories.category);
    //}

    onSelect(val) {
        //console.log("value of selected field  " + val);
        //console.log("*******list of categories*********" + JSON.stringify(this.listOfCategories));
    }

    talentCategory() {
        this.addProductService.talentCategories().then(res => {
            this.categories = res;
            this.categories = this.categories.category;
        }).catch(error => {
            //console.log("error value: " + error);
        });
    }

    addProduct(formData) {

       if(this.imageUploader.queue.length>0){
            this.loading = true;
            this.commercialFileRequired = false;
            this.addProductService.addProduct(formData.value).then(res => {
                // this.uploadImages(res);
                // this.uploadVideos(res);
                // this.uploadDocs(res);
                this.talentCategory();
                this.selectedCategory = 'Author';
                

                if(this.imageUploader.queue.length>0 || this.videoUploader.queue.length>0 ||
                    this.pdfUploader.queue.length)
                {
                    this.loading = true;
                }
                else{
                    this.loading = false;
                    this.toastr.success('Product added successfully!');
                    this.router.navigate(['/seller/dashboard']);
                    formData.reset();
                }
                this.uploadImages(res);
                this.uploadVideos(res);
                this.uploadDocs(res);
                formData.reset();

                
                //this.toastr.success('We have sent an email. Please check your inbox!', 'Success!');
            }).catch(error => {
                this.selectedCategory = 'Author';
                this.loading = false;
                this.toastr.error('Error occured' + '!', 'Oops!');
            });
        }
        else{
            this.commercialFileRequired = true;
            this.topFunction();
        }
    }


    onUploadImageFailed(item:FileLikeObject, filter:any, options:any) {
        
        switch (filter.name) {
            case 'fileSize':
                this.errorMessage = 'Maximum upload size 100 MB allowed';
                //console.log(this.errorMessage);
                this.toastr.error('Maximum upload size 100 MB allowed', 'Error');
                break;
            case 'mimeType':
                const allowedTypes = this.allowedImageType.join();
                this.errorMessage = 'Type ' + item.type + ' is not allowed. Allowed types:' + allowedTypes;
                //console.log(this.errorMessage);
                this.toastr.error('Type ' + item.type + ' is not allowed', 'Error');
                break;
            default:
                this.errorMessage = 'Image and video are allowed';
                this.toastr.error('Image and video are allowed');
                //console.log(this.errorMessage);
        }
    }

    onUploadVideoFailed(item:FileLikeObject, filter:any, options:any) {
        ////console.log(item.type);
        switch (filter.name) {
            case 'fileSize':
                this.errorMessage = 'Maximum upload size 8 MB allowed';
                //console.log(this.errorMessage);
                this.toastr.error('Maximum upload size 8 MB allowed', 'Error');
                break;
            case 'mimeType':
                const allowedTypes = this.allowedVideoType.join();
                this.errorMessage = 'Type ' + item.type + ' is not allowed. Allowed types:' + allowedTypes;
                //console.log(this.errorMessage);
                this.toastr.error('Type ' + item.type + ' is not allowed', 'Error');
                break;
            default:
                this.errorMessage = 'Audio and video are allowed';
                //console.log(this.errorMessage);
                this.toastr.error('Audio and video are allowed');
        }
    }

    onUploadPdfFailed(item:FileLikeObject, filter:any, options:any) {
        //console.log(item.type);
        switch (filter.name) {
            case 'fileSize':
                this.errorMessage = 'Maximum upload size 100 MB allowed';
                //console.log(this.errorMessage);
                this.toastr.error('Maximum upload size 100 MB allowed', 'Error');
                break;
            case 'mimeType':
                const allowedTypes = this.allowedDocType.join();
                this.errorMessage = 'Type ' + item.type + ' is not allowed. Allowed types:' + allowedTypes;
                //console.log(this.errorMessage);
                this.toastr.error('Type ' + item.type + ' is not allowed', 'Error');
                break;
            default:
                this.errorMessage = 'Video, Audio, Image, Pdf and Doc files are allowed';
                //console.log(this.errorMessage);
                this.toastr.error('Video, Audio, Image, Pdf and Doc files are allowed');
        }
    }

    uploadImages(talent_id) {
        this.imageUploader.onBuildItemForm = function(fileItem, form){
            form.append('talent_id', talent_id);
            fileItem.remove();
        };
        this.imageUploader.uploadAll();
    }

    uploadVideos(talent_id) {
        
        this.videoUploader.onBuildItemForm = function(fileItem, form){
                form.append('talent_id', talent_id);
                fileItem.remove();
        };
        this.videoUploader.uploadAll();
    }


    uploadDocs(talent_id) {
        this.pdfUploader.onBuildItemForm = function(fileItem, form){
            form.append('talent_id', talent_id);
            fileItem.remove();
        };
        this.pdfUploader.uploadAll();
    }
}
