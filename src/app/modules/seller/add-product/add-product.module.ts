import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// import {ResetPasswordService} from './service/reset-password.service';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import {FileUploadModule} from "ng2-file-upload";
import { AddProductComponent } from './add-product.component';
import { LoadingModule } from 'ngx-loading';


@NgModule({
  declarations: [
    AddProductComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    FormsModule,
    LoadingModule,
    ReactiveFormsModule,
    FileUploadModule,
    RouterModule.forChild([
    {
      path: '',
      component: AddProductComponent

    }
  ])
],
providers: [],
bootstrap: [AddProductComponent]

})
export class AddProductModule { }
