import { Component, OnInit ,Output,EventEmitter} from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';

declare var $:any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private _location: Location) { }

  ngOnInit() {

  }

  topFunction() {
     $('html, body').animate({ scrollTop: 0 }, 'fast');
 }

    goBack(){
   this._location.back();
}

}
