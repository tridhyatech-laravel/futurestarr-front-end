import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommercialAddDashboardComponent } from './commercial-add-dashboard.component';

describe('CommercialAddDashboardComponent', () => {
  let component: CommercialAddDashboardComponent;
  let fixture: ComponentFixture<CommercialAddDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommercialAddDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommercialAddDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
