import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { CommercialAdsService } from '../../commercial-ads/service/commercial-ads.service';
import { SharedService } from '../../../../services/shared.service';
import { ToastrService } from 'ngx-toastr';

declare var $: any;

@Component({
  selector: 'app-commercial-add-dashboard',
  templateUrl: './commercial-add-dashboard.component.html',
  styleUrls: ['./commercial-add-dashboard.component.css']
})
export class CommercialAddDashboardComponent implements OnInit {

  sellerPlan: any;

  adTitle: any;
  userSelectedFile: any;
  seller_plan_id: any;
  public loading: boolean = false;
  seller_ads: any;
  seller_talents:any;
  product_id:any;
  activeModalPlan:any = [];
  availablePlan:any;
  availableUpgradePlan:any;
  planError:any;
  upgradePlanDetails:any;
  totalViews:number = 0;

  ngOnInit() {
    $('#commercialAdsModal').on('hidden.bs.modal', function () {
      // this.productIdToDelete = false;
      // this.seller_talents = null;
      // this.adTitle = '';
      // this.userSelectedFile = null;
      //console.log("this.productIdToDelete - "+this.productIdToDelete);
      
    })
    $('#upgradePlanModal').on('hidden.bs.modal', function () {
      this.availablePlan = null
      this.activeModalPlan = null
      this.availableUpgradePlan = null
      this.planError = null
      this.upgradePlanDetails = null;
    });

    this.getMyPlans();
    this.getCommercialAds();
  }

  constructor(public toastr: ToastrService, private _location: Location, public adsService: CommercialAdsService, public sharedServices: SharedService) {
    // this.getMyPlans();
    // this.getCommercialAds();
  }
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    layout: {
      padding: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
      },

    },
    scales: {
      xAxes: [{
        stacked: true
      }],
      yAxes: [{
        stacked: true,
        scaleLabel: {
          display: true,
          labelString: 'Views - Thounsonds',
          fontColor: '#fff'
        }
      }],
    },
    legend: {
      labels: {
        fontColor: '#fff'
      }
    }

  };
  // Bar Chart
  public barChartLabels: string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;
  public barChartData: any[] = [
    { data: [-28, -48, -40, -19, -13, -27, -9, -48, -40, -19, -86, -27, -50], label: 'Series B' },
    { data: [85, 59, 80, 81, 56, 55, 40, 59, 80, 81, 56, 55, 10], label: 'Series A' }
  ];

  // events
  public chartClicked(e: any): void {
    // console.log(e);
  }

  public chartHovered(e: any): void {
    // console.log(e);
  }

  // Line Chart

  public lineChartData: Array<any> = [
    { data: [15, 29, 30, 41, 56, 55, 40, 59, 80, 81, 56, 55, 90] }
  ];
  public lineChartLabels: Array<any> = ['2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018'];
  public lineChartOptions: any = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          display: false
        }
      }],
      xAxes: [{
        ticks: {
          display: false
        }
      }]
    },
    tooltips: {
      enabled: false
    }
  };

  public lineChartLegend: boolean = false;
  public lineChartType: string = 'line';
  public lineChartColors: Array<any> = [
    {
      //  backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: '#4285ba',
      // pointBackgroundColor: 'rgba(148,159,177,1)',
      // pointBorderColor: '#fff',
      //  pointHoverBackgroundColor: '#fff',
      //  pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];

  goBack() {
    this._location.back();
  }

  topFunction() {
    $('html, body').animate({ scrollTop: 0 }, 'fast');
  }

  fileInputSelect(event) {
    // console.log(event.target.files[0]);
    if (event.target.files[0])
      this.userSelectedFile = event.target.files[0];
    // console.log(this.userSelectedFile);
  }

  getMyPlans() {
    let user_id = this.sharedServices.currentUser.id;
    this.adsService.sellerPurchasedPlan(user_id).then(data => {
      if (data['success']) {
        this.sellerPlan = data['sellerPlan'];
        // console.log(this.sellerPlan);
      }
    })
      .catch(error => {

      })
  }

  showMyPlanModal() {
    $('#myPlanModal').modal({ backdrop: 'static' });
  }

  openUpgradePlanModal(sellerPlan){
    this.loading = true;
    sellerPlan.remaining_days = this.calculateDays(new Date(), new Date(sellerPlan.end_date))
    
    this.activeModalPlan = sellerPlan;
    // console.log(this.activeModalPlan);
    this.adsService.getUpgradePlans(this.activeModalPlan.plan_id).then( data=>{
      this.loading = false;
      if(data['success']){
        this.availableUpgradePlan = data['plans'];
        setTimeout(()=>{
          this.availablePlan = this.availableUpgradePlan[0].id;
          this.upgradePlanDetails = this.availableUpgradePlan[0];
          this.calculatePlanPrice();
        }, 500);
      }
      else{
        this.planError = data['message'];
      }
      $('#upgradePlanModal').modal({ backdrop: 'static' });
    })
    .catch( error=>{
      this.loading = false;
    })
    
  }

  calculateDays(first, second){
    return Math.round((second-first)/(1000*60*60*24));
  }

  onChange(event){
    // console.log(event);
    // console.log(this.availablePlan);
    
    if(this.availableUpgradePlan){
      this.availableUpgradePlan.forEach(element => {
        if(element.id == this.availablePlan){
          this.upgradePlanDetails = element;  
          this.calculatePlanPrice();
        }
      });
    }
  }
  calculatePlanPrice(){
    let priceTillToday = (this.activeModalPlan.price/30) * (30-this.activeModalPlan.remaining_days);
    let PriceToMinus = this.activeModalPlan.price - priceTillToday;

    let newPriceForRemaingDays = (this.upgradePlanDetails.price/30) * this.activeModalPlan.remaining_days;

    let amountToPay = Math.floor(newPriceForRemaingDays - PriceToMinus);

    this.upgradePlanDetails.amountToPay = amountToPay;
  }

  upgradePlan(){
    this.loading = true;
    this.upgradePlanDetails.user_id = this.sharedServices.currentUser.id;
    this.upgradePlanDetails.seller_plan_id = this.sellerPlan.id;
    // console.log(this.upgradePlanDetails);
    this.adsService.upgradePlanCheckout(this.upgradePlanDetails).then( data=>{
      this.loading = false;
      if(data['success']){
        window.location.href = data['paypal_url']
      }
      else{
        this.toastr.error(data['message']);
        // console.log(data);
      }
    })
    .catch( error=>{
      this.loading = false;
    })
  }

  createAdsModal() {
    this.seller_talents = null;
    this.adTitle = '';
    this.userSelectedFile = null;
    this.product_id = null;
    this.getSellerProducts()
    $('#commercialAdsModal').modal({ backdrop: 'static' });
  }

  getSellerProducts(){
    let user_id = this.sharedServices.currentUser.id;
    this.adsService.getSellerProducts(user_id).then( data=>{
      this.seller_talents = data['talents'];
      this.product_id = this.seller_talents[0].id;
    })
    .catch( error=>{

    })
  }

  deleteSelectedFile() {
    this.userSelectedFile = null;
  }

  uploadAds() {
    if (this.sellerPlan.id && this.seller_talents && this.seller_talents.length > 0) {
      let frmData = new FormData();
      if (this.userSelectedFile)
        frmData.set("banner_file", this.userSelectedFile, this.userSelectedFile.name);

      frmData.set("user_id", JSON.parse(localStorage.getItem('currentUser')).id);
      frmData.set("banner_title", this.adTitle);
      frmData.set("talent_id", this.product_id);
      frmData.set("seller_plan_id", this.sellerPlan.id);
      this.loading = true;
      this.adsService.postAdsVideo(frmData).then(data => {
        this.loading = false;
        if (data['success']) {
          this.getCommercialAds();
          $('#commercialAdsModal').modal('toggle');
          this.userSelectedFile = null;
          this.adTitle = '';
          this.toastr.success(data["message"]);
        }
        else {
          this.toastr.warning(data["message"]);
        }
      })
      .catch(error => {
        this.loading = false;
        this.loading = false;
      })
    }
    else {

    }
  }

  getCommercialAds() {
    let user_id = this.sharedServices.currentUser.id
    this.adsService.getSellerAds(user_id).then(data => {
      if (data['success']) {
        this.seller_ads = data['seller_ads'];
        if(this.seller_ads){
          this.seller_ads.forEach(element => {
            this.totalViews += parseInt(element.views); 
          });
        }
      }
      else {

      }
    })
    .catch(error => {

    })
  }

  disableAds(ads) {
    let dataToSend = {
      ad_id: ads.id,
      user_id: ads.user_id,
      new_status: 0
    }
    this.loading = true;
    this.adsService.enableDisableAd(dataToSend).then(data => {
      this.loading = false;
      if (data['success']) {
        this.toastr.success(data['message']);
        this.getCommercialAds();
      }
      else {
        this.toastr.error(data['message']);
      }
    })
      .catch(error => {
        this.loading = false;
        this.toastr.error("Something went wrong, Please try again");
      })
  }

  enableAds(ads) {
    let dataToSend = {
      ad_id: ads.id,
      user_id: ads.user_id,
      new_status: 1
    }
    this.loading = true;
    this.adsService.enableDisableAd(dataToSend).then(data => {
      this.loading = false;
      if (data['success']) {
        this.toastr.success(data['message']);
        this.getCommercialAds();
      }
      else {
        this.toastr.error(data['message']);
      }
    })
      .catch(error => {
        this.loading = false;
        this.toastr.error("Something went wrong, Please try again");
      })
  }

}