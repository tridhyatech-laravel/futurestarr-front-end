import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CommercialAddDashboardComponent } from './commercial-add-dashboard/commercial-add-dashboard.component';
// import { Cha } from 'chart.js';
// import { UploadCommercialComponent } from './upload-commercial/upload-commercial.component';
import {FileUploadModule} from "ng2-file-upload";
import { FormsModule } from '@angular/forms';
import { LoadingModule } from 'ngx-loading';


@NgModule({
  imports: [
    CommonModule,
    // ChartsModule,
    FormsModule,
    LoadingModule,
    FileUploadModule,
    RouterModule.forChild([
      {
        path: '',
        component: CommercialAddDashboardComponent
  
      },
      // {
      //   path: 'upload-commercial/:seller_plan_id',
      //   component: UploadCommercialComponent
  
      // }
    ])
  ],
  declarations: [CommercialAddDashboardComponent]
})
export class CommercialAddDashboardModule { }
