import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { FileUploader, FileLikeObject, FileItem, ParsedResponseHeaders } from 'ng2-file-upload';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { AccountSettingService } from './service/account-setting.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SharedService } from '../../../services/shared.service';

declare var $: any;

//const URL = 'http://18.217.7.143/futurestarr-backend/api/seller/seller-profile-image';

@Component({
    selector: 'app-account-setting',
    templateUrl: './account-setting.component.html',
    styleUrls: ['./account-setting.component.css']
})

export class AccountSettingComponent implements OnInit {
    public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
    public errorMsg: string = '';
    public user: any = {};
    public social: any = {};
    public profileImage: string;
    public errorMessage: string;
    public currentPercentage: number;
    public visibiltyType = ['Public', 'Private'];
    public selectedVisibilty = 'Public';
    public totalVisibiltyStaus: any = {};
    public latestImage;
    public expType = ['Bignner', 'Entermediate', 'Expert'];
    public selectedExp = 'Entermediate';
    public totalExp: any = {};
    public loading: boolean = false;

    //image variables
    public allowedImageType = ['image/png', 'image/gif', 'image/jpeg'];
    public maxImageSize = 8 * 1024 * 1024; // 8 MB
    public imageUploader: FileUploader = new FileUploader({
        url: this.accountSettingService.serverIp + '/api/seller/seller-profile-image',
        itemAlias: 'profile_pic',
        method: 'POST',
        allowedMimeType: this.allowedImageType,
        maxFileSize: this.maxImageSize,
        autoUpload: false,
        removeAfterUpload: false,
        queueLimit: 1
    });


    constructor(private el: ElementRef,
        public accountSettingService: AccountSettingService,
        public router: Router,
        public toastr: ToastrService,
        public sharedService: SharedService,
        private _location: Location) {


        // image uploader functions
        this.imageUploader.onWhenAddingFileFailed = (item, filter, options) => this.onUploadImageFailed(item, filter, options);
        this.imageUploader.onAfterAddingFile = (fileItem) => {
            fileItem.withCredentials = false;
            // console.log(fileItem.file.type);
            //this.uploadImages();
        };

        this.imageUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            //this.setLatestImage(response);
            // console.log(item);
            this.setLatestImage();
            //console.log(JSON.stringify(response));
        };

    }


    ngOnInit() {
        this.getProfile();
        this.getSocialAccount();
        this.changeCurrentProfilePercentage();
        this.topFunction();
        if(!this.currentUser.email)
        {
            this.toastr.error("We are unable to get the Email from social platform. Please complete your registration by entering the information including email ID ");
        }
        this.imageUploader.onErrorItem = (item, response, status, headers) => this.onErrorItem(item, response, status, headers);
        this.imageUploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);

    }

    onSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let data = JSON.parse(response); //success server response
        // console.log("data");
        // console.log(data);
        if (data.profile_pic) {
            let userData = JSON.parse(localStorage.getItem("currentUser"));
            userData.profile_pic = data.profile_pic;

            localStorage.setItem("currentUser", JSON.stringify(userData));
            window.location.reload();
        }

    }

    onErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let error = JSON.parse(response); //error server response
        // console.log("error");
        // console.log(error);
        this.toastr.success(error.message);
    }

    ngAfterContentInit() {

    }

    topFunction() {
        $('html, body').animate({ scrollTop: 0 }, 'fast');
    }

    goBack() {
        this._location.back();
    }


    changeVisibilty($event) {
        $event.preventDefault();
        //console.log("@@@@@@@@@@@@@@ :: " + $event.target.value);
        this.user[0].visibilty = $event.target.value;
        //console.log('this.selectedFacility: ' + this.user[0].visibilty);
    }

    changeExp($event) {
        $event.preventDefault();
        //console.log("@@@@@@@@@@@@@@ :: " + $event.target.value);
        this.user[0].experience_level = $event.target.value;
        //console.log('this.selectedFacility: ' + this.user[0].experience_level);
    }

    //changeFile($event) {
    //      $event.preventDefault();
    //      this.imageUploader.queue[0].remove();
    //}

    getProfile() {
        this.loading = true;
        this.currentPercentage = 0;

        let account;
        let contact;
        let profile;
        let socialInfo;


        this.accountSettingService.getSellerProfile().then(res => {
            this.user = res;
            //console.log("seller account info :::::::" + JSON.stringify(this.social));

            account = this.user[0].profile_pic != null || this.user[0].email != null;
            contact = account && (this.user[0].username != null || this.user[0].address != null || this.user[0].phone != null);
            profile = contact && (this.user[0].visibilty != null || this.user[0].experience_level != null || this.user[0].first_name != null);
            socialInfo = profile && (this.social.facebook_link || this.social.twitter_link != null || this.social.insta_link != null);
            let profileTemp = this.user[0].profile_pic;
            if (profileTemp)
                this.profileImage = profileTemp.substr(69);

            // console.log("account");
            // console.log(account);
            // console.log("contact");
            // console.log(contact);
            // console.log("profile");
            // console.log(profile);
            // console.log("socialInfo");
            // console.log(socialInfo);

            if (account) {
                // console.log("account :: ");
                this.currentPercentage = 25;
            }

            if (contact) {
                //  console.log("account + contact :: ");
                this.currentPercentage = 50;
            }

            if (profile) {
                //  console.log("account + contact + profile  :: ");
                this.currentPercentage = 75;
            }

            if (socialInfo) {
                // console.log("account + contact + profile + social :: ");
                this.currentPercentage = 100;
            }

            this.loading = false;

        }).catch(error => {
            // console.log(error);
            this.loading = false;
        });
    }


    changeCurrentProfilePercentage() {
        this.getProfile();
    }

    editProfile(formData) {
        // console.log("1");
        // console.log(this.imageUploader.queue);
        this.loading = true;
        // console.log("edit profile sdsdsdsds..." + JSON.stringify(formData[0]));
        this.accountSettingService.editSellerProfile(formData[0]).then(res => {
            this.loading = false;
            // console.log("2");

            this.toastr.success('Updated successfully!');
            this.setLatestUsername();
            let data = JSON.parse(localStorage.getItem("currentUser"));
            data.first_name = formData[0].first_name;
            data.last_name = formData[0].last_name;
            data.email = formData[0].email;
            data.display_name = formData[0].display_name;
            data.address = formData[0].address;
            data.phone = formData[0].phone;
            data.experience_level = formData[0].experience_level;
            data.visibilty = formData[0].visibilty;
            localStorage.setItem("currentUser", JSON.stringify(data));

            if (this.imageUploader.queue.length) {
                // console.log(this.imageUploader.queue)
                // console.log("3");
                this.loading = true;
            }
            else {
                // console.log("4");
                this.loading = false;
                window.location.reload();
            }
            this.uploadImages();
        }).catch(error => {
            // console.log("5");
            // console.log("error value: " + error);
            this.loading = false;
            this.toastr.error('Error occured' + '!', 'Oops!');
        });
    }

    getSocialAccount() {
        this.loading = true;
        this.accountSettingService.getSocialAccountInfo().then(res => {
            this.social = res;
            this.loading = false;
            //  console.log("social account::" + JSON.stringify(this.social));
        }).catch(error => {
            this.loading = false;
            // console.log("error value: ", 'Error');
        });
    }

    editSocialAccount(formData) {
        this.loading = true;
        // console.log("form value of seller account" + JSON.stringify(formData));
        this.accountSettingService.editSocialAccountInfo(formData).then(res => {
            // this.social = res;
            this.getSocialAccount();
            this.loading = false;
            this.toastr.success('Updated successfully!');
            this.setLatestUsername();

        }).catch(error => {
            //console.log("error value: " + error);
            this.loading = false;
            this.toastr.error(error._body + '!', 'Oops!');
        });
    }

    onUploadImageFailed(item: FileLikeObject, filter: any, options: any) {
        //console.log(item.type);
        switch (filter.name) {
            case 'fileSize':
                this.errorMessage = 'Maximum upload size 8 MB allowed';
                //  console.log(this.errorMessage);
                break;
            case 'mimeType':
                const allowedTypes = this.allowedImageType.join();
                this.errorMessage = 'Type ' + item.type + ' is not allowed. Allowed types:' + allowedTypes;
                //console.log(this.errorMessage);
                break;
            default:
                this.errorMessage = 'Unknown error filter is ' + filter.name;
            //    console.log(this.errorMessage);
        }
    }


    uploadImages() {
        //console.log("image <<<<<<< :: " + JSON.stringify(talent_id));
        //   console.log("image uploader 1 :::" + JSON.stringify(this.imageUploader));
        // console.log("image uploader 2:::" + this.currentUser.id);
        let user_id = this.currentUser.id;

        this.imageUploader.onBuildItemForm = function (fileItem, form) {
            // console.log("upload image function is called :" + fileItem +  form);
            form.append('user_id', user_id);
            //    service method to announce latest image
            // this.sharedService.announceImagePath(fileItem.file.name);
            //
            return { fileItem, form }
            //console.log("upload image :::: " + this.fileItem);
            //    fileItem.remove();
        };

        this.imageUploader.uploadAll();
    }

    setLatestImage() {
        this.sharedService.announceImagePath();
    }

    setLatestUsername() {
        this.sharedService.announceUsername();
    }


}
