import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// import {ResetPasswordService} from './service/reset-password.service';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import {FileUploadModule} from "ng2-file-upload";
import { AccountSettingComponent } from './account-setting.component';
import { LoadingModule } from 'ngx-loading';


@NgModule({
  declarations: [
    AccountSettingComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    LoadingModule,
    RouterModule.forChild([
    {
      path: '',
      component: AccountSettingComponent

    }
  ])
],
providers: [],
bootstrap: [AccountSettingComponent]

})
export class AccountSettingModule { }
