import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { SharedService } from '../../../../services/shared.service';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
declare var $:any;

@Injectable()
export class AccountSettingService {

    headers:Headers;
    options:RequestOptions;

    public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
    public serverIp;

    constructor(public http:Http, public sharedService:SharedService) {
       // console.log('data service connected');
        this.serverIp = this.sharedService.apiUrl;
        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.currentUser.token
        });
        this.options = new RequestOptions({headers: this.headers});
    }


    getSellerProfile():Promise<Object> {
       // console.log("seller account function called ******) " + this.currentUser);

        return this.http.get(this.sharedService.apiUrl + '/api/seller/account?user_id='+this.currentUser.id, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    getSellerLatestProfilePic():Promise<Object> {
        // console.log("seller account function called ******) " + this.currentUser);

         return this.http.post(this.sharedService.apiUrl + '/api/seller/seller-profile-image', this.currentUser,  this.options).toPromise()
             .then(this.extractData)
             .catch(this.handleErrorPromise);
    }


    editSellerProfile(body: Object):Promise<Object> {
       // console.log("seller account function called ******) " + this.currentUser);
        return this.http.put(this.sharedService.apiUrl + '/api/seller/account-setting?user_id='+this.currentUser.id, body, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    getSocialAccountInfo():Promise<Object> {
       // console.log("social account information::: ");

        return this.http.get(this.sharedService.apiUrl + '/api/seller/social-media?user_id='+this.currentUser.id, this.options).toPromise()
               .then(this.extractData)
               .catch(this.handleErrorPromise);
    }


    editSocialAccountInfo(body: Object):Promise<Object> {
      //  console.log("seller account function called ******) " + JSON.stringify(body));
        return this.http.put(this.sharedService.apiUrl + '/api/seller/social?user_id='+this.currentUser.id, body, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }


       private extractData(res:Response) {
        let body = res.json();
       // console.log("commercial page : below is the extracted data ******* " + JSON.stringify(body));
        return body || {};
    }

    // below function can be used when deal with Observabled
    private handleErrorPromise(error:Response | any) {
        console.error(error.message || error);
        return Promise.reject(error.message || error);
    }

    private handleErrorObservable(error:Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    }

}
