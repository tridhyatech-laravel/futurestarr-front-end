import { Component, OnInit, ElementRef, Input, Injectable } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CategoriesService } from './service/categories.service';
//import { AddProductService } from '../add-product/service/add-product.service';

declare var $: any;

@Component({
    selector: 'categories',
    templateUrl: './categories.component.html',
    styleUrls: ['./categories.component.css']
})


export class CategoriesComponent implements OnInit {

    public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
    public errorMsg: string = '';
    public categories: any;

    constructor(private el: ElementRef,
        public categoriesService: CategoriesService,
        public router: Router,
        public toaster: ToastrService,
        private _location: Location) {


    }

    ngOnInit() {
        this.talentCategory();
        //  console.log("server ip on categories"+this.categoriesService.serverIp);
    }

    ngAfterContentInit() {
        // this.getSellerMessage();
    }

    goBack() {
        this._location.back();
    }

    talentCategory() {
        // console.log("list of categories :: ");
        this.categoriesService.talentCategories().then(res => {
            this.categories = res;
            this.categories = this.categories.category;
            //console.log("seller plans ^^^^^^^" + JSON.stringify(this.categories));
        }).catch(error => {
            // console.log("error value: " + error);
        });

    }

    editProfile( value = null) {

    }

    editProduct(){

    }

    deleteProduct(){
        
    }

}
