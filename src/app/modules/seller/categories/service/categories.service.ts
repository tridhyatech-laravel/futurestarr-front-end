import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { SharedService } from '../../../../services/shared.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
declare var $:any;

@Injectable()
export class CategoriesService {

    headers:Headers;
    options:RequestOptions;

    public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
    public serverIp;
    constructor(public http:Http, public sharedService:SharedService) {
        this.serverIp = this.sharedService.apiUrl;
       // console.log('data service connected');
        this.headers = new Headers({'Content-Type': 'application/json'});
        //this.headers  = new Headers({'Authorization': 'Bearer '+ this.currentUser.token});
        this.options = new RequestOptions({headers: this.headers});
    }


    talentCategories():Promise<Object> {
        //console.log("seller promote products service function called ******) " + this.currentUser);

        return this.http.get(this.sharedService.apiUrl + '/api/seller/categories', this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    // addProduct(body:Object):Promise<Object> {
    //   // console.log("seller add products service function called ******) " + this.currentUser);
    //
    //    return this.http.post(this.sharedService.apiUrl + '/api/seller/talent?user_id='+this.currentUser.id ,body, this.options).toPromise()
    //        .then(this.extractData)
    //        .catch(this.handleErrorPromise);
    //
    //}




    private handleError(error:any):Promise<any> {
        // console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }



    private extractData(res:Response) {
        let body = res.json();
       // console.log("below is the extracted data of categories ::" + JSON.stringify(body));
        // console.log(body);
        return body || {};
    }

    private handleErrorObservable(error:Response | any) {
        // console.error(error.message || error);
        return Observable.throw(error.message || error);
    }

    private handleErrorPromise(error:Response | any) {
        // console.error(error.message || error);
        return Promise.reject(error.message || error);
    }


}
