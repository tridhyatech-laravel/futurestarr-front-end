import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CategoriesComponent } from './categories.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// import {ResetPasswordService} from './service/reset-password.service';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    CategoriesComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
    {
      path: '',
      component: CategoriesComponent

    }
  ])
],
providers: [],
bootstrap: [CategoriesComponent]

})
export class CategoriesModule { }
