import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { SharedService } from '../../../../services/shared.service';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
declare var $:any;

@Injectable()
export class PromoteProductService {

    headers:Headers;
    options:RequestOptions;
    public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
    public serverIp;
    constructor(public http:Http,
                public sharedService:SharedService) {
        this.serverIp = this.sharedService.apiUrl;
        // console.log('seller promote products service function is called');

        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.currentUser.token
        });
        this.options = new RequestOptions({headers: this.headers});
    }


    showPromoteProduct(id: Object):Promise<Object> {
       // console.log("seller promote products service function called ******) " + this.currentUser);

        return this.http.get(this.sharedService.apiUrl + '/api/seller/promote-products?user_id='+this.currentUser.id+'&number_of_days='+id, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

     deletePromoteProduct(id: Object):Promise<Object> {
        //console.log("s ******) " + id);

        return this.http.put(this.sharedService.apiUrl + '/api/seller/promote-product/delete?promote_id='+id , this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    getPromoteProductUpdate(id: Object):Promise<Object> {
        // console.log("s ******) " + id);

        return this.http.get(this.sharedService.apiUrl + '/api/seller/promote-product/show?promote_id='+id+'&'+'user_id='+this.currentUser.id, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

     editPromoteProduct(body: Object):Promise<Object> {
        // console.log("s ******) " + body);

        return this.http.put(this.sharedService.apiUrl + '/api/seller/promote-product/edit?user_id='+this.currentUser.id, body, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }



    socialAccount():Promise<Object> {
        //console.log("seller promote products service function called ******) " + this.currentUser);

        return this.http.get(this.sharedService.apiUrl + '/api/seller/social-link?user_id='+this.currentUser.id, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    //addPromoteProduct(body: Object):Promise<Object> {
    //    console.log("s ******) " + body);
    //
    //    return this.http.put(this.sharedService.apiUrl + '/api/seller/promoteproduct?talent_id='+body+'user_id='+this.currentUser.id, this.options).toPromise()
    //        .then(this.extractData)
    //        .catch(this.handleErrorPromise);
    //
    //}




    private extractData(res:Response) {
        let body = res.json();
       // console.log("promote page : below is the extracted data ******* " + JSON.stringify(body));
        return body || {};
    }

    // below function can be used when deal with Observabled
    private handleErrorPromise(error:Response | any) {
        console.error(error.message || error);
        return Promise.reject(error.message || error);
    }

    private handleErrorObservable(error:Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    }


}
