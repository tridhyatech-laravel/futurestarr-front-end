import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { FileUploader, FileLikeObject } from 'ng2-file-upload';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { PromoteProductService } from './service/promote-product.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AddProductService } from '../add-product/service/add-product.service';
import { SharedService } from '../../../services/shared.service';

declare var $: any;
//const URL = 'http://18.217.7.143/futurestarr/api/upload/profile2';

@Component({
    selector: 'promote-product',
    templateUrl: './promote-product.component.html',
    styleUrls: ['./promote-product.component.css']
})
export class PromoteProductComponent implements OnInit {


    //public repoUrl = 'https://github.com/Epotignano/ng2-social-share';
    //public imageUrl = 'https://avatars2.githubusercontent.com/u/10674541?v=3&s=200';

    public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
    public errorMsg: string = '';
    public promoteProducts: any = {};
    public errorMessage: string;
    public categories: any;
    public socials: any;
    public products: any = {};
    public days = [30, 50, 80, 100];
    public selectedDay = 30;
    public numberOfDays: any = {};
    public promoteId = {};
    public loading: boolean = true;
    public p = 1;
    public socialName;
    public social: any = {};
    public repoUrl;

    constructor(private el: ElementRef,
        public promoteProductService: PromoteProductService,
        public addProductService: AddProductService,
        public router: Router,
        public toaster: ToastrService,
        private _location: Location,
        public sharedService: SharedService) {


        this.getPromoteProducts();
        this.talentCategory();
        this.getSocialInfo();



    }

    ngOnInit() {
        // this.repoUrl =  window.location.href;
        // console.log(window.location.origin);
    }

    topFunction() {
        $('html, body').animate({ scrollTop: 0 }, 'fast');
    }

    ngAfterContentInit() {
    }

    goBack() {
        this._location.back();
    }

    isImage(new_product_img_path) {
        let images = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'GIF', 'gif'];
        if (new_product_img_path) {
            let fileExtension = new_product_img_path.replace(/^.*\./, '');
            if (images.includes(fileExtension)) {
                return true;
            }
        }
    }


    onSelect(val) {
        // console.log("value of selected field" + val);
        // console.log("*******djgjksgfsk*********" + JSON.stringify(this.numberOfDays));
        // //this.numberOfDays = this.products.filter(x => x.value == val)
        // console.log("after filter ::::::::: " + JSON.stringify(this.numberOfDays));
        this.getPromoteProducts();
    }


    selectedSocialAccount(id) {

        this.repoUrl = window.location.origin + '/talent-mall/product-info/' + id;
        // console.log(this.repoUrl);
    }


    getPromoteProducts() {
        this.loading = true;
        let dayNum = this.selectedDay;
        this.promoteProductService.showPromoteProduct(dayNum).then(res => {
            this.products = res;
            this.numberOfDays = this.products;
            this.promoteId = this.products.promote_id;
            this.loading = false;
        }).catch(error => {
            this.loading = false;
            // console.log("error value: " + error);
        });
    }

    deletePromoteProduct(id) {
        this.loading = true;
        this.promoteProductService.deletePromoteProduct(id).then(res => {
            this.getPromoteProducts();
            this.loading = false;
            this.toaster.success('Deleted successfully!', 'Success');
        }).catch(error => {
            this.loading = false;
            this.toaster.error('Error accoured!!', 'Error');
            // console.log("error value: " + error);
        });
    }


    getPromoteProductUpdate(id, talent_id) {
        this.repoUrl = window.location.origin + '/talent-mall/product-info/' + talent_id;
        this.loading = true;
        this.selectedSocialAccount(talent_id);
        $('#editPromotionModal').modal('show');
        this.promoteProductService.getPromoteProductUpdate(id).then(res => {
            this.promoteProducts = res;
            this.getPromoteProducts();
            this.loading = false;
        }).catch(error => {
            this.loading = false;
            // console.log("error value: " + error);
        });
    }

    getSocialShareMediaName(name) {
        this.socialName = name;
    }


    editPromoteProduct(formData) {
        this.loading = true;
        this.social = formData;
        this.social.social_name = this.socialName;
        this.promoteProductService.editPromoteProduct(this.social).then(res => {
            // console.log("Deleted successfullly");
            $('#editPromotionModal').modal('hide');
            this.getPromoteProducts();
            this.loading = false;
            this.toaster.success('Edit promote product successfully!', 'Success');
        }).catch(error => {
            this.loading = false;
            this.toaster.error('Product not deleted!!', 'Error');
            // console.log("error value: " + error);
        });
    }


    talentCategory() {
        this.addProductService.talentCategories().then(res => {
            this.categories = res;
            this.categories = this.categories.category;
        }).catch(error => {
            // console.log("error value: " + error);
        });

    }

    getSocialInfo() {
        this.promoteProductService.socialAccount().then(res => {
            this.socials = res;
            //console.log("seller social ^^^^^^^" + JSON.stringify(this.socials));
        }).catch(error => {
            // console.log("error value: " + error);
        });

    }


    //addSocialInfo(user) {
    //
    //   // console.log("social add function :: " + JSON.stringify(user));
    //
    //   this.promoteProductService.addPromoteProduct(user).then(res => {
    //       this.products = res;
    //
    //      // console.log("seller social ^^^^^^^" + JSON.stringify(this.products));
    //   }).catch(error => {
    //       console.log("error value: " + error);
    //   });
    //}


}
