import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// import {ResetPasswordService} from './service/reset-password.service';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import {FileUploadModule} from "ng2-file-upload";
import { PromoteProductComponent } from './promote-product.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { LoadingModule } from 'ngx-loading';
// import { CeiboShare } from './ng2-social-share';
import { SharedModule } from '../../shared/shared.module';
import {ShareButtonsModule} from 'ngx-sharebuttons';

@NgModule({
  declarations: [
    PromoteProductComponent
    //CeiboShare
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    LoadingModule,
    FileUploadModule,
    SharedModule,
    ShareButtonsModule.forRoot(),
    RouterModule.forChild([
    {
      path: '',
      component: PromoteProductComponent

    }
  ])
],
providers: [],
bootstrap: [PromoteProductComponent]

})
export class PromoteProductModule { }
