import { Component, OnInit ,ElementRef, Input, Injectable} from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ChangePasswordService } from './service/change-password.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

declare var $:any;

@Component({
    selector: 'change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.css']
})


export class ChangePasswordComponent implements OnInit {

    public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
    public errorMsg:string = '';
    public user = {
        pass :'',
        newpassword : '',
        confirmpassword : ''
    };
    public errMsg:string = '';
    //public showMsg:boolean = false ;
   //  public validateEqual: string;
    //public pass = {};

    constructor(private el:ElementRef,
                public changePasswordService:ChangePasswordService,
                public router:Router,
                public toaster:ToastrService,
                private _location: Location) {


    }

    ngOnInit() {
    //this.getSellerMessage();

    }


    ngAfterContentInit() {
        //this.getSellerMessage();
    }

     goBack(){
      this._location.back();
  }

     topFunction() {
    $('html, body').animate({ scrollTop: 0 }, 'fast');
    }


    //
    //Validate(newValue) {
    //  this.pass = newValue;
    //    console.log("pass value validation : " + newValue);
    //  //this.Platform.ready().then(() => {
    //  //   this.rootRef.child("users").child(this.UserID).child('range').set(this.range)
    //  //})
    //}


    changePassword(formData) {
        // console.log("seller *************" + JSON.stringify(formData));
     //   this.showMsg = false;

        //if(formData.newpassword == formData.confirmpassword ){
        //    this.showMsg = true;
          this.changePasswordService.resetPassword(formData).then(res => {
            //this.user = res;
            // console.log("seller message ^^^^^^^" + JSON.stringify(res));
              // formData.reset();
               this.toaster.success('Updated successfully!', '!' + 'Success');



        }).catch(error => {
            // console.log("error value: " + error);
          this.toaster.error(error._body + '!', 'Oops!');
        });
        //} else{
        //    this.showMsg = false;
        //    this.errMsg = 'confirm password is not matched !';
        //}


    }


}
