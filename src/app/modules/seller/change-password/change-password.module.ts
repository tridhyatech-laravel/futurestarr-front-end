import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { FileUploadModule } from "ng2-file-upload";
import { ChangePasswordComponent } from './change-password.component';
import { ChangePasswordService } from './service/change-password.service';


@NgModule({
  declarations: [
    ChangePasswordComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    RouterModule.forChild([
    {
      path: '',
      component: ChangePasswordComponent

    }
  ])
],
providers: [ChangePasswordService],
bootstrap: [ChangePasswordComponent]

})
export class ChangePasswordModule { }
