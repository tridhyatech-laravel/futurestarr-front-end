import { Component, OnInit ,ElementRef, Input} from '@angular/core';
// import { FileUploader , FileLikeObject} from 'ng2-file-upload';
// import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
// import { Router } from '@angular/router';
// import { ToastrService } from 'ngx-toastr';
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SharedService } from '../../../services/shared.service';
declare var $:any;

@Component({
  selector: 'app-award',
  templateUrl: './award.component.html',
  styleUrls: ['./award.component.css']
})
export class AwardComponent implements OnInit {

  talentAwardUsers:any;

  constructor(public sharedService:SharedService) {
    
  }

  ngOnInit() {
    // this.sellerTalentAwardUsers();
  }

  sellerTalentAwardUsers(){
    this.sharedService.getSellerTalentAwardUsers().then( data=>{
      if(data['success']){
        this.talentAwardUsers = data['awardUsers'];
      }
      else{
        this.talentAwardUsers = null;
      }
    })
    .catch( error=>{
      this.talentAwardUsers = null;
    })
  }

  showAwardModal(){
    this.talentAwardUsers = [];
    this.sellerTalentAwardUsers();
    $('#awardModal').modal('show');
  }

  hideAwardModal() {
    this.talentAwardUsers = [];
    $('#awardModal').modal('hide');
  }


}
