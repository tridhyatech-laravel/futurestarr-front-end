import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import {FileUploadModule} from "ng2-file-upload";
import { SaleComponent } from './sale.component';
import { LoadingModule } from 'ngx-loading';
import { SalesService } from './sale/sales.service';


@NgModule({
  declarations: [
    SaleComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    FormsModule,
    LoadingModule,
    ReactiveFormsModule,
    FileUploadModule,
    RouterModule.forChild([
    {
      path: '',
      component: SaleComponent

    }
  ])
],
providers: [SalesService],
bootstrap: [SaleComponent]

})
export class SaleModule { }
