import { Component, OnInit ,ElementRef, Input} from '@angular/core';
import { FileUploader , FileLikeObject} from 'ng2-file-upload';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { SharedService } from '../../../services/shared.service';
import { SalesService } from './sale/sales.service';

declare var $:any;


@Component({
  selector: 'sale',
   providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.css']
})


export class SaleComponent implements OnInit {

    public sales:any = {};
    public loading:boolean = false;


  constructor(private _location: Location,
              public sharedService:SharedService,
              public salesService:SalesService) {

  this.getSalesDetialed();

  }

  ngOnInit() {

  }

  goBack(){
      this._location.back();
  }

    topFunction() {
     $('html, body').animate({ scrollTop: 0 }, 'fast');
 }

  getSalesDetialed() {
        this.loading = true;

        this.salesService.showSalesDetail().then(res => {
            this.sales = res;
        //    console.log(res);

            this.loading = false;
        }).catch(error => {
            this.loading = false;
            // console.log("error value: " + error);
        });
    }








}
