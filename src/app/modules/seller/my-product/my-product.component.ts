import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { FileUploader, FileLikeObject, FileItem, ParsedResponseHeaders } from 'ng2-file-upload';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { MyProductService } from './service/my-product.service';
import { Router, RouterLinkActive } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AccountSettingService } from '../account-setting/service/account-setting.service';
import { AddProductService } from '../add-product/service/add-product.service';


declare var $: any;
//const URL = 'http://18.217.7.143/futurestarr/api/upload/profile2';

@Component({
    selector: 'my-product',
    templateUrl: './my-product.component.html',
    styleUrls: ['./my-product.component.css']
})
export class MyProductComponent implements OnInit {

    //public repoUrl = 'https://github.com/Epotignano/ng2-social-share';
    //public imageUrl = 'https://avatars2.githubusercontent.com/u/10674541?v=3&s=200';

    public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
    public errorMsg: string = '';
    public products: any = {};
    public errorMessage: string;
    public showProduct: boolean = false;
    public days = [30, 50, 80, 100,];
    // public selectedDay = 30;
    public selectedDay = 0;
    public numberOfDays: any = {};
    public promoteId;
    public promoteProducts: any = {};
    public social: any = {};
    // public social_id: any = {};
    public loading: boolean = true;
    public p = 1;
    public socialName;
    showPriceError:boolean = false;

    // ***** add Product  ******
    public categories: any;
    public modalProduct: any;
    public talent_id: any;
    public selectedCategory = 'Author';
    public listOfCategories: any = {};

    public repoUrl;
    commercialFileRequired: boolean = false;
    productIdToDelete: any = null;

    public allowedVideoType = ['video/mp4', 'audio/mp3'];
    public maxVideoSize = 100 * 1024 * 1024; // 100 MB
    public videoUploader: FileUploader = new FileUploader({
        url: this.addProductService.serverIp + '/api/seller/video',
        itemAlias: 'video',
        removeAfterUpload: false,
        // allowedMimeType: this.allowedVideoType,
        allowedFileType: ['video', 'audio'],
        maxFileSize: this.maxVideoSize,
        autoUpload: false,
        method: "POST",
        queueLimit: 1
        //headers: [{name: 'Authorization', value: 'Bearer ' + 'token-here'}, {
        //    name: 'Content-Type',
        //    value: 'multipart/form-data'
        //}],

    });


    // ****** Add Product upload files

    // image variables
    public allowedImageType = ['image/png', 'image/gif', 'image/jpeg', 'video/mp4'];
    public maxImageSize = 100 * 1024 * 1024; // 100 MB
    public imageUploader: FileUploader = new FileUploader({
        url: this.myProductService.serverIp + '/api/seller/image',
        itemAlias: 'image',
        method: "POST",
        // allowedMimeType: this.allowedImageType,
        allowedFileType: ['video', 'image'],
        maxFileSize: this.maxImageSize,
        autoUpload: false,
        removeAfterUpload: false,
        queueLimit: 1
    });


    // video variables
    public allowedProductType = ['video/mp4', 'audio/mp3'];
    public maxProductSize = 100 * 1024 * 1024; // 100 MB
    public productUploader: FileUploader = new FileUploader({
        url: this.myProductService.serverIp + '/api/seller/video',
        itemAlias: 'video',
        removeAfterUpload: false,
        // allowedMimeType: this.allowedProductType,
        allowedFileType: ['video', 'audio'],
        maxFileSize: this.maxProductSize,
        autoUpload: false,
        method: "POST",
        queueLimit: 1
        //headers: [{name: 'Authorization', value: 'Bearer ' + 'token-here'}, {
        //    name: 'Content-Type',
        //    value: 'multipart/form-data'
        //}],

    });

    // pdf/doc variables
    public allowedDocType = ['application/pdf', 'application/docx', 'application/doc', 'video/mp4', 'audio/mp3'];
    public maxDocSize = 100 * 1024 * 1024; // 100 MB
    public pdfUploader: FileUploader = new FileUploader({
        url: this.myProductService.serverIp + '/api/seller/pdf',
        removeAfterUpload: false,
        itemAlias: 'pdf',
        // allowedMimeType: this.allowedDocType,
        allowedFileType: ['video', 'pdf', 'doc', 'image', 'audio'],
        maxFileSize: this.maxDocSize,
        autoUpload: false,
        method: "POST",
        // queueLimit: 1
        //headers: [{name: 'Authorization', value: 'Bearer ' + 'token-here'}, {
        //    name: 'Content-Type',
        //    value: 'multipart/form-data'
        //}],

    });


    constructor(private el: ElementRef,
        public myProductService: MyProductService,
        public router: Router,
        public toaster: ToastrService,
        private _location: Location,
        public accountSettingService: AccountSettingService,
        public addProductService: AddProductService) {


        if (this.currentUser && this.currentUser.role_id == "seller") {

        }
        else {
            this.router.navigate(['/']);
        }
        // video uploader functions
        this.videoUploader.onWhenAddingFileFailed = (item, filter, options) => this.onUploadVideoFailed(item, filter, options);
        this.videoUploader.onAfterAddingFile = (fileItem) => {
            fileItem.withCredentials = false;
            // //console.log(fileItem.file.type);
        };

        this.videoUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            // //console.log("Video:uploaded:", item, status, response);

        };


        // ****** add Product *****


        // image uploader functions
        this.imageUploader.onWhenAddingFileFailed = (item, filter, options) => this.onUploadImageFailed(item, filter, options);

        this.imageUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            // //console.log("ImageUpload:uploaded:", item, status, response);
            ////console.log("test data ++++++++++" + item.formData[0].talent_id);

        };

        this.imageUploader.onAfterAddingFile = (fileItem) => {
            fileItem.withCredentials = false;
            // //console.log("item value of image :: " + fileItem);
            // //console.log(fileItem.file.type);
        };


        // video uploader functions
        this.productUploader.onWhenAddingFileFailed = (item, filter, options) => this.onUploadProductFailed(item, filter, options);
        this.productUploader.onAfterAddingFile = (fileItem) => {
            fileItem.withCredentials = false;
            // //console.log(fileItem.file.type);
            // //console.log("data is added......");
            //this.uploadVideos();
        };
        this.productUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            // //console.log("Video:uploaded:", item, status, response);
        };


        // pdf/doc uploader functions
        this.pdfUploader.onWhenAddingFileFailed = (item, filter, options) => this.onUploadPdfFailed(item, filter, options);
        this.pdfUploader.onAfterAddingFile = (fileItem) => {
            fileItem.withCredentials = false;
            // console.log(fileItem.file.type);
            // this.uploadDocs();
        };
        this.pdfUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            // //console.log("pdfUploaded:uploaded:", item, status, response);
        };

        // video uploader functions
        this.videoUploader.onWhenAddingFileFailed = (item, filter, options) => this.onUploadVideoFailed(item, filter, options);
        this.videoUploader.onAfterAddingFile = (fileItem) => {
            fileItem.withCredentials = false;
            // //console.log(fileItem.file.type);
            // //console.log("data is added......");
            //this.uploadVideos();
        };
        this.videoUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            // //console.log("Video:uploaded:", item, status, response);
        };


        this.talentCategory();

        this.imageUploader.onErrorItem = (item, response, status, headers) => this.onImageErrorItem(item, response, status, headers);
        this.imageUploader.onSuccessItem = (item, response, status, headers) => this.onImageSuccessItem(item, response, status, headers);

        this.videoUploader.onErrorItem = (item, response, status, headers) => this.onVideoErrorItem(item, response, status, headers);
        this.videoUploader.onSuccessItem = (item, response, status, headers) => this.onVideoSuccessItem(item, response, status, headers);

        this.pdfUploader.onErrorItem = (item, response, status, headers) => this.onPdfErrorItem(item, response, status, headers);
        this.pdfUploader.onSuccessItem = (item, response, status, headers) => this.onPdfuccessItem(item, response, status, headers);

        this.productUploader.onErrorItem = (item, response, status, headers) => this.onProductErrorItem(item, response, status, headers);
        this.productUploader.onSuccessItem = (item, response, status, headers) => this.onProductuccessItem(item, response, status, headers);
    }

    onImageSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let data = JSON.parse(response); //success server response
        // this.imageUploader.queue = [];
        this.loading = false;
        if (this.imageUploader.queue.length > 0 || this.videoUploader.queue.length > 0 ||
            this.productUploader.queue.length > 0 || this.pdfUploader.queue.length > 0) {
            this.loading = true;
        }
        else {
            this.loading = false;
            this.toaster.success("Product added");
            this.getProducts();
        }

    }

    onImageErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let error = JSON.parse(response); //error server response
        this.loading = false;
        if (this.imageUploader.queue.length > 0 || this.videoUploader.queue.length > 0 ||
            this.productUploader.queue.length > 0 || this.pdfUploader.queue.length) {
            this.loading = true;
        }
        else {
            this.loading = false;
            //console.log("loader from image");
            this.toaster.error("Product could not added");
        }
    }

    onVideoSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let data = JSON.parse(response); //success server response
        // this.videoUploader.queue = [];
        this.loading = false;
        if (this.imageUploader.queue.length > 0 || this.videoUploader.queue.length > 0 ||
            this.productUploader.queue.length > 0 || this.pdfUploader.queue.length > 0) {
            this.loading = true;
        }
        else {
            this.loading = false;
            this.getProducts();
            //console.log("loader from video");
            this.toaster.success("Product added");
        }

    }

    onVideoErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let error = JSON.parse(response); //error server response
        this.loading = false;
        if (this.imageUploader.queue.length > 0 || this.videoUploader.queue.length > 0 ||
            this.productUploader.queue.length > 0 || this.pdfUploader.queue.length) {
            this.loading = true;
        }
        else {
            this.loading = false;
            this.toaster.error("Product could not added");
        }
    }

    onPdfuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let data = JSON.parse(response); //success server response
        // this.pdfUploader.queue = [];
        this.loading = false;
        if (this.imageUploader.queue.length > 0 || this.videoUploader.queue.length > 0 ||
            this.productUploader.queue.length > 0 || this.pdfUploader.queue.length > 0) {
            this.loading = true;
        }
        else {
            this.loading = false;
            this.getProducts();
            //console.log("loader from pdf");
            this.toaster.success("Product added");
        }

    }

    onPdfErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let error = JSON.parse(response); //error server response
        this.loading = false;
        if (this.imageUploader.queue.length > 0 || this.videoUploader.queue.length > 0 ||
            this.productUploader.queue.length > 0 || this.pdfUploader.queue.length) {
            this.loading = true;
        }
        else {
            this.loading = false;
            this.toaster.error("Product could not added");
        }
    }

    onProductuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let data = JSON.parse(response); //success server response
        // this.productUploader.queue = [];
        this.loading = false;
        if (this.imageUploader.queue.length > 0 || this.videoUploader.queue.length > 0 ||
            this.productUploader.queue.length > 0 || this.pdfUploader.queue.length > 0) {
            this.loading = true;
        }
        else {
            this.loading = false;
            this.getProducts();
            //console.log("loader from product");
            this.toaster.success("Product added");
        }

    }

    onProductErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let error = JSON.parse(response); //error server response
        this.loading = false;
        if (this.imageUploader.queue.length > 0 || this.videoUploader.queue.length > 0 ||
            this.productUploader.queue.length > 0 || this.pdfUploader.queue.length) {
            this.loading = true;
        }
        else {
            this.loading = false;
            this.toaster.error("Product could not added");
        }
    }

    uploadVideos(talent_id) {
        //this.videoUploader.uploadAll();

        // //console.log("Cal uploadVideo function..." + JSON.stringify(talent_id));
        // //console.log("image uploader :::" +  this.imageUploader);

        // //console.log("image uploader 1 :::" + this.videoUploader);
        // //console.log("image uploader 2:::" + this.videoUploader.queue);

        //this.videoUploader.queue.forEach(function (fileItem) {
        //    //console.log("video upload" + fileItem);
        //    fileItem.formData.push({talent_id: talent_id});
        //    //fileItem.formData = talent_id;
        //});

        this.videoUploader.onBuildItemForm = function (fileItem, form) {
            form.append('talent_id', talent_id);
            fileItem.remove();

            // //console.log("checked talent_id:::" + fileItem);
            //return {fileItem, form}
            // fileItem.remove();
        };
        this.videoUploader.uploadAll();
    }


    ngOnInit() {
        this.getProducts();
        this.getSocialAccount();
        $('#deleteConfirmModal').on('hidden.bs.modal', function () {
            this.productIdToDelete = false;
            //console.log("this.productIdToDelete - "+this.productIdToDelete);

        })
    }

    topFunction() {
        $('html, body').animate({ scrollTop: 0 }, 'fast');
    }

    ngAfterContentInit() {
        //this.getProducts();
    }

    goBack() {
        this._location.back();
    }

    showProductPage() {
        //////console.log("show product edit page...");
        this.showProduct = true;
    }

    onSelect(val) {
        // //console.log("after filter ::::::::: " + JSON.stringify(this.numberOfDays));
        this.getProducts();
    }

    getProducts() {
        // //console.log("Products *************");
        let dayNum = this.selectedDay;
        this.loading = true;

        this.myProductService.sellerGetProduct(dayNum).then(res => {
            this.products = res;
            this.products = this.products.talents;
            this.numberOfDays = this.products;
            this.loading = false;

            // //console.log("seller plans fot buy a product^^^^^^^" + JSON.stringify(this.products));

        }).catch(error => {
            // //console.log("error value: " + error);
            this.loading = false;
        });
    }

    editProduct(id) {
        // //console.log("edit id of product..." + id);
        this.router.navigate(['seller/my-product/edit-product/' + id]);
    }

    deleteProduct(id) {
        //console.log("this.productIdToDelete aa - "+this.productIdToDelete);
        this.productIdToDelete = id;
        //console.log("this.productIdToDelete bb - "+this.productIdToDelete);
        $('#deleteConfirmModal').modal('show');
        // this.loading = true;
        // // //console.log("get product id :: " + id);
        // this.myProductService.sellerDeleteProduct(id).then(res => {
        //     this.loading = false;
        //     this.getProducts();
        //     // //console.log("deleted successfullly");
        //     this.toaster.success('Deleted successfully!');

        // }).catch(error => {
        //     //console.log("error value: " + error);
        //     this.loading = false;
        //     this.toaster.error('Get error!');
        // });
    }

    confirmDelete() {
        if (this.productIdToDelete) {

            this.loading = true;
            // //console.log("get product id :: " + id);
            this.myProductService.sellerDeleteProduct(this.productIdToDelete).then(res => {
                this.loading = false;
                this.getProducts();
                $('#deleteConfirmModal').modal('hide');
                // //console.log("deleted successfullly");
                this.toaster.success('Deleted successfully!');

            }).catch(error => {
                $('#deleteConfirmModal').modal('hide');
                //console.log("error value: " + error);
                this.loading = false;
                this.toaster.error('Get error!');
            });
        }
        else {
            $('#deleteConfirmModal').modal('hide');
        }
    }

    showPromoteModal(id) {
        ////console.log("modal id :: " + id);

        this.promoteId = id;
        this.repoUrl = window.location.origin + '/talent-mall/product-info/' + this.promoteId;
        // //console.log(this.repoUrl);

        $('#promotionModal').modal('show');
    }

    getSocialShareMediaName(name) {
        this.socialName = name;
        //  //console.log('>>>>>>>>>>>>>>>>>>>>>>> ' + this.socialName);
    }

    addPromote(formData) {
        let id = this.promoteId;
        //this.loading = true;
        this.promoteProducts = formData;
        this.promoteProducts.social_name = this.socialName;
        this.loading = true;
        //   //console.log("promoteProducts:::::::::::  " + JSON.stringify(this.promoteProducts));
        this.myProductService.addPromoteProduct(id, this.promoteProducts).then(res => {
            // this.myProductService.addPromoteProduct(id, formData).then(res => {
            this.loading = false;
            $('#promotionModal').modal('hide');
            this.getProducts();
            //  formData.reset();
            this.toaster.success('Added successfully!');

        }).catch(error => {
            ////console.log("error value: " + error);
            this.toaster.error('Get error!');
            this.loading = false;
        });
    }

    getSocialAccount() {
        // //console.log("social account :::::::::::::::: + ");
        this.accountSettingService.getSocialAccountInfo().then(res => {
            this.social = res;
            ////console.log("social account::" + JSON.stringify(this.social));
        }).catch(error => {
            ////console.log("error value: " + error);
        });
    }

    onUploadVideoFailed(item: FileLikeObject, filter: any, options: any) {
        ////console.log(item.type);
        switch (filter.name) {
            case 'fileSize':
                this.errorMessage = 'Maximum upload size 100 MB allowed';
                this.toaster.error(this.errorMessage);
                // //console.log(this.errorMessage);
                break;
            case 'mimeType':
                const allowedTypes = this.allowedVideoType.join();
                this.errorMessage = 'Type ' + item.type + ' is not allowed. Allowed types:' + allowedTypes;
                this.toaster.error(this.errorMessage);
                // //console.log(this.errorMessage);
                break;
            default:
                this.errorMessage = 'Video and Audio are allowed';
                this.toaster.error(this.errorMessage);
            // //console.log(this.errorMessage);
        }
    }

    //********** add product ********
    showProductModal() {
        //  this.promoteId = id;
        $('#productModal').modal('show');
    }

    onSelectCategories(val) {
        // //console.log("value of selected field  " + val);
        // //console.log("*******list of categories*********" + JSON.stringify(this.listOfCategories));
    }

    talentCategory() {
        // //console.log("list of categories :: ");

        ////console.log("Products *************");
        this.addProductService.talentCategories().then(res => {
            this.categories = res;
            this.categories = this.categories.category;

            //  //console.log("seller categories ^^^^^^^" + JSON.stringify(this.categories));
        }).catch(error => {
            // //console.log("error value: " + error);
        });
    }

    addProduct(formData) {
        // //console.log(formData.value);
        if (this.imageUploader.queue.length > 0) {
            this.commercialFileRequired = false;
            this.loading = true;
            this.addProductService.addProduct(formData.value).then(res => {


                this.selectedCategory = 'Author';
                if (this.imageUploader.queue.length > 0 || this.videoUploader.queue.length > 0 ||
                    this.productUploader.queue.length > 0 || this.pdfUploader.queue.length > 0) {
                    this.loading = true;
                }
                else {
                    this.loading = false;
                    //console.log("this.imageUploader.queue.length - "+this.imageUploader.queue.length);
                    //console.log("this.videoUploader.queue.length - "+this.videoUploader.queue.length);
                    //console.log("this.productUploader.queue.length - "+this.productUploader.queue.length);
                    //console.log("this.pdfUploader.queue.length - "+this.pdfUploader.queue.length);
                    //console.log("loader from addProducts");
                    this.getProducts();
                    this.toaster.success('Added successfully!');
                }
                this.uploadImages(res);
                //this.uploadProduct(res);
                this.uploadDocs(res);
                this.uploadVideos(res);
                this.talentCategory();
                formData.reset();

                $('#productModal').modal('hide');


                //this.toaster.success('We have sent an email. Please check your inbox!', 'Success!');
            }).catch(error => {
                //console.log(error._body);
                this.selectedCategory = 'Author';
                this.loading = false;
                this.toaster.error(error._body + '!', 'Oops!');
            });
        }
        else {
            this.commercialFileRequired = true;
        }
    }

    onUploadImageFailed(item: FileLikeObject, filter: any, options: any) {
        // //console.log("onUploadImagesFailed :::::::::" + item.type);

        switch (filter.name) {
            case 'fileSize':
                this.errorMessage = 'Maximum upload size 100 MB allowed';
                // //console.log(this.errorMessage);
                this.toaster.error(this.errorMessage);
                break;
            case 'mimeType':
                const allowedTypes = this.allowedImageType.join();
                this.errorMessage = 'Type ' + item.type + ' is not allowed. Allowed types:' + allowedTypes;
                // //console.log(this.errorMessage);
                this.toaster.error(this.errorMessage);
                break;
            default:
                this.errorMessage = 'Only video and Image are allowed';
                this.toaster.error(this.errorMessage);
            // //console.log(this.errorMessage);
        }
    }

    onUploadProductFailed(item: FileLikeObject, filter: any, options: any) {
        ////console.log(item.type);
        switch (filter.name) {
            case 'fileSize':
                this.errorMessage = 'Maximum upload size 100 MB allowed';
                // //console.log(this.errorMessage);
                this.toaster.error(this.errorMessage);
                break;
            case 'mimeType':
                const allowedTypes = this.allowedProductType.join();
                this.errorMessage = 'Type ' + item.type + ' is not allowed. Allowed types:' + allowedTypes;
                // //console.log(this.errorMessage);
                this.toaster.error(this.errorMessage);
                break;
            default:
                this.errorMessage = 'Only video and audio are allowed';
                this.toaster.error(this.errorMessage);
            // //console.log(this.errorMessage);
            
        }
    }

    onUploadPdfFailed(item: FileLikeObject, filter: any, options: any) {
        ////console.log(item.type);
        switch (filter.name) {
            case 'fileSize':
                this.errorMessage = 'Maximum upload size 100 MB allowed';
                this.toaster.error(this.errorMessage);
                // //console.log(this.errorMessage);
                break;
            case 'mimeType':
                const allowedTypes = this.allowedDocType.join();
                this.errorMessage = 'Type ' + item.type + ' is not allowed. Allowed types:' + allowedTypes;
                // //console.log(this.errorMessage);
                this.toaster.error(this.errorMessage);
                break;
            default:
                this.errorMessage = 'Video, Audio, Image, Pdf and Doc files are allowed';
                this.toaster.error(this.errorMessage);
            //console.log(this.errorMessage);
        }
    }

    uploadImages(talent_id) {
        // //console.log("image <<<<<<< :: " + JSON.stringify(talent_id));
        // //console.log("image uploader :::" +  this.imageUploader);

        // //console.log("image uploader 1 :::" + this.imageUploader);
        // //console.log("image uploader 2:::" + this.imageUploader.queue);

        this.imageUploader.onBuildItemForm = function (fileItem, form) {
            form.append('talent_id', talent_id);
            fileItem.remove();
            //return {fileItem, form}
            // fileItem.remove();
        };

        //this.uploader.queue[idx].upload();

        this.imageUploader.uploadAll();
    }

    uploadProduct(talent_id) {
        //this.videoUploader.uploadAll();

        // //console.log("Cal uploadVideo function..." + JSON.stringify(talent_id));
        // //console.log("image uploader :::" +  this.imageUploader);

        // //console.log("image uploader 1 :::" + this.productUploader);
        // //console.log("image uploader 2:::" + this.productUploader.queue);

        this.videoUploader.onBuildItemForm = function (fileItem, form) {
            form.append('talent_id', talent_id);
            fileItem.remove();

            // //console.log("checked talent_id:::" + fileItem);
            //return {fileItem, form}
            // fileItem.remove();
        };

        this.productUploader.uploadAll();
    }

    uploadDocs(talent_id) {
        // this.pdfUploader.uploadAll();

        // //console.log("pdf <<<<<<<< :: " + JSON.stringify(talent_id));
        // //console.log("image uploader :::" +  this.imageUploader);

        // //console.log("image uploader 1 :::" + this.pdfUploader);
        // //console.log("image uploader 2:::" + this.pdfUploader.queue);

        this.pdfUploader.onBuildItemForm = function (fileItem, form) {
            form.append('talent_id', talent_id);
            fileItem.remove();
            //return {fileItem, form}
            // fileItem.remove();
        };
        this.pdfUploader.uploadAll();
    }

    isImage(new_product_img_path) {
        let images = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'GIF', 'gif'];
        if (new_product_img_path) {
            let fileExtension = new_product_img_path.replace(/^.*\./, '');
            if (images.includes(fileExtension)) {
                return true;
            }
        }
    }

}
