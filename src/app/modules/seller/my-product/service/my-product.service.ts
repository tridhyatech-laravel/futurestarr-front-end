import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { SharedService } from '../../../../services/shared.service';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
declare var $:any;

@Injectable()
export class MyProductService {

    headers:Headers;
    options:RequestOptions;
    public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
    public serverIp;
    constructor(public http:Http,
                public sharedService:SharedService) {
        this.serverIp = this.sharedService.apiUrl;  
        // console.log('seller products service function is called');

        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.currentUser.token
        });
        this.options = new RequestOptions({headers: this.headers});
    }


    sellerGetProduct(id:Object):Promise<Object> {
        // console.log("seller products service function called ******) " + this.currentUser);

        return this.http.get(this.sharedService.apiUrl + '/api/seller/products/show?user_id='+this.currentUser.id+'&number_of_days='+id, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    sellerDeleteProduct(body: Object):Promise<Object> {
        // console.log("s ******) " + body);

        return this.http.put(this.sharedService.apiUrl + '/api/seller/product?talent_id='+body, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    talentCategories():Promise<Object> {
        //console.log("seller promote products service function called ******) " + this.currentUser);

        return this.http.get(this.sharedService.apiUrl + '/api/seller/categories', this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

     showProduct(talent_id:Object):Promise<Object> {
        // console.log("this.talent_id ******) " + talent_id);

        return this.http.get(this.sharedService.apiUrl + '/api/seller/product?talent_id='+talent_id, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

     addPromoteProduct(id, body):Promise<Object> {
       // console.log("seller promote products service function called ******) " + this.currentUser);

        return this.http.post(this.sharedService.apiUrl + '/api/seller/promote-product/add?user_id='+this.currentUser.id+'&promote_id='+id,body, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }


    updateProduct(body, id):Promise<Object> {
       // console.log("seller promote products service function called ******) " + this.currentUser);

        return this.http.put(this.sharedService.apiUrl + '/api/seller/product/edit?user_id='+this.currentUser.id+'&talent_id='+id,body, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    getDeletedProducts(id:Object):Promise<Object> {
        // console.log("seller products service function called ******) " + this.currentUser);

        return this.http.get(this.sharedService.apiUrl + '/api/seller/product/delete?user_id='+this.currentUser.id+'&number_of_days='+id, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    removeProduct(item:Object):Promise<Object> {
    //    console.log("seller  ******) " + JSON.stringify(item));

        return this.http.post(this.sharedService.apiUrl + '/api/seller/product/delete/permanent?user_id='+this.currentUser.id, item, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    undeleteProduct(item:Object):Promise<Object> {
    //    console.log("seller  ******) " + JSON.stringify(item));

        return this.http.put(this.sharedService.apiUrl + '/api/seller/products/undelete?user_id='+this.currentUser.id, item, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }



    private extractData(res:Response) {
        let body = res.json();
        //console.log("commercial page : below is the extracted data ******* " + JSON.stringify(body));
        return body || {};
    }

    // below function can be used when deal with Observabled
    private handleErrorPromise(error:Response | any) {
        console.error(error.message || error);
        return Promise.reject(error.message || error);
    }

    private handleErrorObservable(error:Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    }


}