import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// import {ResetPasswordService} from './service/reset-password.service';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import {FileUploadModule} from "ng2-file-upload";
import { MyProductComponent } from './my-product.component';
import { EditProductComponent } from './edit-product.component';
// import { CarouselModule } from 'angular4-carousel';
import { DeleteProductComponent } from './delete-product.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { LoadingModule } from 'ngx-loading';
// import { CeiboShare } from '../promote-product/ng2-social-share';
import { SharedModule } from '../../shared/shared.module';
import {ShareButtonsModule} from 'ngx-sharebuttons';


@NgModule({
  declarations: [
    MyProductComponent,
    EditProductComponent,
    DeleteProductComponent
  //  CeiboShare
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    FormsModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    LoadingModule,
    FileUploadModule,
    // CarouselModule,
    SharedModule,
    ShareButtonsModule.forRoot(),
    RouterModule.forChild([
    {
      path: '',
      component: MyProductComponent
    },
    {
      path: 'edit-product/:id',
      component: EditProductComponent
    },
    {
      path: 'delete-product',
      component: DeleteProductComponent
    }
  ])
],
providers: [],
bootstrap: [MyProductComponent]

})
export class MyProductModule { }
