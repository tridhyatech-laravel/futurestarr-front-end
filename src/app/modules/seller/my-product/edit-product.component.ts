import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { FileUploader, FileLikeObject, FileItem, ParsedResponseHeaders } from 'ng2-file-upload';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { MyProductService } from './service/my-product.service';
import { Router, RouterLinkActive, Params, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
// import { ICarouselConfig, AnimationConfig, CarouselService } from 'angular4-carousel';

declare var $: any;

//const URL = 'http://18.217.7.143/futurestarr-backend/api/seller/image/edit';

@Component({
    selector: 'edit-product',
    templateUrl: './edit-product.component.html',
    styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
    public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));

    public categories: any;
    public product: any = {};
    public talent_id: any;
    public showProductInfo: any = {};
    public imageSources: string[];
    public loading: boolean = true;
    showPriceError: boolean = false;

    // image variables
    public allowedImageType = ['image/png', 'image/gif', 'image/jpeg', 'video/mp4'];
    public maxImageSize = 100 * 1024 * 1024; // 100 MB
    public imageUploader: FileUploader = new FileUploader({
        url: this.myProductService.serverIp + '/api/seller/image/edit',
        itemAlias: 'image',
        method: "POST",
        // allowedMimeType: this.allowedImageType,
        allowedFileType: ['video', 'image'],
        maxFileSize: this.maxImageSize,
        autoUpload: false,
        removeAfterUpload: false,
        queueLimit: 1
    });

    // video variables
    public allowedVideoType = ['video/mp4', 'audio/mp3'];
    public maxVideoSize = 100 * 1024 * 1024; // 100 MB
    public videoUploader: FileUploader = new FileUploader({
        url: this.myProductService.serverIp + '/api/seller/video/edit',
        itemAlias: 'video',
        method: "POST",
        removeAfterUpload: false,
        // allowedMimeType: this.allowedVideoType,
        allowedFileType: ['video', 'audio'],
        maxFileSize: this.maxVideoSize,
        autoUpload: false,
        queueLimit: 1
    });

    // pdf/doc variables
    public allowedDocType = ['application/pdf', 'application/docx', 'application/doc', 'video', 'audio/mp3', 'image/jpg', , 'image/jpeg'];
    public maxDocSize = 100 * 1024 * 1024; // 100 MB
    public pdfUploader: FileUploader = new FileUploader({
        url: this.myProductService.serverIp + '/api/seller/pdf/edit',
        removeAfterUpload: false,
        itemAlias: 'pdf',
        // allowedMimeType: this.allowedDocType,
        allowedFileType: ['video', 'pdf', 'doc', 'image', 'audio'],
        maxFileSize: this.maxDocSize,
        autoUpload: false,
        method: "POST",
        // queueLimit: 1
    });

    errorMessage: string;

    constructor(private _location: Location,
        private el: ElementRef,
        public myProductService: MyProductService,
        public router: Router,
        private activatedRoute: ActivatedRoute,
        public toastr: ToastrService) {

        if (this.currentUser && this.currentUser.role_id == "seller") {

        }
        else {
            this.router.navigate(['/']);
        }

        // image uploader functions
        this.imageUploader.onWhenAddingFileFailed = (item, filter, options) => this.onUploadImageFailed(item, filter, options);
        this.imageUploader.onAfterAddingFile = (fileItem) => {
            fileItem.withCredentials = false;
            //console.log("item value of image :: " + fileItem);
            ////console.log(fileItem.file.type);
        };

        this.imageUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            ////console.log("ImageUpload:uploaded:", item, status, response);
            //////console.log("test data ++++++++++" + item.formData[0].talent_id);
        };

        // video uploader functions
        this.videoUploader.onWhenAddingFileFailed = (item, filter, options) => this.onUploadVideoFailed(item, filter, options);
        this.videoUploader.onAfterAddingFile = (fileItem) => {
            fileItem.withCredentials = false;
            //console.log("item value of video :: " + fileItem);
            //console.log(fileItem.file.type);
            //console.log("data is added......");
            //this.uploadVideos();
        };
        this.videoUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            //console.log("Video:uploaded:", item, status, response);
        };

        // pdf/doc uploader functions
        this.pdfUploader.onWhenAddingFileFailed = (item, filter, options) => this.onUploadPdfFailed(item, filter, options);
        this.pdfUploader.onAfterAddingFile = (fileItem) => {
            fileItem.withCredentials = false;
            //console.log(this.pdfUploader.queue);
            //console.log(fileItem.file.name);
            // this.uploadDocs();
        };
        this.pdfUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            //console.log("pdfUploaded:uploaded:", item, status, response);
        };

        this.activatedRoute.params.subscribe((params: Params) => {
            this.talent_id = params['id'];
            //console.log(this.talent_id);
        });

        this.imageUploader.onErrorItem = (item, response, status, headers) => this.onImageErrorItem(item, response, status, headers);
        this.imageUploader.onSuccessItem = (item, response, status, headers) => this.onImageSuccessItem(item, response, status, headers);

        this.videoUploader.onErrorItem = (item, response, status, headers) => this.onVideoErrorItem(item, response, status, headers);
        this.videoUploader.onSuccessItem = (item, response, status, headers) => this.onVideoSuccessItem(item, response, status, headers);

        this.pdfUploader.onErrorItem = (item, response, status, headers) => this.onPdfErrorItem(item, response, status, headers);
        this.pdfUploader.onSuccessItem = (item, response, status, headers) => this.onPdfuccessItem(item, response, status, headers);

    }

    checkFn(event){
        // console.log(event);
    }

    onImageSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let data = JSON.parse(response); //success server response
        // this.imageUploader.queue = [];
        if (this.imageUploader.queue.length > 0 || this.videoUploader.queue.length > 0 ||
            this.pdfUploader.queue.length > 0) {
            this.loading = true;
        }
        else {
            this.loading = false;
            this.toastr.success("Product updated successfully");
            this.router.navigate(['/seller/my-product'])
        }
    }

    onImageErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let error = JSON.parse(response); //error server response
        if (this.imageUploader.queue.length > 0 || this.videoUploader.queue.length > 0 ||
            this.pdfUploader.queue.length) {
            this.loading = true;
        }
        else {
            this.loading = false;
            //console.log("loader from image");
            this.toastr.error("Product could not updated");
        }
    }

    onVideoSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let data = JSON.parse(response); //success server response
        // this.videoUploader.queue = [];
        if (this.imageUploader.queue.length > 0 || this.videoUploader.queue.length > 0 ||
            this.pdfUploader.queue.length > 0) {
            this.loading = true;
        }
        else {
            this.loading = false;
            this.router.navigate(['/seller/my-product'])
            //console.log("loader from video");
            this.toastr.success("Product updated successfully");
        }
    }

    onVideoErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let error = JSON.parse(response); //error server response
        if (this.imageUploader.queue.length > 0 || this.videoUploader.queue.length > 0 ||
            this.pdfUploader.queue.length) {
            this.loading = true;
        }
        else {
            this.loading = false;
            this.toastr.error("Product could not updated");
        }
    }

    onPdfuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let data = JSON.parse(response); //success server response
        // this.pdfUploader.queue = [];
        if (this.imageUploader.queue.length > 0 || this.videoUploader.queue.length > 0 ||
            this.pdfUploader.queue.length > 0) {
            this.loading = true;
        }
        else {
            this.loading = false;
            this.router.navigate(['/seller/my-product'])
            //console.log("loader from pdf");
            this.toastr.success("Product updated successfully");
        }
    }

    onPdfErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let error = JSON.parse(response); //error server response
        if (this.imageUploader.queue.length > 0 || this.videoUploader.queue.length > 0 ||
            this.pdfUploader.queue.length) {
            this.loading = true;
        }
        else {
            this.loading = false;
            this.toastr.error("Product could not updated");
        }
    }

    ngOnInit() {
        this.talentCategory();
        this.showProduct();
    }

    ngAfterContentInit() {

    }

    topFunction() {
        $('html, body').animate({ scrollTop: 0 }, 'fast');
    }

    goBack() {
        this._location.back();
    }

    talentCategory() {
        //console.log("list of categories :: ");
        this.myProductService.talentCategories().then(res => {
            this.categories = res;
            this.categories = this.categories.category;
        })
        .catch(error => {
            //console.log("error value: " + error);
        });
    }

    showProduct() {
        var arr = [];
        this.loading = true;

        this.myProductService.showProduct(this.talent_id).then(res => {
            this.showProductInfo = res;
            this.loading = false;
        })
        .catch(error => {
            //console.log("error value: " + error);
            this.loading = false;
        });
    }

    change($event) {
        $event.preventDefault();
        // //console.log("target value :: " + $event.target.value);

        this.showProductInfo.category = $event.target.value;
        ////console.log('this.selectedFacility: ' + this.showProductInfo.category);
    }

    editProduct(formData) {
        this.toastr.clear();
        this.loading = true;
        this.myProductService.updateProduct(formData, this.talent_id).then(res => {
            this.talentCategory();
            if (this.imageUploader.queue.length > 0 || this.videoUploader.queue.length > 0 ||
                this.pdfUploader.queue.length) {
                this.loading = true;
                this.uploadVideos(res);
                this.uploadImages(res);
                this.uploadDocs(res);
            }
            else {
                this.loading = false;
                // console.log("from edi product");
                this.toastr.success('Product updated successfully!', 'Success!');
                this.router.navigate(['/seller/my-product'])
            }
        })
        .catch(error => {
            //console.log(error._body);
            this.loading = false;
            this.toastr.error('Error..!! ', 'Oops!');
        });
    }

    onUploadImageFailed(item: FileLikeObject, filter: any, options: any) {
        //console.log("onUploadImagesFailed :::::::::" + item.type);

        switch (filter.name) {
            case 'fileSize':
                this.errorMessage = 'Maximum upload size 100 MB allowed';
                //console.log(this.errorMessage);
                this.toastr.error('Maximum upload size 100 MB allowed');
                break;
            case 'mimeType':
                const allowedTypes = this.allowedImageType.join();
                this.errorMessage = 'Type ' + item.type + ' is not allowed. Allowed types:' + allowedTypes;
                this.toastr.error('Type ' + item.type + ' is not allowed');
                //console.log(this.errorMessage);
                break;
            default:
                this.errorMessage = 'Image and video are allowed';
                //console.log(this.errorMessage);
                this.toastr.error('Image and video are allowed');
        }
    }

    onUploadVideoFailed(item: FileLikeObject, filter: any, options: any) {
        // console.log(filter);
        switch (filter.name) {
            case 'fileSize':
                this.errorMessage = 'Maximum upload size is 100 MB';
                //console.log(this.errorMessage);
                this.toastr.error('Maximum upload size is 100 MB');
                break;
            case 'mimeType':
                const allowedTypes = this.allowedVideoType.join();
                this.errorMessage = 'Type ' + item.type + ' is not allowed. Allowed types:' + allowedTypes;
                this.toastr.error('Type ' + item.type + ' is not allowed');
                //console.log(this.errorMessage);
                break;
            default:
                this.errorMessage = 'Only video and audio are allowed';
                //console.log(this.errorMessage);
                this.toastr.error('Only video and audio are allowed');
        }
    }

    onUploadPdfFailed(item: FileLikeObject, filter: any, options: any) {
        //console.log("onUploadPdfFailed :::::::::" + item.type);
        switch (filter.name) {
            case 'fileSize':
                this.errorMessage = 'Maximum upload size 100 MB allowed';
                //console.log(this.errorMessage);
                this.toastr.error('Maximum upload size 100 MB allowed');
                break;
            case 'mimeType':
                const allowedTypes = this.allowedDocType.join();
                this.errorMessage = 'Type ' + item.type + ' is not allowed. Allowed types:' + allowedTypes;
                this.toastr.error('Type ' + item.type + ' is not allowed');
                //console.log(this.errorMessage);
                break;
            default:
                this.errorMessage = 'Video, Audio, Image, Pdf and Doc files are allowed';
                //console.log(this.errorMessage);
                this.toastr.error('Video, Audio, Image, Pdf and Doc files are allowed');
        }
    }

    uploadImages(talent_id) {
        //console.log("upload image 0");
        this.imageUploader.onBuildItemForm = function (fileItem, form) {
            form.append('talent_id', talent_id);
            fileItem.remove();
        };
        this.imageUploader.uploadAll();
    }

    uploadVideos(talent_id) {
        //console.log("uploda video ");
        this.videoUploader.onBuildItemForm = function (fileItem, form) {
            form.append('talent_id', talent_id);
            fileItem.remove();
        };
        this.videoUploader.uploadAll();
    }

    uploadDocs(talent_id) {
        let self = this;
        let filenames = [];
        this.pdfUploader.queue.forEach(element => {
            filenames.push(element.file.name);
        })
        this.pdfUploader.onBuildItemForm = function (fileItem, form) {
            // //console.log(self.showProductInfo.pdf);
            form.append('talent_id', talent_id);
            form.append('upload_files', filenames.join("@@@"));
            fileItem.remove();
        };
        this.pdfUploader.uploadAll();
    }
}
