import { Component, OnInit ,ElementRef, Input} from '@angular/core';
import { FileUploader , FileLikeObject} from 'ng2-file-upload';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { MyProductService} from './service/my-product.service';
import { Router, RouterLinkActive } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AccountSettingService} from '../account-setting/service/account-setting.service';


declare var $:any;

@Component({
    selector: 'app-delete-product',
    templateUrl: './delete-product.component.html',
    styleUrls: ['./delete-product.component.css']
})
export class DeleteProductComponent implements OnInit {


    public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
    public errorMsg:string = '';
    public products:any = {};
    public errorMessage:string;
    public days = [30, 50, 80, 100];
    public selectedDay = 30;
    public numberOfDays:any = {};
    public social:any = {};
    public ProductId = {};
    public newObj:any = {item:''};
    //public checkedValue = [];
    public checkedValue = [];
    public loading: boolean = true;
    public p = 1;
    //public selected:string;
    //public selectedProducts:Product[];


    constructor(private el:ElementRef,
                public myProductService:MyProductService,
                public router:Router,
                public toaster:ToastrService,
                private _location:Location,
                public accountSettingService:AccountSettingService) {

        this.getProducts();

    }

    ngOnInit() {

        //console.log("check val :: " + this.checkedValue);
    }

    goBack() {
        this._location.back();
    }

            topFunction() {
     $('html, body').animate({ scrollTop: 0 }, 'fast');
 }


    onSelect(val) {
        //console.log("after filter ::::::::: " + JSON.stringify(this.numberOfDays));
        this.getProducts();
    }

    getProducts() {
        // //console.log("Products *************");
        let dayNum = this.selectedDay;
        this.loading = true;

        this.myProductService.getDeletedProducts(dayNum).then(res => {
            this.products = res;
            this.products = this.products.talents;
            this.numberOfDays = this.products;
            this.ProductId = this.products.id;

            this.loading = false;

            //console.log("seller plans fot buy a product^^^^^^^" + JSON.stringify(this.products));

        }).catch(error => {
            //console.log("error value: " + error);
            this.loading = false;


        });
    }

    isImage(new_product_img_path) {
        let images = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'GIF', 'gif'];
        if (new_product_img_path) {
            let fileExtension = new_product_img_path.replace(/^.*\./, '');
            if (images.includes(fileExtension)) {
                return true;
            }
        }
    }

    changeCheckbox(e, productData) {
        //console.log("product data :: " + productData);
        //console.log(">>>>> :: " + JSON.stringify(this.checkedValue));

        if (e.target.checked) {
            //console.log("find product id");
            //(this.checkedValue).push(productData);

                (this.checkedValue).push({id: productData});

        }

        else {
            //console.log("not found product id");
            let updateItem = this.checkedValue.find(this.findIndexToUpdate, productData);

            let index = this.checkedValue.indexOf(updateItem);

            this.checkedValue.splice(index, 1);
        }

    }

    findIndexToUpdate(productData) {
        return productData === this;
    }


    confirmDeleteProducts() {
        //console.log(this.checkedValue);
        // if(confirm("Are you sure to delete "+this.checkedValue)) {
        //  //console.log("Implement delete functionality here");
        //
        //}

          if (this.checkedValue.length === 0) {
            //console.log('no value is selected');
            this.toaster.error('No item is selected..!');
        } else {

              $('#confirmModal').modal({
                  backdrop: 'static'
              });

          }
    }


    deleteProducts() {
        this.loading = true;
        //console.log(this.checkedValue);
        let talentId = this.checkedValue;
          this.newObj.item = this.checkedValue;

         //console.log("this.newObj " + JSON.stringify(this.newObj));
         //console.log("this.newObj.item " + JSON.stringify(this.newObj.item));

            //console.log("value is selected >>>>  " + JSON.stringify(this.checkedValue));
           this.myProductService.removeProduct(this.newObj).then(res => {
                this.getProducts();
                this.toaster.success('Deleted successfully!');
                this.checkedValue =[];
                this.loading = false;

            }).catch(error => {
                //console.log("error value: " + error);
                this.loading = false;
                this.toaster.error('Got some error!');
            });
        }

    confirmUndeleteProducts() {
        //console.log(this.checkedValue);
        // if(confirm("Are you sure to delete "+this.checkedValue)) {
        //  //console.log("Implement delete functionality here");
        //
        //}

          if (this.checkedValue.length === 0) {
            //console.log('no value is selected');
            this.toaster.error('No item is selected..!');
        } else {

              $('#undeletedConfirmModal').modal({
                  backdrop: 'static'
              });

          }
    }


    undeleteProducts() {
       this.loading = true;
        //console.log(this.checkedValue);
        let talentId = this.checkedValue;
          this.newObj.item = this.checkedValue;

         //console.log("this.newObj " + JSON.stringify(this.newObj));
         //console.log("this.newObj.item " + JSON.stringify(this.newObj.item));

            //console.log("value is selected >>>>  " + JSON.stringify(this.checkedValue));
           this.myProductService.undeleteProduct(this.newObj).then(res => {
                this.getProducts();
                this.toaster.success('Restored successfully!');
                this.checkedValue =[];
                this.loading = false;

            }).catch(error => {
                //console.log("error value: " + error);
                this.loading = false;
                this.toaster.error('Get some error!');
            });
        }





}
