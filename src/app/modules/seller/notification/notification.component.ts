import { Component, OnInit ,ElementRef, Input, Injectable} from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {NotificationService} from './service/notification.service';

declare var $:any;

@Component({
    selector: 'notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.css']
})


export class NotificationComponent implements OnInit {

    public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
    public errorMsg:string = '';
    public message:any;

    constructor(private el:ElementRef,
                public notificationService:NotificationService,
                public router:Router,
                public toaster:ToastrService,
                private _location: Location) {


         this.showMessageCount();

    }

    ngOnInit() {
    }

    ngAfterContentInit() {
    }

     goBack(){
      this._location.back();
  }


    showMessageCount() {
        // console.log("seller *************");
        this.notificationService.getNotification().then(res => {
            this.message = res;
            // console.log("seller message ^^^^^^^" + JSON.stringify(this.message));
        }).catch(error => {
            // console.log("error value: " + error);


        });
    }


}
