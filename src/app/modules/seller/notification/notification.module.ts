import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NotificationComponent } from './notification.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// import {ResetPasswordService} from './service/reset-password.service';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    NotificationComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
    {
      path: '',
      component: NotificationComponent

    }
  ])
],
providers: [],
bootstrap: [NotificationComponent]

})
export class NotificationModule { }
