import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { SharedService } from '../../../../services/shared.service';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
// import { HttpHeaders } from '@angular/common/http';
declare var $: any;

@Injectable()
export class CommercialAdsService {

    headers: Headers;
    options: RequestOptions;
    public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
    public serverIp;
    constructor(public http: Http,
        public sharedService: SharedService) {
        this.serverIp = this.sharedService.apiUrl;
        // console.log('seller commercial ads plan service connected');

        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.currentUser.token
        });
        this.options = new RequestOptions({ headers: this.headers });
    }

    sellerPlans(user_id): Promise<Object> {
        // console.log("commercial ads plan service function called ******) " + this.currentUser);

        return this.http.get(this.sharedService.apiUrl + '/api/seller/plans/'+user_id, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    sellerPurchasedPlan(user_id): Promise<Object> {
        // console.log("commercial ads plan service function called ******) " + this.currentUser);

        return this.http.get(this.sharedService.apiUrl + '/api/seller/seller_purchased_plan/'+user_id, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    getUpgradePlans(plan_id): Promise<Object> {
        // console.log("commercial ads plan service function called ******) " + this.currentUser);

        return this.http.get(this.sharedService.apiUrl + '/api/seller/get_upgrade_plans/'+plan_id, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    getSellerProducts(user_id){
        return this.http.get(this.sharedService.apiUrl + '/api/seller/seller_products/'+user_id, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);
    }

    commercialPlans(body: Object): Promise<Object> {
        // console.log("commercial ads plan service function called ******) " + this.currentUser);

        return this.http.post(this.sharedService.apiUrl + '/api/seller/commercial-plan?user_id=' + this.currentUser.id, body, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    contactService(body: Object): Promise<Object> {
        return this.http.post(this.sharedService.apiUrl + '/api/seller/contact?user_id=' + this.currentUser.id, body, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    customPlan(body: Object): Promise<Object> {
        return this.http.post(this.sharedService.apiUrl + '/api/seller/custom-plan?user_id=' + this.currentUser.id, body, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    buyCommercialPlan(body: Object): Promise<Object> {
        return this.http.post(this.sharedService.apiUrl + '/api/seller/add-plan', body, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    postAdsVideo(formData): Promise<Object>{  
        return this.http.post(this.sharedService.apiUrl + '/api/seller/addCommercialBanner',formData ).toPromise()
          .then(this.extractData)
          .catch(this.handleErrorPromise);
      }

    processToCheckout(plan_id, user_id): Promise<Object> {
        let dataToSend = {
            plan_id : plan_id,
            user_id : user_id
        }
        let header = new Headers();
        header.set('Content-Type', "application/json");

        let options = new RequestOptions({
            headers:header,
            body: dataToSend
        });
        // console.log(options);   
        // let paypal_url = 'https://www.futurestarr.com/paypal/commercial_plan.php'
        let paypal_url = 'https://www.futurestarr.com:444/paypal/commercial_plan.php'
        // let paypal_url = 'http://localhost/paypal/commercial_plan.php'
        return this.http.post(paypal_url, dataToSend).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);
    }

    upgradePlanCheckout(data){
        
        // let paypal_url = 'https://www.futurestarr.com/paypal/upgrade_commercial_plan.php'
        let paypal_url = 'https://www.futurestarr.com:444/paypal/upgrade_commercial_plan.php'
        
        return this.http.post(paypal_url, data).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);
    }

    enableDisableAd(body: Object): Promise<Object> {
        return this.http.post(this.sharedService.apiUrl + '/api/seller/enable_disable_ad', body, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    getSellerAds(user_id){
        return this.http.get(this.sharedService.apiUrl + '/api/seller/seller_ads/'+user_id, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);
    }

    private extractData(res: Response) {
        let body = res.json();
        //console.log("commercial page : below is the extracted data ******* " + JSON.stringify(body));
        return body || {};
    }

    // below function can be used when deal with Observabled
    private handleErrorPromise(error: Response | any) {
        console.error(error.message || error);
        return Promise.reject(error.message || error);
    }

    private handleErrorObservable(error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    }


}
