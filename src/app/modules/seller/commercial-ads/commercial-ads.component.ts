import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FileUploader, FileLikeObject } from 'ng2-file-upload';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { CommercialAdsService } from './service/commercial-ads.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from '../../../services/shared.service';

declare var $: any;

@Component({
    selector: 'commercial-ads',
    templateUrl: './commercial-ads.component.html',
    styleUrls: ['./commercial-ads.component.css']
})


export class CommercialAdsComponent implements OnInit {

    public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
    public errorMsg: string = '';
    public plans: any = {};
    public plan: any = {};
    public showHide: boolean = false;
    public errorMessage: string;
    public ads: Object = { description: "" };
    public contact= {
        username:'',
        email:'',
        productInfo:''
    };
    public custom: any = {};
    public loading: boolean = false;

    public pay: any = {};
    public payLink: any = {};
    public editCart: any = {};
    public cart: any = {};
    public getPlanInfo: any = {};
    public sellerPurchasedPlan :any;


    // video variables
    public allowedImageType = ['image/png', 'image/gif', 'image/jpeg'];
    public maxImageSize = 50 * 1024 * 1024; // 8 MB
    public uploader: FileUploader = new FileUploader({
        url: this.commercialAdsService.serverIp + '/api/seller/plan',
        itemAlias: 'image',
        method: "POST",
        allowedMimeType: this.allowedImageType,
        maxFileSize: this.maxImageSize,
        autoUpload: false,
        removeAfterUpload: false,
        queueLimit: 1
    });


    constructor(private el: ElementRef,
        public commercialAdsService: CommercialAdsService,
        public router: Router,
        public toaster: ToastrService,
        private _location: Location,
        public sharedService: SharedService) {


        // video uploader functions
        this.uploader.onWhenAddingFileFailed = (item, filter, options) => this.onUploadImageFailed(item, filter, options);

        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            // console.log("ImageUpload:uploaded:", item, status, response);
            //console.log("test data ++++++++++" + item.formData[0].talent_id);

        };

        this.uploader.onAfterAddingFile = (fileItem) => {
            fileItem.withCredentials = false;
            // console.log("item value of image :: " + fileItem);
            // console.log(fileItem.file.type);
        };


    }

    ngOnInit() {

    }

    ngAfterContentInit() {
        this.getPlans();
    }

    goBack() {
        this._location.back();
    }

    topFunction() {
        $('html, body').animate({ scrollTop: 0 }, 'fast');
    }


    //showHideDiv() {
    //    this.showHide = !this.showHide;
    //
    //    console.log("show div :: " + this.showHide);
    //
    //}

    showContactServiceModal() {
        //  $('#contactServiceModal').modal('show');
        $('#contactServiceModal').modal({ backdrop: 'static' });
    }

    hideContactServiceModal() {
        $('#contactServiceModal').modal('hide');
    }


    getPlans() {
        // console.log("Plans *************");
        this.loading = true;
        this.commercialAdsService.sellerPlans(this.sharedService.currentUser.id).then(res => {
            this.plans = res;
            // console.log(this.plans)
            this.plans = this.plans.plans;
            this.sellerPurchasedPlan = res['sellerPlan'];
            // console.log(this.sellerPurchasedPlan);
            this.loading = false;
            //console.log("seller plans ^^^^^^^" + JSON.stringify(this.plans));
        }).catch(error => {
            this.loading = false;
            // console.log("error value: " + error);
        });
    }


    showPlanModal(id) {
        $('#commercialModal').modal({ backdrop: 'static' });
        //  console.log(this.plans);
        this.plans.forEach((plan: any) => {
            if (plan.id === id) {
                this.plan = plan;
            }
        });
    }

    hidePlanModal() {
        $('#commercialModal').modal('hide');
    }


    getPayPalPlan(id) {
        // console.log("hide model");
        this.showHide = !this.showHide;

        // console.log("show div :: " + this.showHide);
        this.hidePlanModal();
    }

    // addPlansInfo(formData) {
    //     console.log(formData.value);
    //     this.commercialAdsService.commercialPlans(formData.value).then(res=>{
    //         formData.reset();
    //         console.log("User info: " + JSON.stringify(res));
    //     }).catch(error => {
    //         console.log("error value: " + error);
    //         this.errorMsg = 'Data is not inserted';
    //     });
    // }

    addContact(formData) {
        this.loading = true;
        // console.log("addContact function is called ");
        // console.log("Add contact information..." + JSON.stringify(formData));
        this.commercialAdsService.contactService(formData).then(res => {
            // this.contact = res;
            this.hideContactServiceModal();
            this.loading = false;
            this.toaster.success('Added Contact Successfully!', 'SuccessS');
            // console.log("user contact::" + JSON.stringify(this.contact));
        }).catch(error => {
            this.loading = false;
            this.toaster.error('Error occured', 'Oops!');
        });
    }


    addCustomPlan(formData) {
        this.loading = true;
        // console.log(formData.value);
        this.commercialAdsService.customPlan(formData.value).then(res => {
            formData.reset();
            this.loading = false;
            this.toaster.success('Email has been send to Admin.', 'SuccessS');
            // console.log("user contact::" + JSON.stringify(this.contact));
        }).catch(error => {
            this.loading = false;
            this.toaster.error('Error occured', 'Oops!');
        });
    }


    onUploadImageFailed(item: FileLikeObject, filter: any, options: any) {
        // console.log("onUploadImagesFailed :::::::::" + item.type);

        switch (filter.name) {
            case 'fileSize':
                this.errorMessage = 'Maximum upload size 50 MB allowed';
                // console.log(this.errorMessage);
                break;
            case 'mimeType':
                const allowedTypes = this.allowedImageType.join();
                this.errorMessage = 'Type ' + item.type + ' is not allowed. Allowed types:' + allowedTypes;
                // console.log(this.errorMessage);
                break;
            default:
                this.errorMessage = 'Unknown error filter is ' + filter.name;
                // console.log(this.errorMessage);
        }
    }


    uploadFile(talent_id) {
        // console.log("image <<<<<<< :: " + JSON.stringify(talent_id));
        // console.log("image uploader 1 :::" + this.uploader);
        // console.log("image uploader 2:::" + this.uploader.queue);
        this.uploader.onBuildItemForm = function (fileItem, form) {
            form.append('talent_id', talent_id);
            //return {fileItem, form}
            fileItem.remove();
        };

        //this.uploader.queue[idx].upload();
        this.uploader.uploadAll();


    }

    checkedOut(price, name, plan_id) {
        // console.log('jhhlkh');
        this.pay.amount = price;
        this.pay.quantity = 1;
        this.pay.title = name;
        this.pay.user_id = this.currentUser.id;
        this.pay.plan_id = plan_id;
        // console.log(this.pay);
        this.getPlanByPayPal(this.pay);
        // this.commercialAdsService.buyCommercialPlan(this.pay).then(res => {
        //     console.log("payment linbk ???????" + res);
        // }).catch(error => {
        //   console.log("error value: " + error);
        // });
    }

    getPlanByPayPal(pay){
        this.loading = true;
        this.commercialAdsService.processToCheckout(this.pay.plan_id, this.sharedService.currentUser.id).then(data=>{
            this.loading = true;
            if(data['success']){
                document.location.href = data['redirect_url'];
            }
            else{
                this.toaster.warning(data['message']);
            }
        })
        .catch( error=>{
            this.loading = true;
            // console.log(error);
        })
    }

    // getPlanByPayPal(pay) {
    //     // console.log(pay);
    //     this.sharedService.payMoneyByPayPal(pay).then(res => {
    //         this.payLink = res;
    //         // console.log("payment linbk ???????" + this.payLink);
    //         window.open(this.payLink, "_blank");
    //         this.sharedService.announcecartItem();
    //         this.router.navigate(['/cart/thankyou']);
    //         // this.loading = false;
    //     }).catch(error => {
    //         // console.log("error value: " + error);
    //     });
    // }




}
