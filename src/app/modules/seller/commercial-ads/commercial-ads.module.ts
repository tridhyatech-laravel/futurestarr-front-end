import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CommercialAdsService} from './service/commercial-ads.service';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import {FileUploadModule} from "ng2-file-upload";
import { CommercialAdsComponent } from './commercial-ads.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { LoadingModule } from 'ngx-loading';


@NgModule({
  declarations: [
    CommercialAdsComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    LoadingModule,
    FileUploadModule,
    RouterModule.forChild([
    {
      path: '',
      component: CommercialAdsComponent

    }
  ])
],
providers: [],
bootstrap: [CommercialAdsComponent]

})
export class CommercialAdsModule { }
