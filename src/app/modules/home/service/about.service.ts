import { Injectable } from '@angular/core';
import { SharedService } from '../../../services/shared.service';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
declare var $: any;

@Injectable()
export class AboutService {

  headers: Headers;
  options: RequestOptions;
  public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
  public serverIp;
  constructor(public http: Http, public sharedService: SharedService) {
    this.serverIp = this.sharedService.apiUrl;
    this.headers = new Headers({ 'Content-Type': 'application/json' });
    this.options = new RequestOptions({ headers: this.headers });

  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  private extractData(res: Response) {
    let body = res.json();
    // console.log("below is the extracted data of categories ::" + JSON.stringify(body));
    // console.log(body);
    return body || {};
  }

  private handleErrorPromise(error: Response | any) {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
  }

  
}
