import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AboutService } from '../../service/about.service';
import { SharedService } from '../../../../services/shared.service';
import { LoadingModule } from 'ngx-loading';
import { Location } from '@angular/common';

declare var $: any;

@Component({
  selector: 'app-about-admin',
  templateUrl: './about-admin.component.html',
  styleUrls: ['./about-admin.component.css']
})
export class AboutAdminComponent implements OnInit {

  public loading = false;
  public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));

  constructor(private router: Router,
    public aboutService: AboutService,
    private route: ActivatedRoute,
    public sharedService: SharedService,
    private _location: Location) { }

  ngOnInit() {
    this.topFunction();
  }

  goBack() {
    this._location.back();
  }

  topFunction() {
    $('html, body').animate({ scrollTop: 0 }, 'fast');
  }

  sellTalent() {
    if (this.currentUser == null) {
      // console.log("not loggedin")
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();
    } else {
      if (this.currentUser.role_id == 'seller') {
        // console.log('seller');
        this.router.navigate(['/seller/dashboard']);
      } else {
        // console.log('not seller');
        $('#askToLogin').modal({ backdrop: 'static' });
        this.setTimeOut();
        // $('#askToJoinAsSeller').modal({ backdrop: 'static' });
      }
    }
  }

  buyTalent() {
    if (this.currentUser == null) {
      // console.log("not loggedin")
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();
    } else {
      if (this.currentUser.role_id == "buyer") {
        // console.log('buyer');
        this.router.navigate(['/buyer/dashboard']);
      } else {
        // console.log('not buyer');
        $('#askToLogin').modal({ backdrop: 'static' });
        this.setTimeOut();
        // $('#askToJoinAsBuyer').modal({ backdrop: 'static' });
      }
    }
  }

  goToRegister() {
    this.sharedService.announcelogOut();
  }

  setTimeOut() {
    var t = setTimeout(() => {
      $('#askToLogin').modal('toggle');
      this.sharedService.announceAskToLogIn(window.location.href);
    }, 500);
  }

}
