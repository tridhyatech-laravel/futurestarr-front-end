import { Component, OnInit, PLATFORM_ID, Inject } from '@angular/core';
import { Router, ActivatedRoute, ParamMap, RouterLinkActive } from '@angular/router';
import { SharedService } from '../../services/shared.service';
import { Location } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';
import { isPlatformBrowser } from '@angular/common';

declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent implements OnInit {

  latestBlog: any;

  public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));

  constructor(private router: Router,
    private route: ActivatedRoute,
    public sharedService: SharedService,
    @Inject(PLATFORM_ID) private platform: Object,
    private _location: Location, public title: Title, public meta: Meta) {
    // this.title.setTitle("1011 my home page");
    // this.meta.updateTag({ name: 'description', content: "1011 my home pagecontent" });
    
  }

  ngOnInit() {
    this.setMetaTags();
    if (isPlatformBrowser(this.platform)) {
      $(document).on("scroll", function () {
        if ($(document).scrollTop() > 10) {
          document.getElementById("myBtn").style.display = "block";
        } else {
          document.getElementById("myBtn").style.display = "none";
        }
      });
    }
    this.getLatestBlogs();
  }

  // Set meta tags
  setMetaTags() {
    this.title.setTitle("Future Starr | Talent Marketplace | Sell your Talent Online");
    this.meta.updateTag({ name: 'keywords', content: "advertise yourself, future rising stars, entertainment careers, online portfolios, online talent marketplace" });
    this.meta.updateTag({ name: 'description', content: 'Future Starr promotes promising talent from around the world. Our platform helps you in becoming a famous celebrity. Only requirement is; just upload your talent videos on Future Starr and showcase your talent to the world. Sell your Talent Free - Make Sales and Be your Own Boss!' });
    
  }

  topFunction() {
    if (isPlatformBrowser(this.platform)) {
      $('html, body').animate({ scrollTop: 0 }, 'fast');
    }
  }

  goBack() {
    this._location.back();
  }


  sellTalent() {
    if (isPlatformBrowser(this.platform)) {
      if (this.currentUser == null) {

        $('#askToLogin').modal({ backdrop: 'static' });
        this.setTimeOut();
      } else {
        if (this.currentUser.role_id == 'seller') {
          this.router.navigate(['/seller/dashboard']);
        } else {
          $('#askToJoinAsSeller').modal({ backdrop: 'static' });
        }
      }
    }
  }

  model_toggle() {
    if (isPlatformBrowser(this.platform)) {
      $('#register_my_model').modal('toggle');
    }
  }

  purchaseTalent() {
    // console.log('puchase function is called');
    // if (this.currentUser == null) {
    // console.log("not loggedin")
    if (isPlatformBrowser(this.platform)) {
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();
    }
    // } else {
    //   if (this.currentUser.role_id == 'buyer') {
    //     this.router.navigate(['/talent-mall']);
    //   } else {
    //     $('#askToJoinAsBuyer').modal({ backdrop: 'static' });
    //   }
    // }
  }

  getLatestBlogs() {
    this.sharedService.latestBlogs().then(data => {
      if (data['success']) {
        this.latestBlog = data['blogs']
      }
      else { }
    })
      .catch(error => {

      })
  }

  setTimeOut() {
    if (isPlatformBrowser(this.platform)) {
      var t = setTimeout(() => {
        $('#askToLogin').modal('toggle');
        this.sharedService.announceAskToLogIn(window.location.href);
      }, 1000);
    }
  }

  goToRegister() {
    this.sharedService.announcelogOut();
  }


}
