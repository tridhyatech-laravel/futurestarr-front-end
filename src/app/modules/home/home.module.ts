import { NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { AboutUsComponent } from './about/about-us/about-us.component';
import { AboutAdminComponent } from './about/about-admin/about-admin.component';
import { AboutService } from './service/about.service';
import { CommonModule } from '@angular/common';  

@NgModule({
  declarations: [
    HomeComponent,
    AboutUsComponent,
    AboutAdminComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomeComponent,
      },
      {
        path: 'about-us',
        component: AboutUsComponent
      },
      {
        path: 'about-admin',
        component: AboutAdminComponent
      }
    ])
  ],
  providers: [AboutService],
  bootstrap: [HomeComponent]
})
export class HomeModule { }
