import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { StarSearchComponent } from './star-search.component';
import { LoadingModule } from 'ngx-loading';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule } from '@angular/forms';
import { AboutCategoryComponent } from './about-category/about-category.component';
import {ShareButtonsModule} from 'ngx-sharebuttons';

@NgModule({
  imports: [
    CommonModule,
    LoadingModule,
    NgxPaginationModule,
    FormsModule,
    ShareButtonsModule.forRoot(),
    RouterModule.forChild([
      {
        path: '',
        component: StarSearchComponent
      },
      {
        path: 'about/:category_id',
        component: AboutCategoryComponent
      }
    ]),
  ],
  declarations: [StarSearchComponent, AboutCategoryComponent],
})
export class StarSearchModule { }
