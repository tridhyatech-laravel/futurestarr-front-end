import { Component, OnInit } from '@angular/core';
import { StarSearchService } from './service/star-search.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { SharedService } from '../../services/shared.service';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';
// import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';
// import { MetaService } from '@ngx-meta/core';



declare var $: any;

@Component({
  selector: 'app-star-search',
  templateUrl: './star-search.component.html',
  styleUrls: ['./star-search.component.css']
})


export class StarSearchComponent implements OnInit {

  public categoryResponse: any = {};
  public p = 1;
  public loading = true;
  public categoriesCount: number = 0;
  public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
  metaData: any;

  constructor(public starSearchService: StarSearchService,
    private router: Router,
    private route: ActivatedRoute,
    public sharedService: SharedService,
    public toastr: ToastrService,
    // public metaService:MetaService,
    private _location: Location, public title: Title, public meta: Meta) {
    this.metaData = this.route.snapshot.data;
    // console.log(this.metaData);
      this.setMetaTags();
    // this.title.setTitle("22011 star search page");
    // // this.meta.removeTag("description");
    // this.meta.updateTag({ name: 'description', content: "22011 star searchpagecontent" });
    // setTimeout(() => {
    //   this.meta.addTag({ name: 'description', content: "22011 star searchpagecontent" }, true);
    // }, 100);
  }

  ngOnInit() {
    this.getTalentCategories();
  }


  // Set meta tags
  setMetaTags() {
    this.title.setTitle("Future Star Search | Premier Talent Marketing Platform");
    this.meta.updateTag({ name: 'keywords', content: 'star search, Online career fairs, talent marketing platform, build successful career, advertise online' });
    this.meta.updateTag({ name: 'description', content: "Future Starr, an Online Talent Distribution Platform where you can promote and turn your talent into a full-time career. Whether you are fitness trainer, an artist, author, entertainer, photographer or a comedian our Starr Search feature will help you to promote and market your talent. Sign up Today with Future Starr." });
  }


  topFunction() {
    $('html, body').animate({ scrollTop: 0 }, 'slow');
  }

  // top_to_scroll(){
  //   alert();
  // }

  goBack() {
    this._location.back();
  }

  getTalentCategories() {
    this.starSearchService.getCategories().then(res => {
      this.loading = true;
      this.categoryResponse = res;
      this.categoriesCount = res.category.length;
      this.loading = false;
    }).catch(error => {
      this.loading = false;
      //  console.log("error value: " + error);
    });
  }

  purchaseTalent() {
    //  console.log('puchase function is called');
    if (this.currentUser == null) {
      //  console.log("not loggedin")
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();
    } else {
      if (this.currentUser.role_id) {
        this.toastr.success('You have already logged in', null);
      } else {
        $('#askToJoinAsBuyer').modal({ backdrop: 'static' });
      }
    }
  }

  setTimeOut() {
    var t = setTimeout(() => {
      $('#askToLogin').modal('toggle');
      this.sharedService.announceAskToLogIn(window.location.href);
    }, 1000);
  }

  goToRegister() {
    this.sharedService.announcelogOut();
  }

}
