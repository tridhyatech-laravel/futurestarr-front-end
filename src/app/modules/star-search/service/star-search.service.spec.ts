import { TestBed, inject } from '@angular/core/testing';

import { StarSearchService } from './star-search.service';

describe('StarSearchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StarSearchService]
    });
  });

  it('should be created', inject([StarSearchService], (service: StarSearchService) => {
    expect(service).toBeTruthy();
  }));
});
