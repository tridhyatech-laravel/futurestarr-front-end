import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { StarSearchService } from '../service/star-search.service';
import { SharedService } from '../../../services/shared.service';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';
import { Meta, Title } from '@angular/platform-browser';

declare var $: any;

@Component({
  selector: 'app-about-category',
  templateUrl: './about-category.component.html',
  styleUrls: ['./about-category.component.css']
})
export class AboutCategoryComponent implements OnInit {

  public sub;
  public categoryId;
  public categoryInfo: any = {};
  public loading = false;
  public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));

  public repoUrl;
  public imageUrl;

  //public repoUrl = 'https://github.com/Epotignano/ng2-social-share';
  //public imageUrl = 'https://avatars2.githubusercontent.com/u/10674541?v=3&s=200';

  constructor(private router: Router,
    public starSearchService: StarSearchService,
    private route: ActivatedRoute,
    public sharedService: SharedService,
    public toastr: ToastrService,
    private meta: Meta, private title: Title,
    private _location: Location) {
    this.sub = this.route.params.subscribe(params => {
      this.categoryId = params['category_id'];
      // console.log(this.categoryId);
      this.storeMetaContents(this.categoryId);
      this.getCategoryInfo(this.categoryId);
    });
  }

  ngOnInit() {

    this.repoUrl = window.location.href;

  }

  topFunction() {
    $('html, body').animate({ scrollTop: 0 }, 'fast');
  }

  goBack() {
    this._location.back();
  }

  getCategoryInfo(id) {
    this.loading = true;
    this.starSearchService.getCategoryInfo(id).then(res => {
      // console.log(res)
      if (res && res['category_info'].length > 0) {
        // console.log(res);

        this.categoryInfo = res;
        this.loading = false;
      }
      else {
        this.toastr.error('Category not found');
        setTimeout(() => {
          this.router.navigate(['/star-search']);
        }, 300);
      }
    }).catch(error => {
      this.loading = false;
      // console.log("error value: " + error);
    });
  }

  sellTalent() {
    if (this.currentUser == null) {
      // console.log("not loggedin")
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();
    } else {
      if (this.currentUser.role_id == 'seller') {
        this.router.navigate(['/seller/dashboard']);
      } else {
        // $('#askToJoinAsSeller').modal({ backdrop: 'static' });
        $('#askToLogin').modal({ backdrop: 'static' });
        this.setTimeOut();
      }
    }
  }

  purchaseTalent() {
    // console.log('puchase function is called');
    if (this.currentUser == null) {
      // console.log("not loggedin")
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();
    } else {
      if (this.currentUser.role_id == 'buyer') {
        this.router.navigate(['/talent-mall']);
      } else {
        // $('#askToJoinAsBuyer').modal({ backdrop: 'static' });
        $('#askToLogin').modal({ backdrop: 'static' });
        this.setTimeOut();
      }
    }
  }

  goToRegister() {
    this.sharedService.announcelogOut();
  }

  setTimeOut() {
    var t = setTimeout(() => {
      $('#askToLogin').modal('toggle');
      this.sharedService.announceAskToLogIn(window.location.href);
    }, 1000);
  }

  // SEO Meta contents
  storeMetaContents(category_id) {
    switch (category_id) {
      case '1':
        var title = 'Promote & Advertise yourself Online as an Author | Future Starr';
        var description = 'Sign Up with Future Starr and become a successful & well known Author. Future Starr is a platform where you can share your vision as well as your quality work to the audience around the globe. So Sign up with Future Starr, upload your work, make a great impact and target and reach millions of readers/audience worldwide.';
        var keywords = 'Book marketing, self promotions, promote your book online, promote yourself as an author, promote yourself as an writer, promote yourself as an publisher';
        this.setMetaContents(title, description, keywords);
        break;
      case '2':
        var title = 'Brand and Promote Yourself as an Entertainer | Future Starr';
        var description = 'Entertainer! Sign Up Today with Future Starr and promote yourself as a Celebrity and also boost up your reputation among your followers. With Future Starr you can have access to top vendors and get connected with your valuable audiences all over the globe. Hurry up, Sign Up Today!';
        var keywords = 'branding for entertainers, personal branding, personal branding website, personal branding online, individual branding';
        this.setMetaContents(title, description, keywords);
        break;
      case '4':
        var title = 'Sell Promote and Earn more with your Music | Future Starr';
        var description = 'Join Future Starr community and let your music speak for you. Whatever your genre may be; Punk, Rap, Hip Hop, EDM, Pop, Metal, Rock; just upload your music and promote yourself. Future Starr will help you get connected to various labels, brands, publishers and to a wider fan community. Sign Up Today!';
        var keywords = 'sell your music online, promote your music, sell music online, Digital Music Distribution, upload my song online free';
        this.setMetaContents(title, description, keywords);
        break;
      case '5':
        var title = 'Share, Display and Sell your Photos Online | Future Starr';
        var description = 'Future Starr is a global marketplace for the photographers and artists to display, share and sell their images. Whatever your niche may be including travel, fashion, cityscapes landmarks, weddings, portraitures, wildlife or more; you just need to upload your passionate projects and turn it into hard cash.Sign Up Today!';
        var keywords = 'fine art photography, sell your photographs online, professional photographer, sell images online';
        this.setMetaContents(title, description, keywords);
        break;
      case '6':
        var title = 'Standup Comedy Online Platform | Future Starr';
        var description = 'Sign Up with Future Starr and get connected to the global audiences. Upload your Best Comic works, events, video clips from your T.V. shows, podcasts and expand your fan community way beyond and be a global star. Hurry Up! Promote your comedy event and get the word out to the right people and make yourself successful.';
        var keywords = 'art of comedy, comedian actors portfolio, comedy club';
        this.setMetaContents(title, description, keywords);
        break;
      case '7':
        var title = 'Online Model Portfolio | Modelling Career | Future Starr';
        var description = 'Sign Up Today and outshine in the Modeling Industry. Upload your various projects and photoshoots, showcasing your lovely features and artistic vision and make your presence impressive. To create and promote a high-quality online modeling portfolio join Future Starr team today.';
        var keywords = 'online model portfolio, online modeling agency, become a model, best model portfolio websites, Create an Online Portfolio';
        this.setMetaContents(title, description, keywords);
        break;
      case '8':
        var title = 'Marketplace for Online Personal Fitness Trainer | Future Starr';
        var description = 'Sell Your Talent with Future Starr Today! Either you are a personal trainer or fitness guru you need a platform that can get you connected with the global audience and help you attract clients worldwide. Future Starr will help you promote and sell your services of fitness products instantly. Read more.';
        var keywords = 'online personal trainer, online gym trainer, online exercise trainer, fitness training career, ';
        this.setMetaContents(title, description, keywords);
        break;
      case '9':
        var title = 'National Geographic Careers & Talent Hunt | Future Starr';
        var description = 'Future Starr enables you to get connected with the global audience. Upload videos of your training sessions, meetings, rallies or marches and engage the global audience to create awareness about various environmental issues and attract non-profit organizations, or other activists who can support you.';
        var keywords = 'natgeo, wildlife photographer, national geographic photos, national geographic careers';
        this.setMetaContents(title, description, keywords);
        break;
      case '10':
        var title = 'Science Employment Opportunities | Future Starr';
        var description = 'Upload your profile and be accessible and searchable with Future Starr. Upload your actual classroom teaching visual clips or experiments in biology, chemistry, engineering or you can also upload charts graphs, or tables to provide detailed knowledge about various scientific concepts. Upload Your Classroom Teaching Today.';
        var keywords = 'science talent search, science careers, science careers jobs, science careers options, science employment';
        this.setMetaContents(title, description, keywords);
        break;
      case '11':
        var title = 'Advertise Your Cooking Talent & Skills | Future Starr';
        var description = 'Advertise your Food Making Skills with Future Starr Today! Promote and share your culinary expertise with a like-minded community of cooks, culinary fans, and food lovers. Create galleries through pictures of your recipes video tutorials and reach worldwide food enthusiasts.';
        var keywords = 'cooking talent, advertise cooking skills, market and brand cooking talent, professional chefs, advertising website';
        this.setMetaContents(title, description, keywords);
        break;
      case '12':
        var title = 'Nutritionists & Dietitians Talent | Future Starr';
        var description = 'Whether you are a registered dietitian or nutritionist, With Future Starr, you can target your specialized audience by uploading your blogs, or videos to share your healthy cooking recipes or your opinions about some important nutrition topics. Sign Up Today!';
        var keywords = 'Nutrition Professionals, nutritionists and dietitians talent, dietitian skills, nutritionist careers';
        this.setMetaContents(title, description, keywords);
        break;
      case '13':
        var title = 'Careers Opportunities for Maths Tutors Online | Future Starr';
        var description = 'Get rid of costly Advertisements! If you are a calculus or trigonometry expert in writing tricky math problems and want to be more accessible to your students, get connected with Future Starr. Join future starr today and express your math knowledge and skills. Just upload your work and help the students.';
        var keywords = 'Career Opportunities for Mathematics, best online tutoring sites, maths teacher jobs in usa, Math Tutors and Teachers Online';
        this.setMetaContents(title, description, keywords);
        break;
      case '14':
        var title = 'Career Opportunities in Beauty & Cosmetic Industry | Future Starr';
        var description = 'Sign Up with Future Starr Today. Whether you deal in Cosmetics, Fragrance, Skin Care Products, Hair Care Products or Salon Services, linking up with Future Starr will earn your ultimate loyal fanbase and clientele. Join us Today ! Future Starr fulfills your dreams and also make the dreams of your clients come true.';
        var keywords = 'cosmetic industry, beauty job opportunities,beauty business, online cosmetic industry, Industrial Models';
        this.setMetaContents(title, description, keywords);
        break;
      case '15':
        var title = 'Fashion Designer Careers, Talent & Opportunities | Future Starr';
        var description = 'Join hands with Future Starr, a dedicated Fashion Design Platform for Freelance Fashion Designers. Upload your clothing or your catalogs, displaying size and colors available and offer bespoke service to your customers. Share the competitions you organized or celebrations you made and make your customers your partners.';
        var keywords = 'fashion designing, fashion industry, fashion careers, fashion career opportunity';
        this.setMetaContents(title, description, keywords);
        break;
      case '16':
        var title = 'Professional Tattooists | Tattoo Artist Careers | Future Starr';
        var description = 'Sign Up with Future Starr, and get connected with reputable tattoo studios, can book your consultations easily and that too around the world. Upload your curated art galleries, articles, blogs, client’s reviews, artists’ recommendations, tattoo making videos. ';
        var keywords = 'Professional Tattooist, Professional Tattooing, Tattoo artist, Custom Tattoo Designs, professional and experienced tattoo artist ';
        this.setMetaContents(title, description, keywords);
        break;
      default:
        var title = 'Future Star Search | Premier Talent Marketing Platform ';
        var description = 'Future Starr, an Online Talent Distribution Platform where you can promote and turn your talent into a full-time career. Whether you are fitness trainer, an artist, author, entertainer, photographer or a comedian our Starr Search feature will help you to promote and market your talent. Sign up Today with Future Starr.';
        var keywords = 'star search, Online career fairs, talent marketing platform, build successful career, advertise online';
        this.setMetaContents(title, description, keywords);
    }

  }

  // Set meta information
  setMetaContents(title, description, keywords) {
    this.title.setTitle(title);
    this.meta.updateTag({ name: 'keywords', content: keywords });
    this.meta.updateTag({ name: 'description', content: description });
  }


}
