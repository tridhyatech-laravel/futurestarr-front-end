import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { SharedService } from '../../../../services/shared.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
declare var $:any;

@Injectable()
export class ChangePasswordService {

    headers:Headers;
    options:RequestOptions;
    public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
    public serverIp;

    constructor(public http:Http,
                public sharedService:SharedService) {
        this.serverIp = this.sharedService.apiUrl;
        // console.log('seller message service connected');

        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.currentUser.token
        });
        this.options = new RequestOptions({headers: this.headers});
    }

    resetPassword(body:Object):Promise<Object> {
        // console.log("message service function called ******) " + this.currentUser);

        return this.http.put(this.sharedService.apiUrl + '/api/reset-password?user_id=' + this.currentUser.id, body, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    getUserPassword(body){
    //   console.log("body" + body);
      return this.http.post(this.sharedService.apiUrl + '/api/buyer/password?user_id='+this.currentUser.id, body, this.options).toPromise()
          .then(this.extractData)
          .catch(this.handleErrorPromise);
    }

    private extractData(res:Response) {
        let body = res.json();
        // console.log("reset password page ******* " + JSON.stringify(body));

        return body || {};
    }

    // below function can be used when deal with Observabled
    private handleErrorPromise(error:Response | any) {
        // console.error(error.message || error);
        return Promise.reject(error.message || error);
    }

    private handleErrorObservable(error:Response | any) {
        // console.error(error.message || error);
        return Observable.throw(error.message || error);
    }


}
