import { Component, OnInit ,ElementRef, Input, Injectable} from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ChangePasswordService } from './service/change-password.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

declare var $:any;

@Component({
    selector: 'change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.css']
})


export class ChangePasswordBuyerComponent implements OnInit {

    public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));
    public errorMsg:string = '';
    public user:any = {};
    public errMsg:string = '';
    public matchPwd= {'password':''};
    public currPass;
    public showMsg= false;
   //  public validateEqual: string;
    //public pass = {};

    constructor(private el:ElementRef,
                public changePasswordService:ChangePasswordService,
                public router:Router,
                public toaster:ToastrService,
                private _location: Location) {


    }

    ngOnInit() {
    //this.getSellerMessage();

    }

    topFunction() {
      $('html, body').animate({ scrollTop: 0 }, 'fast');
  }


    ngAfterContentInit() {
        //this.getSellerMessage();
    }

     goBack(){
      this._location.back();
  }


    //
    //Validate(newValue) {
    //  this.pass = newValue;
    //    console.log("pass value validation : " + newValue);
    //  //this.Platform.ready().then(() => {
    //  //   this.rootRef.child("users").child(this.UserID).child('range').set(this.range)
    //  //})
    //}

    onSelect(val) {
    //   console.log("calledeklllll");
      //console.log(val);
      var obj = {password: ''};
          obj.password = val;
          this.matchPwd = obj;
        this.changePasswordService.getUserPassword(this.matchPwd).then(res =>
        {
          this.currPass = res;
      }).catch(error => {
        //   console.log("error value: " + error);
      });

    }


    changePassword(formData) {
        // console.log("seller *************" + JSON.stringify(formData.pass));
       // this.user.pass = formData.pass;
       // this.user.newpassword = formData.newpassword;
       // this.user.confirmpassword = formData.confirmpassword;
          this.changePasswordService.resetPassword(this.user).then(res => {
            //this.user = res;
            // console.log("seller message ^^^^^^^" + JSON.stringify(res));
              // formData.reset();
               this.toaster.success('Your password has been updated successfully!', 'Success');
        }).catch(error => {
            // console.log("error value: " + error);
          this.toaster.error(error._body + '!', 'Oops Error!');
        });


  }


}
