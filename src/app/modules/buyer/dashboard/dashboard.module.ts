import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { FormsModule} from '@angular/forms';
import { BuyerAccountSettingService } from '../buyer-profile/service/buyer-account-setting.service';
import { LoadingModule } from 'ngx-loading';
import { GetProductsService } from './../../talent-mall/service/get-products.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../../shared/shared.module';
import {ShareButtonsModule} from 'ngx-sharebuttons';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    LoadingModule,
    NgxPaginationModule,
    SharedModule,
    ShareButtonsModule.forRoot(),
    RouterModule.forChild([
      {
        path: '',
        component: DashboardComponent
      }
    ])
  ],
  declarations: [DashboardComponent],
  providers: [ BuyerAccountSettingService,GetProductsService ]
})
export class DashboardModule { }
