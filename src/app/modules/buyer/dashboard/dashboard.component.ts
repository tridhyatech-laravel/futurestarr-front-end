import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BuyerAccountSettingService } from '../buyer-profile/service/buyer-account-setting.service';
import { GetProductsService } from './../../talent-mall/service/get-products.service';
import { SharedService } from '../../../services/shared.service';
import { LoadingModule } from 'ngx-loading';

declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public ratingsObj: any = {};
  public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
  public talent;
  public buyers: any = {};
  public buyerId;
  public loading = false;
  public blogCount: number = 0;
  public buyerContact: any = {};
  public buyerContactId;
  public buyerRatingId;
  public p: number = 1;
  //public repoUrl = 'https://github.com/Epotignano/ng2-social-share';
  //public imageUrl = 'https://avatars2.githubusercontent.com/u/10674541?v=3&s=200';
  public socialName;
  public promoteProducts: any = {};
  public socialId;
  public repoUrl;
  public recever_ids_for_send_msg: any;

  twitterData: any;

  constructor(private router: Router,
    private _location: Location,
    private activedRoute: ActivatedRoute,
    public buyerAccountSettingService: BuyerAccountSettingService,
    public toaster: ToastrService,
    public getProductsService: GetProductsService,
    public sharedService: SharedService
  ) {
    this.getBuyerProducts();
  }

  ngOnInit() {
  }

  topFunction() {
    $('html, body').animate({ scrollTop: 0 }, 'fast');
  }


  goBack() {
    this._location.back();
  }


  showSendMessageModal(talent_id, seller_ids) {
    $('#sendMessage').modal({ backdrop: 'static' });
    this.buyerContactId = talent_id;
    this.recever_ids_for_send_msg = seller_ids;
  }
  showRatingTalentModal(talent_id) {
    $('#ratingTalent').modal({ backdrop: 'static' });
    this.buyerRatingId = talent_id;
  }
  socialMediaShareModal(talent_id, social) {
    this.repoUrl = null;
    social.reset();
    // this.twitterData.url = '';
    // console.log(social);
    $('#socialMediaShare').modal({ backdrop: 'static' });
    this.socialId = talent_id;
    if (talent_id) {
      // console.log(this.twitterData.url);
      this.repoUrl = this.sharedService.web_url + 'talent-mall/product-info/' + talent_id;
      this.twitterData = { url: this.repoUrl, text: 'Checkout this awesome talent in Futurestarr', hashtags: 'Futurestarr, talent' };
      // console.log(this.twitterData.url);
    }


  }
  confirmDeleteModal(buyer_id) {
    $('#confirmDelete').modal({ backdrop: 'static' });
    // console.log('delete modal is called >> ' + buyer_id);
    this.buyerId = buyer_id;
  }

  deleteBuyerProducts() {
    this.loading = true;
    // console.log('delete function is called.. ' + this.buyerId);
    this.buyerAccountSettingService.deleteBuyerProduct(this.buyerId).then(res => {
      $('#confirmDelete').modal('hide');
      this.loading = false;
      // this.getBuyerProducts();
      this.toaster.success('Item deleted' + '!', 'Success');
      if(this.buyers){
        this.buyers.forEach((element, index) => {
          if(element.buyer_product_id == this.buyerId){
            this.buyers.splice(index, 1)
          }
        });
      }
    }).catch(error => {
      this.loading = false;
      //  console.log("error value: " + error);
      this.toaster.error('Item not deleted' + '!', 'Error');
    });
  }

  isImage(new_product_img_path) {
    let images = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'GIF', 'gif'];
    if (new_product_img_path) {
      let fileExtension = new_product_img_path.replace(/^.*\./, '');
      if (images.includes(fileExtension)) {
        return true;
      }
    }
  }

  getBuyerProducts() {
    // console.log('seller id is.');
    this.buyerAccountSettingService.getBuyerProducts(this.currentUser.id).then(res => {
      this.buyers = res.buyer;

      // console.log(this.buyers);

      // console.log("chetan choha nceytn choiuhan chyen cjhcjnjk oshdfj uhdfjk hougd fjsdjgf ");
      this.blogCount = this.buyers.length;

    }).catch(error => {
      // console.log("error value: " + error);
    });
  }

  addBuyerContact(body) {
    //  console.log(this.loading+body);    

    //  console.log('contact function is called.. ' + this.buyerContactId);
    //  console.log(body);

    this.loading = true;
    this.buyerContact.title = body.title;
    this.buyerContact.message = body.message;
    this.buyerContact.user_id = this.currentUser.id;
    this.buyerContact.talent_id = this.buyerContactId;

    let frmData = new FormData();
    frmData.set("currentUser", JSON.parse(localStorage.getItem('currentUser')).id);
    frmData.set("received_by", this.recever_ids_for_send_msg);
    frmData.set("message", this.buyerContact.message);

    this.sharedService.store_message(frmData);

    //  this.getProductsService.store_message_from_buyer_dashboard(this.recever_ids_for_send_msg,this.buyerContact.message);
    this.getProductsService.addBuyerContactMessage(this.buyerContact).then(res => {
      //  console.log('message added in table');
      $('#sendMessage').modal('hide');
      this.toaster.success('Message has been added', 'Success!');
      body.reset();
      this.loading = false;
    }).catch(error => {
      //  console.log("error value: " + error);
      this.toaster.error('Something wrong please try again', 'Error Occurred!');
      this.loading = false;
    });
  }

  giveRating(ratings) {
    this.loading = true;
    this.ratingsObj.rating = ratings.value.rating;
    this.ratingsObj.comment = ratings.value.comment;
    this.ratingsObj.user_id = this.currentUser.id
    this.ratingsObj.talent_id = this.buyerRatingId;
    //  console.log(this.ratingsObj);
    this.buyerAccountSettingService.giveRatings(this.ratingsObj).then(res => {
      $('#ratingTalent').modal('hide');
      this.toaster.success('Award has been added', 'Success!');
      ratings.reset();
      this.getBuyerProducts();
      this.loading = false;
    }).catch(error => {
      // console.log("Award is not added" , 'Error!');
      this.loading = false;
    });
  }

  getSocialShareMediaName(name) {
    this.socialName = name;
  }

  shareOnSocialMedia(formData) {
    let id = this.socialId;
    this.loading = true;
    this.promoteProducts = formData.value;
    this.promoteProducts.social_name = this.socialName;
    this.buyerAccountSettingService.addPromoteProduct(id, this.promoteProducts).then(res => {
      $('#socialMediaShare').modal('hide');
      this.loading = false;
      this.toaster.success('Added successfully!');
    }).catch(error => {
      this.toaster.error('Get error!');
      this.loading = false;
    });
  }

  download_contain_of_seller(talent_id) {
    this.buyerAccountSettingService.download_contain(talent_id).then(res => {
      let download_url: any;
      download_url = JSON.parse(JSON.stringify(res));
      let visit_link = download_url.download_url;
      document.location.href = visit_link;
    }).catch(error => {
      this.toaster.error('error');
      this.loading = false;
    });
  }


}
