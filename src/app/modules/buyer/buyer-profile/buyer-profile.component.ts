import { Component, OnInit } from '@angular/core';
import { BuyerAccountSettingService } from './service/buyer-account-setting.service';
import { Location } from '@angular/common';
import { FileUploader, FileLikeObject, FileItem, ParsedResponseHeaders } from 'ng2-file-upload';
import { SharedService } from '../../../services/shared.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingModule } from 'ngx-loading';

declare var $: any;

@Component({
    selector: 'app-buyer-profile',
    templateUrl: './buyer-profile.component.html',
    styleUrls: ['./buyer-profile.component.css']
})

export class BuyerProfileComponent implements OnInit {
    public loading: boolean = false;
    public buyer: any = {};
    useremail: any;
    public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
    public errorMessage: string;
    public profileImage: string;
    public allowedImageType = ['image/png', 'image/gif', 'image/jpeg', 'image/jpg'];
    public maxImageSize = 8 * 1024 * 1024; // 8 MB
    public imageUploader: FileUploader = new FileUploader({
        url: this.buyerAccountSettingService.serverIp + '/api/buyer/account/image',
        itemAlias: 'profile_pic',
        method: 'POST',
        allowedMimeType: this.allowedImageType,
        maxFileSize: this.maxImageSize,
        autoUpload: false,
        removeAfterUpload: false,
        queueLimit: 1
    });

    constructor(public buyerAccountSettingService: BuyerAccountSettingService,
        public _location: Location,
        public router: Router,
        public toastr: ToastrService,
        public sharedService: SharedService
    ) {

        // image uploader functions
        this.imageUploader.onWhenAddingFileFailed = (item, filter, options) => this.onUploadImageFailed(item, filter, options);
        this.imageUploader.onAfterAddingFile = (fileItem) => {
            fileItem.withCredentials = false;
            // console.log(fileItem.file.type);
            //this.uploadImages();
        };

        this.imageUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            //this.setLatestImage(response);
            this.setLatestImage();
            //console.log(JSON.stringify(response));
        };
        this.loading = false;
        
        

    }

    ngOnInit() {
        if(!this.currentUser.email)
        {
            this.toastr.error("We are unable to get the Email from social platform. Please complete your registration by entering the information including email ID ");
        }
        this.getCurrentUser();

        this.imageUploader.onErrorItem = (item, response, status, headers) => this.onErrorItem(item, response, status, headers);
        this.imageUploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);
    }

    onSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let data = JSON.parse(response); //success server response
        // console.log("data");
        // console.log(data);
        if (data.profile_pic) {
            let userData = JSON.parse(localStorage.getItem("currentUser"));
            userData.profile_pic = data.profile_pic;

            localStorage.setItem("currentUser", JSON.stringify(userData));
        }
        this.loading = false;
        window.location.reload();
    }

    onErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
        let error = JSON.parse(response); //error server response
        // console.log("error");
        // console.log(error);
        this.toastr.success(error.message);
    }

    topFunction() {
        $('html, body').animate({ scrollTop: 0 }, 'fast');
    }

    goBack() {
        this._location.back();
    }

    getCurrentUser() {
        this.buyerAccountSettingService.getBuyerrProfile().then(res => {
            this.buyer = res[0];
            this.buyer.full_name = this.buyer.first_name + ' ' + this.buyer.last_name;
            // console.log('>>>>>>>>>>>> ' + this.buyer.full_name);
            let profileTemp = this.buyer.profile_pic;
            this.profileImage = profileTemp.substr(69);
            // console.log('>>>>>>>>>>> ' +   this.profileImage);

        }).catch(error => {
            // console.log("error value: " + error);
        });
    }

    editProfile(formData) {
        this.loading = true;
        // console.log(formData);
        this.buyerAccountSettingService.editBuyerProfile(formData).then(res => {
            // console.log(res);
           
            this.getCurrentUser();
            this.uploadImages();
            this.toastr.success('Updated successfully!', 'Success');

            let data = JSON.parse(localStorage.getItem("currentUser"));
            data.first_name = formData.first_name;
            data.last_name = formData.last_name;
            data.email = formData.email;
            data.display_name = formData.display_name;
            data.address = formData.address;
            data.phone = formData.phone;
            
            // console.log(data);
            localStorage.setItem("currentUser", JSON.stringify(data));
            if(!this.imageUploader.queue.length){
                 this.loading = false;
                window.location.reload();      
            }
            // window.location.reload();   

        }).catch(error => {
            // console.log("error value: " + error);
            this.toastr.error('Profile is not updated', 'Oops Error!');
            this.loading = false;
        });
    }

    onUploadImageFailed(item: FileLikeObject, filter: any, options: any) {
        //console.log(item.type);
        switch (filter.name) {
            case 'fileSize':
                this.errorMessage = 'Maximum upload size 8 MB allowed';
                //  console.log(this.errorMessage);
                break;
            case 'mimeType':
                const allowedTypes = this.allowedImageType.join();
                this.errorMessage = 'Type ' + item.type + ' is not allowed. Allowed types:' + allowedTypes;
                //console.log(this.errorMessage);
                break;
            default:
                this.errorMessage = 'Unknown error filter is ' + filter.name;
            //    console.log(this.errorMessage);
        }
    }


    uploadImages() {
        this.getCurrentUser();
        let user_id = this.currentUser.id;

        this.imageUploader.onBuildItemForm = function (fileItem, form) {
            // console.log("upload image function is called :" + fileItem + form);
            form.append('user_id', user_id);
            return { fileItem, form }

        };

        this.imageUploader.uploadAll();
    }

    setLatestImage() {
        this.sharedService.announceImagePath();
    }

}
