import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BuyerProfileComponent } from './buyer-profile.component';
import { FormsModule } from '@angular/forms';
import { BuyerAccountSettingService } from './service/buyer-account-setting.service';
import {FileUploadModule} from "ng2-file-upload";
import { SharedModule } from '../../shared/shared.module';
import { LoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FileUploadModule,
    SharedModule,
    LoadingModule,
    RouterModule.forChild([
      {
        path: '',
        component: BuyerProfileComponent
      }
    ])
  ],
  declarations: [BuyerProfileComponent],
  providers: [ BuyerAccountSettingService ]
})
export class BuyerProfileModule { }
