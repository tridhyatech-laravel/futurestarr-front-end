import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { SharedService } from '../../../../services/shared.service';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
declare var $: any;

@Injectable()
export class BuyerAccountSettingService {
    headers: Headers;
    options: RequestOptions;

    public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
    public serverIp;

    constructor(public http: Http, public sharedService: SharedService) {
        // console.log('data service connected');
        this.serverIp = this.sharedService.apiUrl;
        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.currentUser.token
        });
        this.options = new RequestOptions({ headers: this.headers });
    }


    getBuyerrProfile(): Promise<Object> {

        return this.http.get(this.sharedService.apiUrl + '/api/seller/account?user_id=' + this.currentUser.id, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }

    // editSellerProfile(body: Object):Promise<Object> {
    //    // console.log("seller account function called ******) " + this.currentUser);
    //     return this.http.put(this.sharedService.apiUrl + '/api/seller/account-setting?user_id='+this.currentUser.id, body, this.options).toPromise()
    //         .then(this.extractData)
    //         .catch(this.handleErrorPromise);

    // }

    // getSocialAccountInfo():Promise<Object> {
    //    // console.log("social account information::: ");

    //     return this.http.get(this.sharedService.apiUrl + '/api/seller/social-media?user_id='+this.currentUser.id, this.options).toPromise()
    //            .then(this.extractData)
    //            .catch(this.handleErrorPromise);
    // }



    getBuyerProducts(id) {
        //  console.log("called");
        return this.http.get(this.sharedService.apiUrl + '/api/buyers?user_id=' + id, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);
    }

    deleteBuyerProduct(id) {
        //  console.log("called");
        return this.http.put(this.sharedService.apiUrl + '/api/buyer/delete?buyer_id=' + id + '&user_id=' + this.currentUser.id, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);
    }

    giveRatings(awards) {
        //   console.log("called");
        return this.http.post(this.sharedService.apiUrl + '/api/buyer/rating', awards, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);
    }

    editBuyerProfile(body) {
        // console.log("called*************" + JSON.stringify(body));
        return this.http.put(this.sharedService.apiUrl + '/api/buyer/account/edit?user_id=' + this.currentUser.id, body, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);
    }

    addPromoteProduct(id, body): Promise<Object> {
        // console.log("seller promote products service function called ******) " + this.currentUser);

        return this.http.post(this.sharedService.apiUrl + '/api/seller/promote-product/add?user_id=' + this.currentUser.id + '&promote_id=' + id, body, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);

    }



    private extractData(res: Response) {
        let body = res.json();
        //   console.log("commercial page : below is the extracted from user Piyush ******* " + JSON.stringify(body));
        return body || {};
    }

    // below function can be used when deal with Observabled
    private handleErrorPromise(error: Response | any) {
        // console.error(error.message || error);
        return Promise.reject(error.message || error);
    }

    private handleErrorObservable(error: Response | any) {
        // console.error(error.message || error);
        return Observable.throw(error.message || error);
    }

    // public download_contain(talent_id){
    //     if(this.currentUser.id){
    //         return this.http.put(this.sharedService.apiUrl + '/api/buyer/download_functionallity/talent_ids='+talent_id, this.options).toPromise()
    //         .then(this.extractData)
    //         .catch(this.handleErrorPromise);
    //     }
    // }

    download_contain(talent_id): Promise<Object> {
        return this.http.get(this.sharedService.apiUrl + '/api/buyer/download_functionallity/' + talent_id + '/' + this.currentUser.id, this.options).toPromise()
            .then(this.extractData)
            .catch(this.handleErrorPromise);
    }



}
