import { TestBed, inject } from '@angular/core/testing';

import { BuyerAccountSettingService } from './buyer-account-setting.service';

describe('BuyerAccountSettingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BuyerAccountSettingService]
    });
  });

  it('should be created', inject([BuyerAccountSettingService], (service: BuyerAccountSettingService) => {
    expect(service).toBeTruthy();
  }));
});
