import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { SharedComponent } from './shared.component';
import { TrendingCategorySidebarComponent } from '../../components/trending-category-sidebar/trending-category-sidebar.component';
import { CeiboShare } from '../seller/promote-product/ng2-social-share';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ TrendingCategorySidebarComponent,CeiboShare],
  exports: [TrendingCategorySidebarComponent, CeiboShare]
})
export class SharedModule { }
