import { Component, OnInit, PLATFORM_ID, Inject } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { SharedService } from '../../services/shared.service';
// import { CategoriesService } from '../seller/categories/service/categories.service';
import { BlogService } from './service/blog.service';
import { Title, Meta } from '@angular/platform-browser';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { TrendingCategorySidebarComponent } from '../../components/trending-category-sidebar/trending-category-sidebar.component';
import { isPlatformBrowser } from '@angular/common';



declare var $;

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

  public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
  public categories: any = {};
  public defaultCategory;
  public loading = false;
  public blogContents: any = [];
  public categoryName: any = "";
  public currentBlogCategoryId;
  public blogCount: number;
  public e: any = {};
  constructor(private router: Router,
    private route: ActivatedRoute,
    public sharedService: SharedService,
    public blogService: BlogService,
    private _location: Location, public title: Title, public meta: Meta,
    @Inject(PLATFORM_ID) private platform: Object
  ) {
    this.setMetaTags();

    this.getCtageoriesInfo();

    // var blog = sharedService.blogCategoryAnnounced$.subscribe(res => {
    //   console.log("inside subscribe");
    //    this.e =res;
    //    console.log(res);
    //    var id  = parseInt(this.e.id);
    //    var name = this.e.name;

    //    this.getBlogCtageoriesInfo(id, name);
    // });


    // this.getDefaultCategory();
    let blogCategory = JSON.parse(localStorage.getItem("blogCategory"));
    if (blogCategory) {
      this.getBlogCtageoriesInfo(blogCategory.id, blogCategory.name);
      localStorage.removeItem("blogCategory");
    }
    else {
      localStorage.removeItem("blogCategory");
      this.getDefaultCategory();
    }

  }

  ngOnInit() {
    $(document).on("scroll", function () {
      if ($(document).scrollTop() > 10) {
        document.getElementById("myBtn").style.display = "block";
      } else {
        document.getElementById("myBtn").style.display = "none";
      }
    });
    
  }
  blogtitle:any;

  // Set meta tags
  setMetaTags() {
    this.title.setTitle("Official Future Starr Blog | A Global Talent Platform ");
    this.meta.updateTag({ name: 'keywords', content: 'advertise yourself, future rising stars, entertainment careers, online portfolios, online talent marketplace' });
    this.meta.updateTag({ name: 'description', content: "FutureStarr.com - Create your Unique Blogs and it's pretty easy. For targeted traffic Contact our Unique Talent Marketplace at Future Starr." });
  }


  topFunction() {
    if (isPlatformBrowser(this.platform)) {
      $('html, body').animate({ scrollTop: 0 }, 'fast');
    }
  }
  
  goBack() {
    this._location.back();
  }

  readmore1(blogContent){

    // let id = blogContent.id+"-"+(blogContent.title).replace(/ /g, '-');
    let id = blogContent.id+"-"+((blogContent.title).replace(/\?/g, '')).replace(/ /g, '-');
      this.router.navigate(['/blog/detailed', id])
  }
  readmore()
  {
    this.blogtitle= "One Thing All Successful Celebrities Have In Common";
    let id = 6 +"-"+(this.blogtitle).replace(/ /g, '-');
      this.router.navigate(['/blog/detailed', id])

  }

  getCtageoriesInfo() {
    this.loading = true;
    // console.log("list of categories :: in bolgg ");
    this.blogService.talentCategories().then(res => {
      this.categories = res;
      // console.log(this.categories);
      this.categories = this.categories.category;
      this.loading = false;
    }).catch(error => {
      // console.log("error value: " + error);
      this.loading = false;
    });
  }

  getDefaultCategory() {
    this.loading = true;
    this.blogService.getDefaultCategory().then(res => {
      // console.log("default");

      this.defaultCategory = res;
      // console.log(this.defaultCategory);
      this.getBlogCtageoriesInfo(this.defaultCategory.default_category.id, this.defaultCategory.default_category.name);
      this.loading = false;
    }).catch(error => {
      // console.log("error value: " + error);
      this.loading = false;
    });
  }

  getBlogCtageoriesInfo(id, name) {

    // console.log('***********************');
    this.loading = true;
    this.currentBlogCategoryId = id;
    this.categoryName = name;
    //  console.log('name ******' + name);
    //  console.log('id *******' + id);

    this.blogService.blogTalentCategories(id).then(res => {
      this.blogContents = res;
      this.blogContents = this.blogContents.blog;
      this.loading = false;
      //this.currentBlogCategoryId = this.blogContents[0].category_id;
      this.blogCount = this.blogContents.length;
      // console.log(res)
      this.topFunction();

    }).catch(error => {
      // console.log("error value: " + error);
      this.loading = false;
    });

  }



}
