import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlogComponent } from './blog.component';
// import { BlogDetailedComponent } from './blog-detailed/blog-detailed.component';
import { RouterModule } from '@angular/router';
import { FormsModule} from '@angular/forms';
import { BlogService } from './service/blog.service';
import { LoadingModule } from 'ngx-loading';




@NgModule({
  imports: [
    CommonModule,
    LoadingModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: BlogComponent
      }
      // {
      //   path: 'detailed',
      //   component: BlogDetailedComponent
      // }
    ])
  ],
  declarations: [BlogComponent]

})
export class BlogModule { }
