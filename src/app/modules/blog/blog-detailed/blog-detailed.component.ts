import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { BlogService } from '../service/blog.service';
import { SharedService } from '../../../services/shared.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';


declare var $: any;

@Component({
  selector: 'app-blog-detailed',
  templateUrl: './blog-detailed.component.html',
  styleUrls: ['./blog-detailed.component.css']
})
export class BlogDetailedComponent implements OnInit {

  public sub;
  public blogId;
  public blogData:any ;
  public category:any;
  public subscription1;
  public loading = false;
  public currentBlogCategoryId;
  public categoryName: any = "";
  public blogContents: any = [];
  public blogCount: number;
  public blogCommentId;
  public blogComment:any;
  public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));

  blog_message: any;
  showErrorMessage: boolean = false;

  constructor(private route: ActivatedRoute,
    public blogService: BlogService,
    public sharedService: SharedService,
    public toastr: ToastrService,
    public router: Router,
    private _location: Location) {

      this.sub = this.route.params.subscribe(params => {
        this.blogId = (params['id']).split("/")[0];
      });

    this.getBlogData(this.blogId);

    this.getBlogComments();
    this.topFunction();

  }

  ngOnInit() {
  }
  topFunction() {
    $('html, body').animate({ scrollTop: 0 }, 'fast');
  }

  goBack() {
    this._location.back();
  }

  callCategoryFunction(e) {
    // console.log('inside the blog function :::: ' + e.id);
    // console.log('inside the blog function :::: ' + e.name);
    this.sharedService.announceBlogCategory(e);
    localStorage.setItem("blogCategory", JSON.stringify(e));
    this.router.navigate(['/blog']);
  }

  getBlogData(id) {
    // console.log('detailed id  :: ' + id);
    this.blogCommentId = id;
    this.blogService.getBlogData(id).then(res => {
      //  console.log(res);
      this.blogData = res;
     
      this.getBlogComments();
      this.sharedService.announceSelectedCategory(res['category_id']);

    }).catch(error => {
      // console.log("error value: " + error);
    });
  }

  addBlogComment(formData) {
    // console.log(formData.value);

    if (this.currentUser == null) {
      // console.log("not loggedin")
      // console.log("Please Login first!");
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();

    } else {
      if (this.blog_message) {
        this.loading = true;
        this.showErrorMessage = false;
        // console.log('logged in users ');
        this.blogService.addBlogComment(formData.value).then(res => {
          this.loading = false;
          this.blog_message = '';
          this.getBlogComments();
          this.toastr.success('Post Added successfully', 'Success');
        }).catch(error => {
          this.loading = false;
          this.toastr.error('Post not added', 'Error');
        });
      }
      else {
        this.showErrorMessage = true;
      }
    }
  }

  setTimeOut() {
    // console.log("set timeout")
    var t = setTimeout(() => {
      $('#askToLogin').modal('toggle');
      this.sharedService.announceAskToLogIn(window.location.href);
    }, 1000);
  }

  goToRegister() {
    this.sharedService.announcelogOut();
  }

  getBlogComments() {
    // console.log(this.blogCommentId);
    this.blogService.getBlogComment(this.blogCommentId).then(res => {
      if (res['success']) {
        this.blogComment = res['blogs'];
      }
      //this.blogComment = this.blogComment.blogs
      // console.log(this.blogComment);

    }).catch(error => {
      // console.log("error value: " + error);

    });
  }


}
