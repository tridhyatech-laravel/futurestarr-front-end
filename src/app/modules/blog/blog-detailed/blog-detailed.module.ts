import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlogComponent } from '../blog.component';
import { BlogDetailedComponent } from './blog-detailed.component';
import { RouterModule } from '@angular/router';
import { FormsModule} from '@angular/forms';
import { LoadingModule } from 'ngx-loading';
import { BlogService } from '../service/blog.service';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    LoadingModule,
    SharedModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: BlogDetailedComponent
      }
      // {
      //   path: 'detailed',
      //   component: BlogDetailedComponent
      // }
    ])
  ],
  declarations: [BlogDetailedComponent]
})
export class BlogDetailedModule { }
