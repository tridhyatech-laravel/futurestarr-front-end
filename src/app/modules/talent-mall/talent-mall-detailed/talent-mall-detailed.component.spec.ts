import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TalentMallDetailedComponent } from './talent-mall-detailed.component';

describe('TalentMallDetailedComponent', () => {
  let component: TalentMallDetailedComponent;
  let fixture: ComponentFixture<TalentMallDetailedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TalentMallDetailedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TalentMallDetailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
