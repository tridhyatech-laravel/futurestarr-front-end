import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap, NavigationEnd, NavigationStart } from '@angular/router';
import { GetProductsService } from '../../service/get-products.service';
import { SharedService } from '../../../../services/shared.service';
import { ToastrService } from 'ngx-toastr';


declare var $: any;
declare var videojs: any;
@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.css']
})
export class ProductInfoComponent implements OnInit {
  public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
  public sub;
  public talentId;
  public addToCartSpinner = false;
  public talent;
  public similarTalents;
  public awardsObj: any = {};
  public loading: boolean = false;
  public addingItemToCart = false;
  public userAssetResponse: any = {};
  public getAwardObj: any = {};
  public cartObj: any = {};
  public showAddToCart = true;
  public fanBaseUsersCount = 0;
  public buyerContact: any = {};
  public shareObj: any = {};
  public repoUrl;
  public talent_comments: any;
  public talent_comments_count: any;

  ridres: any;
  selfRider: boolean = true;
  totalRiders: any = 0;

  //public repoUrl = 'https://github.com/Epotignano/ng2-social-share';
  // public imageUrl = 'https://avatars2.githubusercontent.com/u/10674541?v=3&s=200';

  constructor(private router: Router,
    private _location: Location,
    public getProductsService: GetProductsService,
    private route: ActivatedRoute,
    public sharedService: SharedService, public toastr: ToastrService) {
    this.addingItemToCart = false;

  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.talentId = params['product_id'];
      // console.log(window.location.href);
      // console.log(this.talentId);
      this.repoUrl = window.location.href;
      // this.repoUrl = "https://www.google.com";
      this.getTalentDetailed();
      $('html, body').animate({ scrollTop: 0 }, 'fast');
    });

    // this.repoUrl =  window.location.href;
    // console.log(this.repoUrl);

  }

  getSocialShareMediaName(social) {

  }

  shareOnSocialMedia(social){

  }

  initPlayer() {
    setTimeout(() => {
      try {
        var player = videojs('my-player');
        // console.log(player);
      } catch (error) {
        // console.log("player");
        // console.log(player);
      }
    }, 300);
  }

  isVideo(new_product_img_path) {
    let viedos = ['mp4', 'MP4', 'mov', 'MOV', 'mkv', 'MKV', 'FLV', 'flv'];
    if (new_product_img_path) {
      let fileExtension = new_product_img_path.replace(/^.*\./, '');
      if (viedos.includes(fileExtension)) {
        return true;
      }
    }
  }

  isImage(new_product_img_path) {
    let images = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'GIF', 'gif'];
    if (new_product_img_path) {
      let fileExtension = new_product_img_path.replace(/^.*\./, '');
      if (images.includes(fileExtension)) {
        return true;
      }
    }
  }

  topFunction() {
    $('html, body').animate({ scrollTop: 0 }, 'fast');
  }

  showSendMessageModal() {
    if (this.currentUser) {
      $('#sendMessage').modal({ backdrop: 'static' });
    }
    else {
      // this.toastr.warning("Please Login first");
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();
    }
  }

  addToCartModal() {
    if (this.currentUser != null) {
      if (this.currentUser.role_id == "buyer") {
        this.addingItemToCart = true;
        this.addTalentToCart();
      }
      else {
        // console.log("askToJoinAsBuyer");
        // $('#askToJoinAsBuyer').modal({ backdrop: 'static' });
        $('#askToLogin').modal({ backdrop: 'static' });
        this.setTimeOut();
      }
    }
    else {
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();
    }
  }

  goToRegister() {
    this.sharedService.announcelogOut();
    this.sharedService.announceAskToLogIn(window.location.href);
  }

  openAwardTalent() {
    // //console.log("Checking User ...");
    if (this.currentUser != null) {
      $('#giveAward').modal({ backdrop: 'static' });
    }
    else {
      //console.log("Please Login first!");
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();
    }
  }

  openProductComment(talent_id_data) {
    // if (this.talent.talent[0].total_coments.length) {
    this.loading = true;

    this.getProductsService.get_all_comment(talent_id_data).then(res => {
      this.talent_comments = res;
      if (this.talent_comments.length > 0) {
        this.talent_comments_count = this.talent_comments.length;
      } else {
        this.talent_comments_count = "0";
      }
    }).catch(error => {

    });
    $('#openComment').modal({ backdrop: 'static' });
    this.loading = false;
    // }
  }

  openRiderModal(talentId) {
    if (this.currentUser != null) {
      this.loading = true;
      let user_id = this.sharedService.currentUser.id;

      this.getProductsService.get_all_riders(talentId, user_id).then(data => {
        this.loading = false;
        this.selfRider = data['self_rider'];
        if (data['success']) {
          this.totalRiders = data['riders'].length;
          this.ridres = data['riders'];

        }
        else {

        }
      }).catch(error => {
        this.loading = false;

      });

      $('#riderModal').modal({ backdrop: 'static' });
    }
    else {
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();
    }
  }

  rideTalents() {
    let dataToSend = {
      user_id: this.sharedService.currentUser.id,
      talent_id: this.talent.talent[0].id
    }
    this.getProductsService.addTalentRider(dataToSend).then(data => {
      if (data['success']) {
        this.toastr.success(data['message']);
        $('#riderModal').modal('hide');
        // this.getSocialBuzzInfo();
        this.getTalentDetailed();
      }
      else {
        this.toastr.error(data['message']);
      }
    })
      .catch(error => {

      });
  }

  setTimeOut() {
    var showLoginBox = document.getElementById('showLoginBox');
    var t = setTimeout(() => {
      $('#askToLogin').modal('toggle');
      this.sharedService.announceAskToLogIn(window.location.href);
    }, 1000);
  }
  goBack() {
    this._location.back();
  }

  getTalentDetailed() {

    this.loading = true;
    var product_id = this.talentId;
    // //console.log(product_id);
    this.addingItemToCart = false;
    this.getProductsService.getTalentDetailed(product_id).then(res => {
      //console.log('jfjfjfjfjff0');
      this.loading = false;
      if (res['talent']) {
        let data = {
          talent: res['talent']
        }
        this.talent = data;
        //console.log(this.talent);
        this.getSimilarTalents(this.talent.talent[0].talent_category_id, this.talent.talent[0].id);
        this.getTalentAwards();
        // this.initPlayer();
      }
      else {
        this.toastr.warning(res['message']);
        this.router.navigate(['/talent-mall'])
      }



    }).catch(error => {
      //console.log("error value: " + error);
    });
  }


  getSimilarTalents(category_id, talentId) {
    //console.log('****************************************');
    this.getProductsService.getSimilarTalents(category_id, talentId).then(res => {
      this.similarTalents = res;
      // //console.log(this.getSimilarTalents);
    }).catch(error => {
      //console.log("error value: " + error);
    });
  }

  // giveAwards(awards){
  //   ////console.log(awards);
  //   this.awardsObj.awards = awards.value.rating;
  //   this.awardsObj.comment = awards.value.comment;
  //   this.awardsObj.user_id =  this.currentUser.id
  //   this.awardsObj.talent_id =  this.talent.talent[0].id;
  //   //console.log(this.awardsObj);
  //   awards.reset();
  //   $('#giveAward').modal('toggle');
  //   this.getProductsService.giveAwards(this.awardsObj).then(res => {
  //     //console.log(res);
  //   }).catch(error => {
  //     //console.log("error value: " + error);
  //   });
  // }

  awardTalent(flag) {
    this.awardsObj.flag = flag;
    this.awardsObj.talentId = this.talent.talent[0].id;
    this.awardsObj.userId = this.currentUser.id;
    this.getProductsService.giveAwards(this.awardsObj).then(res => {
      $('#giveAward').modal('toggle');
      this.getTalentAwards();
    }).catch(error => {
      //console.log("error value: " + error);
    });
  }

  getTalentAwards() {
    this.getAwardObj.talentId = this.talent.talent[0].id;
    this.getAwardObj.userId = this.currentUser.id;
    this.getProductsService.getAwardAndCartDetails(this.getAwardObj).then(res => {
      this.userAssetResponse = res;
      this.fanBaseUsersCount = this.userAssetResponse.fan_base.length;
      if (this.userAssetResponse.is_this_item_in_usercart == true) {
        this.showAddToCart = false;
      } else {
        this.showAddToCart = true;
      }
    }).catch(error => {
      //console.log("error value: " + error);
    });
  }

  addTalentToCart() {
    this.addToCartSpinner = true;
    this.cartObj.userId = this.currentUser.id;
    this.cartObj.talentId = this.talent.talent[0].id;
    this.cartObj.price = this.talent.talent[0].price;
    this.cartObj.title = this.talent.talent[0].title;
    //console.log('add to cart >>>>>>>> ' + JSON.stringify(this.talent.talent[0].id));
    this.getProductsService.addTalentToCart(this.cartObj).then(res => {     
      this.addToCartSpinner = false;
      if(res["success"]){
        this.toastr.success(res['message']);
        this.getTalentAwards();
        this.sharedService.announcecartItem();
      }
      else{
        this.addingItemToCart = false;
        this.toastr.warning(res['message']);
      }
    }).catch(error => {
      ////console.log("error value: " + error);
      this.toastr.error('Item is already in your cart!', 'Error Occurred!');
    });
  }

  addBuyerContact(body) {
    this.loading = true;
    this.buyerContact.title = body.title;
    this.buyerContact.message = body.message;
    this.buyerContact.user_id = this.currentUser.id;
    this.buyerContact.talent_id = this.talent.talent[0].id;

    //console.log(this.talent.talent);

    let frmData = new FormData();
    frmData.set("currentUser", JSON.parse(localStorage.getItem('currentUser')).id);
    frmData.set("received_by", this.talent.talent[0].user_id);
    frmData.set("message", this.buyerContact.message);

    this.sharedService.store_message(frmData);

    this.getProductsService.addBuyerContactMessage(this.buyerContact).then(res => {
      //console.log('message added in table');

      $('#sendMessage').modal('hide');

      this.toastr.success('Message has been added', 'Success!');
      body.reset();
      this.loading = false;
    }).catch(error => {
      //console.log("error value: " + error);
      this.toastr.error('Message is already present!', 'Error Occurred!');
      this.loading = false;
    });
  }

  shareModal() {
    //  //console.log('Helloooooooooo');
    // //console.log("report product ***** ..." + id);
    if (this.currentUser != null) {
      // this.shareObj.post_id = id;
      $('#shareModal').modal({ backdrop: 'static' });
    }
    else {
      // //console.log("Please Login first!");
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();
    }
  }


}
