import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { GetProductsService } from '../service/get-products.service';

declare var $: any;

@Component({
  selector: 'app-talent-mall-detailed',
  templateUrl: './talent-mall-detailed.component.html',
  styleUrls: ['./talent-mall-detailed.component.css']
})
export class TalentMallDetailedComponent implements OnInit {
  public sub;
  public categoryId;
  public starId;
  public loading: boolean = true;
  public products;
  public p: number = 1;
  public productCount: number;
  public selectedProductIds = [];
  public selectedStarId= [];
  public talentCategories;
  public priceSlider=100000;
  public ramMaxValue:number= 100000;
  public stars = [{"name":"five-star", "id": 5, "value": 5 },
                  {"name":"four-star", "id": 4, "value": 4 },
                  {"name":"three-star", "id": 3,"value": 3 },
                  {"name":"two-star", "id": 2,"value": 2 },
                  {"name":"one-star", "id": 1, "value": 1 }];
  public startsDefaultValue = [5,4,3,2,1];
  public categoryAsset;

  constructor(
               private _location: Location,
               private route: ActivatedRoute,
               public router:Router,
               public getProductsService: GetProductsService)
     {
      this.sub = this.route.params.subscribe(params => {
        this.categoryId = params['selected_category_id'];
      });
      this.selectedProductIds[0] = this.categoryId;

      this.getTalentCategories();
      this.getTalents();
      this.getCategoryBanner();
    }

  ngOnInit() {
  }

  isVideo(new_product_img_path){
    let viedos = ['mp4', 'MP4', 'mov', 'MOV', 'mkv', 'MKV'];
    if (new_product_img_path)
    {
      let fileExtension = new_product_img_path.replace(/^.*\./, '');
      if( viedos.includes(fileExtension)){
        return true;
      }
    }
  }

  goBack() {
    this._location.back();
  }

  topFunction() {
      $('html, body').animate({ scrollTop: 0 }, 'fast');
  }

  setRam(value){
    this.priceSlider = value;
    //console.log(this.priceSlider);
      this.getTalents();
 }


  checkedCategroy(event, id) {
    this.loading = true;
    if (event.target.checked == true) {
      this.selectedProductIds.push(id);
    }
    if (event.target.checked == false) {
      var temp = this.selectedProductIds.indexOf(id);
      this.selectedProductIds.splice(temp, 1);
      // //console.log(temp);

    }
    this.getTalents();
    // this.loading = false;
  //  //console.log(this.selectedProductIds);
  }

  productNavigate(product_id){
    this.router.navigate(['/talent-mall/product-info/'+product_id], { relativeTo: this.route })
  }

  checkedStars(event, id) {
    this.loading = true;
    if (event.target.checked == true) {
      this.selectedStarId.push(id);
    }
    if (event.target.checked == false) {
      var temp1 = this.selectedStarId.indexOf(id);
      this.selectedStarId.splice(temp1, 1);
    }
    this.getTalents();
    // this.loading = false;
    //console.log("star value >>>>>> " + this.selectedStarId);
  }


  getTalentCategories() {
    this.getProductsService.getCategories().then(res => {
      this.talentCategories = res;
      // //console.log(this.talentCategories);
    }).catch(error => {
      ////console.log("error value: " + error);
    });
  }

  showVideoControls(productid){
    let videoTagId = document.getElementById("video"+productid);
    if(!videoTagId.hasAttribute("controls")){
      videoTagId.setAttribute("controls","controls") 
    }
  }

  hideVideoControls(productid){
    let videoTagId = document.getElementById("video"+productid);
    if(videoTagId.hasAttribute("controls")){
      videoTagId.removeAttribute("controls");   
    }
  }

  selectAll(event) {
    this.loading = true;
    if (event.target.checked == true) {
      $(".case").prop('checked', true);
      this.selectedProductIds = [];
      for (var i = 0; i < this.talentCategories.category.length; i++) {
        var currentId = this.talentCategories.category[i].id;
        this.selectedProductIds.push(currentId);
      }
    }
    if (event.target.checked == false) {
      $(".case").prop('checked', false);
      $('#'+this.categoryId).prop('checked', true);
      this.selectedProductIds = [];
      this.selectedProductIds[0] = this.categoryId;
    }
    this.getTalents();
  //  //console.log(this.selectedProductIds);
  }

  getTalents() {
    var ids = this.selectedProductIds;
    var starid = this.selectedStarId;
    var price = this.priceSlider;
    var defaultStar = this.startsDefaultValue;
    //console.log("^^^^^^^^^^" + price);
    //console.log('>>>>>>>>> ' + this.startsDefaultValue);
    this.getProductsService.getTalents(ids,starid,price,defaultStar).then(res => {
      this.loading = false;
      this.products = res;
      this.productCount = this.products.products.length;
      //this.checkProducts();
      //  //console.log('@@@@@@@@@@@@ ' + JSON.stringify(this.products));
    }).catch(error => {
      //console.log("Recalling...")
      this.getTalents();
    });
  }

  checkProducts() {
    ////console.log(this.productCount);
    for (var i = 0; i < this.products.products.length; i++) {
      if (this.products.products[i].product_info.length > 220) {
        var shortDesc = this.products.products[i].product_info.slice(0, 220);
        this.products.products[i].shortDesc = shortDesc.concat("...");
      }
    }
  }

  getCategoryBanner() {
  ////console.log(ids)
    this.getProductsService.getCategoryBanner(this.categoryId).then(res => {
    this.loading = false;
    this.categoryAsset = res;
    //console.log(res);
    }).catch(error => {

    });
  }

    productCounter(product_id){
        //console.log('########### ' + product_id);
        this.getProductsService.getProductCounter(product_id).then(res =>{
            ////console.log(res);
        }).catch(err => {
            //console.log(err);
        })

        // if(localStorage.getItem("")){
        //   this.router.navigate(['/talent-mall/product-info', product_id ]);
        // }
        // else{
          
        // }
    }


}
