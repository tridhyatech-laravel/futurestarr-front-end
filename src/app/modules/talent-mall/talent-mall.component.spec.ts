import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TalentMallComponent } from './talent-mall.component';

describe('TalentMallComponent', () => {
  let component: TalentMallComponent;
  let fixture: ComponentFixture<TalentMallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TalentMallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TalentMallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
