import { Component, OnInit } from '@angular/core';
import { GetProductsService } from './service/get-products.service'
// import { ToastsManager, ToastOptions } from 'ng2-toastr/ng2-toastr';
import { CeiboShare } from '../seller/promote-product/ng2-social-share';
import { Location } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';

declare var $: any;

@Component({
  selector: 'app-talent-mall',
  templateUrl: './talent-mall.component.html',
  styleUrls: ['./talent-mall.component.css'],
})
export class TalentMallComponent implements OnInit {

  public selected_category_id: number;
  public loading: boolean = true;
  public talentCategories;
  public categoriesLength;
  public changeLayoutFlag = 0;
  public lastCategory: number;
  public showErrorDiv = false;
  public secondLastCategory: number;

  public repoUrl = 'https://github.com/Epotignano/ng2-social-share';

  constructor(public getProductsService: GetProductsService, private _location: Location, private titleService: Title, public meta: Meta) {
    this.setMetaTags();
  }

  getTalentCategories() {
    this.getProductsService.getCategories().then(res => {
      this.loading = false;
      this.talentCategories = res;
      //console.log(res);
      this.categoriesLength = this.talentCategories.category.length;
      this.checkLength();
      this.loading = false;
    }).catch(error => {
      this.loading = false;
      this.showErrorDiv = true;
      //console.log("error value: " + error);
    });
  }

  setMetaTags() {
    this.titleService.setTitle("Future Starr Talent Mall: Browse and Purchase Talent");
    this.meta.updateTag({ name: 'description', content: "Sign Up with Future Starr Today! We will help you to get started. Whatever you are espired in, we will give you inspiration to fulfill your respiration." });
    this.meta.updateTag({ name: 'keywords', content: "Rising Star, future star talent show, purchase talent, talent browse, explore talent, america's got talent" });

  }

  ngOnInit() {
    this.getTalentCategories();
  }

  topFunction() {
    $('html, body').animate({ scrollTop: 0 }, 'fast');
  }

  goBack() {
    this._location.back();
  }

  checkLength() {
    var l = this.categoriesLength;
    this.changeLayoutFlag = l % 4;
    //console.log(this.changeLayoutFlag);
    this.lastCategory = l - 1;
    this.secondLastCategory = l - 2;
  }

}
