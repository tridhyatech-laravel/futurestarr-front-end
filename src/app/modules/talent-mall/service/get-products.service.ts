import { Injectable } from '@angular/core';
import { SharedService } from '../../../services/shared.service';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
declare var $: any;


@Injectable()
export class GetProductsService {

  headers: Headers;
  options: RequestOptions;
  public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
  public serverIp;
  constructor(public http: Http, public sharedService: SharedService) {
    this.serverIp = this.sharedService.apiUrl;
    this.headers = new Headers({ 'Content-Type': 'application/json' });
    this.options = new RequestOptions({ headers: this.headers });
  }

  getTalents(id=[], startid=[], price, defsultstar=[]): Promise<Object> {
    //console.log("d ******) " + id);
    return this.http.get(this.sharedService.apiUrl + '/api/talent-mall/products?category_id='+id+'&star_id='+startid+'&price='+price+'&defaultstar='+defsultstar, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }


  getTalentDetailed(id): Promise<Object> {
    return this.http.get(this.sharedService.apiUrl + '/api/talent-mall/product-info?talent_id=' + id, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  getSimilarTalents(id, talent_id): Promise<Object> { 
    //console.log(talent_id);
    return this.http.post(this.sharedService.apiUrl + '/api/talent-mall/similar-products?talent_category_id=' + id+"&talent_id="+talent_id, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }



  getCategories() {
    return this.http.get(this.sharedService.apiUrl + '/api/seller/categories', this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  giveAwards(awards) {
    //console.log("called");
    return this.http.post(this.sharedService.apiUrl + '/api/talent-mall/give-awards', awards, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  addTalentRider(data){
    return this.http.post(this.sharedService.apiUrl + '/api/talent-mall/add-rider', data, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  getAwardAndCartDetails(data) {
    //console.log("called *****-")
    return this.http.post(this.sharedService.apiUrl + '/api/talent-mall/get-awards-and-cart-details', data, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  addTalentToCart(data) {
    //console.log("called *****-")
    return this.http.post(this.sharedService.apiUrl + '/api/talent-mall/add-to-cart', data, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  addBuyerContactMessage(data){
    return this.http.post(this.sharedService.apiUrl + '/api/talent-mall/message', data, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  // FOR ADD A MESSAGE IN CHAT MESSAGE :- CHETAN CHOUHAN
  store_message_from_buyer_dashboard(receiverId,message_send): Promise<Object>{ 
    //console.log("called 101"); 
    let frmData = new FormData();
    frmData.set("currentUser", JSON.parse(localStorage.getItem('currentUser')).id);
    frmData.set("received_by", receiverId);
    frmData.set("message", message_send);
    return this.http.post(this.sharedService.apiUrl + '/api/message/store_msg/', frmData).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  getCategoryBanner(id){
    return this.http.get(this.sharedService.apiUrl + '/api/talent-mall/category_banners?id='+id, this.options).toPromise()
    .then(this.extractData)
    .catch(this.handleErrorPromise);
  }
  
  getProductCounter(product_id){
    return this.http.put(this.sharedService.apiUrl + '/api/talent-mall/count/view?product_id='+product_id, this.options).toPromise()
    .then(this.extractData)
    .catch(this.handleErrorPromise)
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  private extractData(res: Response) {
    let body = res.json();
    // //console.log("below is the extracted data of categories ::" + JSON.stringify(body));
    // //console.log(body);
    return body || {};
  }

  private handleErrorPromise(error: Response | any) {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
  }

  get_all_comment(talent_id_data){
    return this.http.get(this.sharedService.apiUrl + '/api/talent-mall/getallcomment/'+talent_id_data, this.options).toPromise()
    .then(this.extractData)
    .catch(this.handleErrorPromise)
  }

  get_all_riders(talent_id_data, user_id){
    return this.http.get(this.sharedService.apiUrl + '/api/talent-mall/getallriders/'+talent_id_data +"/"+user_id, this.options).toPromise()
    .then(this.extractData)
    .catch(this.handleErrorPromise)
  }


}
