import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TalentMallComponent } from './talent-mall.component';
import { TalentMallDetailedComponent } from './talent-mall-detailed/talent-mall-detailed.component';
import { ProductInfoComponent } from './talent-mall-detailed/product-info/product-info.component';
import { GetProductsService } from './service/get-products.service';
import { LoadingModule } from 'ngx-loading';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import {ShareButtonsModule} from 'ngx-sharebuttons';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxPaginationModule,
    LoadingModule,
    SharedModule,
    ShareButtonsModule.forRoot(),
    RouterModule.forChild([
      {
        path: '',
        component: TalentMallComponent
      },
      {
        path: 'detailed/:selected_category_id',
        component: TalentMallDetailedComponent
      },
      {
        path: 'product-info/:product_id',
        component: ProductInfoComponent
      }
    ])
  ],
  declarations: [TalentMallComponent,
    TalentMallDetailedComponent,
    ProductInfoComponent
    ],
    bootstrap: [TalentMallComponent],

    providers: [GetProductsService]
})
export class TalentMallModule { }
