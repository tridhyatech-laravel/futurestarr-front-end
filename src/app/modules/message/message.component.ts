import { Component, OnInit, ElementRef, ViewChild, PLATFORM_ID, Inject } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SharedService, image_path_url } from '../../services/shared.service';
import { ToastrService } from 'ngx-toastr';
import { MessageService } from './service/message.service';
import { LoadingModule } from 'ngx-loading';
import { Location, isPlatformBrowser, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Message } from '@stomp/stompjs';
// import { StompConfig, StompService } from '@stomp/ng2-stompjs';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
// import { StompState } from '../../../stomp-state';
// import { StompRService } from '../../../stomp-r.service';
declare var $: any;

@Component({
    selector: 'app-message',
    templateUrl: './message.component.html',
    styleUrls: ['./message.component.css'],
    providers: [SharedService]
})
export class MessageComponent implements OnInit {
    @ViewChild('scrollMe') private myScrollContainer: ElementRef;
    @ViewChild('titleInput') titleInput: ElementRef;
    @ViewChild('scrollMessageContent') private scrollMessageContent: ElementRef;
    username: any;
    public loading: boolean = true;
    last_name: any;
    current_user_id: any;
    profile_pic: any;
    all_users: any;
    image_path: any;
    message_username: any;
    userfirst: any;
    userLast: any;
    user_message: any = [];
    receiverId: any;
    fileToUpload: any;
    // titleInput:any;
    interval: any;
    today_date: any;
    favrate_res: any;
    favrate_resp: any;
    favrate_id: any;
    favrate_user_list: any;
    all_contact_list: any = [];
    unread_count_value: any;
    composemessage: any;
    messageid: any;
    firstmsgshow = 0;
    isDeleteChecked: boolean;
    isDeleted: boolean = false;

    userSelectedFile: any;
    userTextInput: boolean = false
    current_chat_id: any;
    current_chat_userid: any;
    current_chat_userImage: any = 'assets/images/profile.png';

    settings: any;
    automaticMessage: any;
    favsearchText: any;
    role_id: any;
    searchText: any;

    own_profile_pic = JSON.parse(localStorage.getItem("currentUser")).profile_pic
    showOwnImage: any = (this.own_profile_pic) ? this.sharedService.apiUrl + "/public/" + this.own_profile_pic : 'assets/images/profile.png';

    public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));

    constructor(public sharedService: SharedService,
        public toastr: ToastrService,
        public router: Router,
        private _location: Location,
        // private _stompService: StompService,
        private activatedRoute: ActivatedRoute,
        // private _stompService1: StompRService
        @Inject(PLATFORM_ID) private platform: Object,
    ) {
        // let stomp_subscription = this._stompService.subscribe('/topic/ng-demo-sub');

        // console.log(stomp_subscription);

        // stomp_subscription.map((message: Message) => {
        //     return message.body;
        // }).subscribe((msg_body: string) => {
        //     //   console.log(`Received: ${msg_body}`);
        // });

        // this._stompService.publish('/topic/ng-demo-sub', 'My important message');

        // this._stompService.state
        //     .map((state: number) => StompState[state])
        //     .subscribe((status: string) => {
        //         //   console.log(`Stomp connection status: ${status}`);
        //     });

        this.username = this.currentUser.first_name;
        this.last_name = this.currentUser.last_name;
        this.current_user_id = this.currentUser.id;
        if (this.currentUser.profile_pic)
            this.profile_pic = image_path_url + this.currentUser.profile_pic;
        // console.log(this.currentUser);
        // console.log(this.profile_pic);
        this.get_all_user();
        this.get_unread_count();
        this.today_date = new Date();



        // this.interval=setInterval(()=>{
        //     this.show_all_messages(this.receiverId,this.userfirst,this.userLast);
        //     this.get_all_user();
        //     this.get_unread_count();
        // },3000 );
        setTimeout(() => {
            this.settings = JSON.parse(localStorage.getItem("currentUser")).auto_reply;
            this.automaticMessage = JSON.parse(localStorage.getItem("currentUser")).automatic_message;
        }, 300);

        this.role_id = JSON.parse(localStorage.getItem("currentUser")).role_id;

    }

    public initStomp() {
        // this._stompService1.initAndConnect();
        // console.log('kdkdkkd');
    }

    topFunction() {
        if (isPlatformBrowser(this.platform)) 
        $('html, body').animate({ scrollTop: 0 }, 'fast');
    }

    ngOnInit() {
        if (isPlatformBrowser(this.platform)) {
        $('.option-box a').on("click", function () {
            if ($($(this).parent('.option-box').data("target")).selector == '#option1') {
                var a = $($(this).parent('.option-box').data("target"))
                a.addClass('active');
                $('#option1-a').css('cssText', 'color: #fff !important');

                $('#option2').removeClass('active');
                $('#option3').removeClass('active');
                $('#option4').removeClass('active');
                $('#option2-a').css('cssText', 'color: #ccc !important');
                $('#option3-a').css('cssText', 'color: #ccc !important');
                $('#option4-a').css('cssText', 'color: #ccc !important');

            }
            if ($($(this).parent('.option-box').data("target")).selector == '#option2') {
                var a = $($(this).parent('.option-box').data("target"))
                a.addClass('active');
                $('#option2-a').css('cssText', 'color: #fff !important');

                $('#option1').removeClass('active');
                $('#option3').removeClass('active');
                $('#option4').removeClass('active');
                $('#option1-a').css('cssText', 'color: #ccc !important');
                $('#option3-a').css('cssText', 'color: #ccc !important');
                $('#option4-a').css('cssText', 'color: #ccc !important');

            }
            if ($($(this).parent('.option-box').data("target")).selector == '#option3') {
                var a = $($(this).parent('.option-box').data("target"))
                a.addClass('active');
                $('#option3-a').css('cssText', 'color: #fff !important');

                $('#option2').removeClass('active');
                $('#option1').removeClass('active');
                $('#option4').removeClass('active');
                $('#option2-a').css('cssText', 'color: #ccc !important');
                $('#option1-a').css('cssText', 'color: #ccc !important');
                $('#option4-a').css('cssText', 'color: #ccc !important');

            }
            if ($($(this).parent('.option-box').data("target")).selector == '#option4') {
                var a = $($(this).parent('.option-box').data("target"))
                a.addClass('active');
                $('#option4-a').css('cssText', 'color: #fff !important');

                $('#option2').removeClass('active');
                $('#option3').removeClass('active');
                $('#option1').removeClass('active');
                $('#option2-a').css('cssText', 'color: #ccc !important');
                $('#option3-a').css('cssText', 'color: #ccc !important');
                $('#option1-a').css('cssText', 'color: #ccc !important');

            }
            $($('.tab-pan')).hide();
            $($(this).data("target")).show();

        });

        //--------emojies----------
        var emojiEl = $('#Emojii').emojioneArea({
            pickerPosition: "Left"
        });
    }
        // console.log(emojiEl);
        // emojiEl[0].emojioneArea.on("keyup", function(events){
        //     console.log(events);
        // });
        // this.activatedRoute.queryParams.subscribe((params: Params) => {
        //     let userId = params['user_id'];
        //     let userfirsts = params['first_names'];
        //     let userLasts = params['last_names'];
        //     let chat_id = params['chat_id'];
        //     this.show_all_messages(userId,userfirsts,userLasts, chat_id)
        // });
        this.topFunction();
    }

    ngOnDestroy() {
        // Clear interval on message page close
        clearInterval(this.interval);
    }

    fileInputSelect(event) {
        // console.log(event.target.files[0]);
        if (event.target.files[0])
            this.userSelectedFile = event.target.files[0];
        // console.log(this.userSelectedFile);
    }

    deleteSelectedFile() {
        this.userSelectedFile = null;
    }

    ngAfterViewChecked() {
        // this.scrollToBottom();
    }

    get_unread_count() {
        this.sharedService.get_unread_count().then(res => {
            // console.log("unread count - ");
            // console.log(res);
            this.unread_count_value = res['unread_count'];
        }).catch(error => {
            //    console.log("error value: " + error);
        })
    }

    scrollToBottom(): void {
        try {
            this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
        } catch (err) { }
    }

    get_all_user(show = null) {
        if (isPlatformBrowser(this.platform)) {
        $('#allmsg').addClass('active');
        $('#readmsg').removeClass('active');
        $('#unrmsg').removeClass('active');
        }
        this.sharedService.get_all_users().then(res => {
            // console.log(res);
            this.all_users = res;
            this.image_path = image_path_url;
            this.loading = false;

            if (this.firstmsgshow == 0 || this.isDeleted || show == "all") {
                this.firstmsgshow = 1;
                this.isDeleted = false;
                this.current_chat_id = res[0].chat_id;
                // console.log("inside......................");
                this.show_all_messages(res[0].user_id, res[0].first_name, res[0].last_name, res[0].chat_id, res[0]);
            }
        }).catch(error => {
            //    console.log("error value: " + error);
        })
    }
    //-------get read message-------
    get_read_msg() {
        if (isPlatformBrowser(this.platform)) {
        $('#readmsg').addClass('active');
        $('#allmsg').removeClass('active');
        $('#unrmsg').removeClass('active');
        }
        this.loading = true;
        this.sharedService.get_read_msg().then(res => {
            this.all_users = res;
            this.image_path = image_path_url;
            this.loading = false;
        }).catch(error => {
            //    console.log("error value: " + error);
        })
    }

    //-------get unread message-------
    get_unread_msg() {
        if (isPlatformBrowser(this.platform)) {
        $('#readmsg').removeClass('active');
        $('#allmsg').removeClass('active');
        $('#unrmsg').addClass('active');
        }
        this.loading = true;
        this.sharedService.get_unread_msg().then(res => {
            this.all_users = res;
            this.image_path = image_path_url;
            this.loading = false;
        }).catch(error => {
            //    console.log("error value: " + error);
        })
    }

    contactSearch() {
        this.sharedService.searchContact(this.favsearchText.trim()).then(data => {
            if (data['success']) {
                this.all_contact_list = data['contacts']
            }
            else {
                this.all_contact_list.length = 0;
            }

        })
            .catch(error => {

            })
    }

    show_all_messages(receiverId, userfirst, userLast, chat_id, user) {
        this.get_unread_count();
        if (this.user_message)
            this.user_message.length = 0;
        // console.log(user);

        this.current_chat_userid = receiverId;
        this.userfirst = userfirst;
        this.userLast = userLast;
        this.receiverId = receiverId;
        this.loading = true;
        this.current_chat_id = chat_id;
        if (user.profile_pic) {
            this.current_chat_userImage = this.image_path + user.profile_pic;
        }
        else {
            this.current_chat_userImage = 'assets/images/profile.png';
        }

        // console.log(this.current_chat_id);
        this.sharedService.check_is_favorite(receiverId).then(res => {
            this.favrate_resp = res;
            this.favrate_res = this.favrate_resp.is_fav;
            this.favrate_id = receiverId;
            this.loading = false;
        }).catch(error => {

        });
        if (chat_id) {
            this.sharedService.get_all_user_msg(chat_id).then(res => {
                // this.get_all_user();
                // this.message_username=userName;

                this.user_message = res;
                this.image_path = image_path_url;
                setTimeout(() => {
                    this.scrollMessageToBottom();
                }, 100);
            }).catch(error => {
                //    console.log("error value: " + error);     
            })
        }
    }

    send_message(receiverId, message_send) {
    
        if (message_send || this.userSelectedFile) {
        //    console.log( this.userSelectedFile);
            this.loading = true;
            // console.log(receiverId+message_send);
            let frmData = new FormData();

            if (this.userSelectedFile)
                frmData.set("message_file", this.userSelectedFile, this.userSelectedFile.name);

            frmData.set("currentUser", JSON.parse(localStorage.getItem('currentUser')).id);
            frmData.set("received_by", this.current_chat_userid);
            frmData.set("message", message_send);
            // console.log(frmData); 

            this.sharedService.store_message(frmData).then(data => {
                this.loading = false;
                if (data['success']) {
                    this.userSelectedFile = null;
                    // this.titleInput.nativeElement.value = '';
                    if (isPlatformBrowser(this.platform)) {
                        $('#Emojii').emojioneArea().data("emojioneArea").setText('');
                    }
                    if (data['last_chat']) {
                        this.user_message.push(data['last_chat']);
                        setTimeout(() => {
                            this.scrollMessageToBottom();
                        }, 100);
                    }
                    if (data['auto_reply']) {
                        this.user_message.push(data['auto_reply']);
                        setTimeout(() => {
                            this.scrollMessageToBottom();
                        }, 100);
                    }
                }
            })
                .catch(error => {
                    this.loading = false;
                })
        }
    }

    scrollMessageToBottom() {
        try {
            // console.log(this.scrollMessageContent.nativeElement.scrollTop + " - " + this.scrollMessageContent.nativeElement.scrollHeight);
            this.scrollMessageContent.nativeElement.scrollTop = this.scrollMessageContent.nativeElement.scrollHeight;
            // scrollMessageContent.scrollIntoView(false);
        } catch (err) { }
    }

    toggelSetting() {
        // console.log(this.settings);
    }

    saveAutomaticMessage() {
        let dataToSend = {
            auto_reply: (this.settings) ? '1' : '0',
            message: this.automaticMessage,
            user_id: JSON.parse(localStorage.getItem("currentUser")).id
        };

        this.sharedService.saveAutomaticMessageSetting(dataToSend).then(data => {
            if (data['success']) {
                this.toastr.success(data['message']);
                let userData = JSON.parse(localStorage.getItem("currentUser"));
                userData.auto_reply = dataToSend.auto_reply;
                userData.automatic_message = dataToSend.message;

                localStorage.setItem("currentUser", JSON.stringify(userData));
            }
            else {
                this.toastr.warning(data['message']);
            }
        })
            .catch(error => {
                this.toastr.error('Something went wrong, Please try again');
            })
    }

    handleFileInput(files: FileList) {
        this.fileToUpload = files.item(0);
        // console.log("file input has changed. The file is", this.fileToUpload);
    }

    add_to_favrate(favrate_ids) {
        this.favrate_res = true;
        // $("#favmark").hide(); 
        this.loading = true;
        this.sharedService.add_to_fav(favrate_ids).then(res => {
            // this.show_all_messages(favrate_ids,this.userfirst,this.userLast);
            this.get_all_fav_contact();
            this.loading = false;
        }).catch(error => {
            //    console.log("error value: " + error);     
        })
    }

    remove_from_favrate(favrate_ids) {
        // $("#unfavmark").hide(); 
        this.favrate_res = false;
        this.loading = true;
        this.sharedService.delete_from_fav(favrate_ids).then(res => {
            // this.show_all_messages(favrate_ids,this.userfirst,this.userLast);
            this.get_all_fav_contact();
            this.loading = false;
        }).catch(error => {
            //    console.log("error value: " + error);     
        })
    }

    onKeydownEvent(value) {
        // console.log(value);
        if (value) {
            this.userTextInput = true;
        }
        else {
            this.userTextInput = false;
        }
    }

    get_all_fav_contact() {
        this.sharedService.get_all_fav_contact().then(res => {
            this.favrate_user_list = res;
        }).catch(error => {
            //    console.log("error value: " + error);     
        })

        this.sharedService.get_all_contact().then(res => {
            this.all_contact_list = res;
        }).catch(error => {
            //    console.log("error value: " + error);     
        })
    }

    //------------delete message------------
    deletemessage(chat_id) {
        var box = confirm("Are you sure delete this message");
        if (box === true) {
            this.loading = true;
            this.sharedService.delete_mesasge(chat_id).then(res => {
                this.isDeleted = true;
                this.get_all_user();
                // if(chat_id == this.current_chat_id){
                //     this.user_message.length = 0;
                // }
                // this.show_all_messages(this.receiverId,this.userfirst,this.userLast);
                this.loading = false;
                this.toastr.success('Message successfully delete!');
            }).catch(error => {
                this.toastr.error('Error occured' + '!', 'Oops!');
            })
        }
    }
    //----------------delete all----------
    deleteAll(event) {
        if (event.target.checked == true) {
            var box = confirm("Are you sure delete All messages");
            if (box === true) {   // yes sure
                this.loading = true;
                this.sharedService.delete_allmesasge().then(res => {
                    this.isDeleted = true;
                    this.userfirst = '';
                    this.userLast = '';
                    this.receiverId = null;
                    this.user_message = null;
                    this.get_all_user();

                    // this.show_all_messages(this.receiverId,this.userfirst,this.userLast);
                    this.loading = false;
                    this.toastr.success('Messages successfully delete!');
                }).catch(error => {
                    this.loading = false;
                    this.toastr.error('Error occured' + '!', 'Oops!');
                })
                // event.target.checked == false;
            }
            else {
                // event.target.checked == false
                this.isDeleteChecked = false;
            }

        }

    }
    goBack() {
        this._location.back();
    }





}
