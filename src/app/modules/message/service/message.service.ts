import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Http, Response } from '@angular/http';
import { SharedService } from '../../../services/shared.service';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
declare var $:any;

@Injectable()
export class MessageService {

    headers:Headers;
    options:RequestOptions;
    public currentUser:any = JSON.parse(localStorage.getItem('currentUser'));

    constructor(
        public http:Http,
        public sharedService:SharedService) {

        // console.log('data service connected' + this.currentUser);

        this.headers = new Headers({'Content-Type': 'application/json'});
        this.options = new RequestOptions({headers: this.headers});
    }

    //login(body:Object):Promise<Object> {
    //    return this.http.post(this.sharedService.apiUrl + '/api/login', body, this.options).toPromise()
    //        .then(this.extractData)
    //        .catch(this.handleErrorPromise);
    //}
    //
    //forgot(user:Object):Promise<Object> {
    //    return this.http.post(this.sharedService.apiUrl + '/api/forgot', user, this.options).toPromise()
    //        .then(this.extractData)
    //        .catch(this.handleErrorPromise);
    //}
    //
    //logout():Promise<Object> {
    //    return this.http.get(this.sharedService.apiUrl + '/api/logout?token='+this.currentUser.token, this.options).toPromise()
    //        .then(this.extractData)
    //        .catch(this.handleErrorPromise);
    //}
    //
    //socialLogin(body:Object):Promise<Object> {
    //    console.log("social *********** :: " + body);
    //    return this.http.get(this.sharedService.apiUrl + '/api/social-login?service='+body+'&user=buyer', this.options).toPromise()
    //        .then(this.extractData)
    //        .catch(this.handleErrorPromise);
    //}



    private extractData(res:Response) {
        let body = res.json();
         if (body) {
            localStorage.setItem('currentUser', JSON.stringify(body));
        }
        return body || {};
    }

    // below function can be used when deal with Observabled
    private handleErrorPromise(error:Response | any) {
        console.error(error.message || error);
        return Promise.reject(error.message || error);
    }



    private handleErrorObservable(error:Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    }



}
