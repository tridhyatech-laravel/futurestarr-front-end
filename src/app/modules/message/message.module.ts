import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MessageComponent } from './message.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule} from '@angular/forms';
import {MessageService} from './service/message.service';
// import {StompRService} from '../../../stomp-r.service';
// import {StompService} from '../../../stomp.service';
import { LoadingModule } from 'ngx-loading';
import {HttpClientModule} from '@angular/common/http';
//Filter pipe
import { FilterPipe} from './filter.pipe';
//30-07---
import {FileUploadModule} from "ng2-file-upload";


@NgModule({
  declarations: [
      MessageComponent,
      FilterPipe
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    FormsModule,
    FileUploadModule,
    LoadingModule,
    HttpClientModule,
    RouterModule.forChild([
    {
      path: '',
      component: MessageComponent

    }
  ])
],
  providers: [MessageService, 
    // StompRService, StompService
  ],
  bootstrap: [MessageComponent]
})
export class MessageModule { }
