import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { RegisterService } from './service/register.service';
// import { SharedService } from '../../services/shared.service';
// import {AppService} from '../../../app/app.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';


declare var $: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  loading: boolean = false;
  public firstToggle: boolean = true;
  public secondToggle: boolean = false;
  public tabToggle: boolean = true;
  public user: Object = { first_name: "", last_name: "", username: "", email: "", password: "", repassword: "" };

  constructor(public router: Router,
    public registerService: RegisterService,
    public toastr: ToastrService,
    private _location: Location) {
  }

  ngOnInit() {

  }

  goBack() {
    this._location.back();
  }


  topFunction() {
    $('html, body').animate({ scrollTop: 0 }, 'fast');
  }

  buyerToggle() {
    //  console.log("buyer");
    this.firstToggle = true;
    this.secondToggle = false;
    this.tabToggle = true;
  }

  sellerToggle() {
    //console.log("seller");
    this.firstToggle = false;
    this.secondToggle = true;
    this.tabToggle = false;
  }


  submitUser(formData, user_type) {
    this.loading = true;
    // console.log(formData.value);
    // console.log(user_type);
    this.registerService.registerUser(user_type, formData.value).then(user1 => {
      this.loading = false;
      if (user1['success'] == false) {
        this.toastr.error(user1['message']);
      }
      else {
        formData.reset();
        this.toastr.success('We have sent an email. Please check your inbox!', 'Success!');
        this.router.navigate(['/register']);
      }
    }).catch(error => {
      this.loading = false;
      this.toastr.error(error._body + '!', 'Oops!');
      this.router.navigate(['/register']);
    });
  }


}
