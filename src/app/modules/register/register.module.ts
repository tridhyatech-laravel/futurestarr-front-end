import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RegisterComponent } from './register.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {RegisterService} from './service/register.service';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { LoadingModule } from 'ngx-loading';


@NgModule({
  declarations: [
    RegisterComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    FormsModule,
    LoadingModule,
    ReactiveFormsModule,
    RouterModule.forChild([
    {
      path: '',
      component: RegisterComponent

    }
  ])
],
providers: [RegisterService],

})
export class RegisterModule { }
