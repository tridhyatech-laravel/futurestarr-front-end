import { TestBed, inject } from '@angular/core/testing';

import { SocialBuzzService } from './social-buzz.service';

describe('SocialBuzzService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SocialBuzzService]
    });
  });

  it('should be created', inject([SocialBuzzService], (service: SocialBuzzService) => {
    expect(service).toBeTruthy();
  }));
});
