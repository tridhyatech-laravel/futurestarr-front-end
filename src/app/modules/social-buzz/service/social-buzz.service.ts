import { Injectable } from '@angular/core';
import { SharedService } from '../../../services/shared.service';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
declare var $: any;

@Injectable()
export class SocialBuzzService {

  headers: Headers;
  options: RequestOptions;
  public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
  public serverIp;
  constructor(public http: Http, public sharedService: SharedService) {
    this.serverIp = this.sharedService.apiUrl;
    this.headers = new Headers({ 'Content-Type': 'application/json' });
    this.options = new RequestOptions({ headers: this.headers });
  }

  addSocialBuzz(body):Promise<Object> {
    return this.http.post(this.sharedService.apiUrl + '/api/social-buzz?user_id='+this.currentUser.id, body,this.options).toPromise()
        .then(this.extractData)
        .catch(this.handleErrorPromise);
  }

  getSocialBuzz(category_id):Promise<Object> {
    return this.http.get(this.sharedService.apiUrl + '/api/social-buzzs?category_id='+category_id ,this.options).toPromise()
        .then(this.extractData)
        .catch(this.handleErrorPromise);
  }

  getCommercialAds(categoryId):Promise<object>{
    return this.http.get(this.sharedService.apiUrl + '/api/social-buzzs/commercial_ads/'+categoryId ,this.options).toPromise()
        .then(this.extractData)
        .catch(this.handleErrorPromise);
  }

  addSocialBuzzAwards(body):Promise<Object> {
    return this.http.post(this.sharedService.apiUrl + '/api/social-buzz/awards?user_id='+this.currentUser.id, body ,this.options).toPromise()
        .then(this.extractData)
        .catch(this.handleErrorPromise);
  }

  getSocialBuzzAwardDetails(id) {
    // console.log("called *****-")
    return this.http.get(this.sharedService.apiUrl + '/api/social-buzz/award-info?user_id='+this.currentUser.id+'&post_id='+id, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  getAllRidersdDetails(id) {
    // console.log("called *****-")
    return this.http.get(this.sharedService.apiUrl + '/api/social-buzz/rider-info?user_id='+this.currentUser.id+'&post_id='+id, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  addSocialBuzzRider(post_id):Promise<Object> {
    // console.log(body);
    return this.http.get(this.sharedService.apiUrl + '/api/social-buzz/add-rider?user_id='+this.currentUser.id+"&post_id="+post_id,this.options).toPromise()
        .then(this.extractData)
        .catch(this.handleErrorPromise);
  }

  postSocialBuzzReport(body) {
    // console.log("called *****-")
    return this.http.post(this.sharedService.apiUrl + '/api/social-buzz/report?user_id='+this.currentUser.id,body, this.options).toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);
  }

  addSocialBuzzComment(body):Promise<Object> {
    // console.log(body);
    return this.http.post(this.sharedService.apiUrl + '/api/social/buzz/post?user_id='+this.currentUser.id, body,this.options).toPromise()
        .then(this.extractData)
        .catch(this.handleErrorPromise);
  }

  getSocialBuzzComment(id):Promise<Object> {

    return this.http.get(this.sharedService.apiUrl + '/api/social/buzz/posts?user_id='+this.currentUser.id+'&post_id='+id,this.options).toPromise()
        .then(this.extractData)
        .catch(this.handleErrorPromise);
  }

  addPromoteProduct(id, body):Promise<Object> {
    // console.log("seller promote products service function called ******) " + this.currentUser);

     return this.http.post(this.sharedService.apiUrl + '/api/seller/promote-product/add?user_id='+this.currentUser.id+'&promote_id='+id,body, this.options).toPromise()
         .then(this.extractData)
         .catch(this.handleErrorPromise);

  }

  private handleError(error: any): Promise<any> {
    // console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  private extractData(res: Response) {
    let body = res.json();
    // console.log("below is the extracted data of categories ::" + JSON.stringify(body));
    // console.log(body);
    return body || {};
  }

  private handleErrorPromise(error: Response | any) {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
  }

}
