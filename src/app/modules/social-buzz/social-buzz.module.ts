import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SocialBuzzComponent } from './social-buzz.component';
import { RouterModule } from '@angular/router';
import { LoadingModule } from 'ngx-loading';
import { SharedModule } from '../shared/shared.module';
import { SocialBuzzService } from './service/social-buzz.service';
import { SharedService } from '../../services/shared.service';
import { FormsModule } from '@angular/forms';
import {FileUploadModule} from "ng2-file-upload";
import {ShareButtonsModule} from 'ngx-sharebuttons';
// import {EmojiPickerModule} from 'ng-emoji-picker';

// const customProp = {
//   facebook: {
//     icon: ['fab', 'fa-facebook-official'],
//     text: 'Share'
//   },
//   twitter: {
//     icon: ['fab', 'fa-twitter-square'],
//     text: 'Tweet'
//   },
//   // and so on...
// };

@NgModule({
  imports: [
    CommonModule,
    LoadingModule,
    SharedModule,
    FormsModule,
    FileUploadModule,
    ShareButtonsModule.forRoot(),
    RouterModule.forChild([
      {
        path: '',
        component: SocialBuzzComponent
      }
    ]),
    // EmojiPickerModule
  ],
  declarations: [SocialBuzzComponent],
  providers: [SocialBuzzService]
})
export class SocialBuzzModule { }
