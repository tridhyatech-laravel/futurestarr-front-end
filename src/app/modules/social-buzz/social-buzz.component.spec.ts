import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialBuzzComponent } from './social-buzz.component';

describe('SocialBuzzComponent', () => {
  let component: SocialBuzzComponent;
  let fixture: ComponentFixture<SocialBuzzComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocialBuzzComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialBuzzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
