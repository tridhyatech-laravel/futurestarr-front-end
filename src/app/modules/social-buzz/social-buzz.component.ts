import { Component, OnInit, PLATFORM_ID, Inject } from '@angular/core';
import { TrendingCategorySidebarComponent } from '../../components/trending-category-sidebar/trending-category-sidebar.component';
import { Location, isPlatformBrowser } from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { SocialBuzzService } from './service/social-buzz.service';
import { SharedService, image_path_url } from '../../services/shared.service';
import { BlogService } from './../blog/service/blog.service';
import { LoadingModule } from 'ngx-loading';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { FileUploader, FileLikeObject, FileItem, ParsedResponseHeaders } from 'ng2-file-upload';
import { Title, Meta } from '@angular/platform-browser';

declare var $: any;

@Component({
  selector: 'app-social-buzz',
  templateUrl: './social-buzz.component.html',
  styleUrls: ['./social-buzz.component.css']
})
export class SocialBuzzComponent implements OnInit {

  public showReplyInputBox = false;
  public category;
  public loading = false;
  public flag = false;
  public defaultCategory;
  public socilaBuzzValue: any = {};
  public categoryId;
  public buzzs: any = {};
  public awardsObj: any = {};
  public getAwardObj: any = {};
  public fanBaseUsersCount = 0;
  public userAssetResponse: any = {};
  public product_url;
  public reportObj: any = {};
  public shareObj: any = {};
  public posts: any = {};
  public postId;
  public postVals: any = {};
  public reply = [];
  public reply22 = [];
  public socialName;
  public promoteProducts: any = {};
  public socialId;
  public repoUrl;
  public image_path_urls: any;
  rider: any;
  allRiders: any = [];
  isAlreadyRider: boolean = false;
  commercial_ads: any;

  shareTags: any;
  shareTitle: any;
  shareUrl: any = this.sharedService.web_url;

  //public repoUrl = 'https://github.com/Epotignano/ng2-social-share';
  //public imageUrl = 'https://avatars2.githubusercontent.com/u/10674541?v=3&s=200';

  public errorMessage: string;
  public profileImage: string;
  public allowedImageType = ['image/png', 'image/gif', 'image/jpeg', 'image/jpg', 'video/mp4', 'video/mov'];
  public maxImageSize = 80 * 1024 * 1024; // 8 MB
  public imageUploader: FileUploader = new FileUploader({
    url: this.socialBuzzService.serverIp + '/api/social/buzz/product/image',
    itemAlias: 'product_img',
    method: 'POST',
    allowedMimeType: this.allowedImageType,
    maxFileSize: this.maxImageSize,
    autoUpload: false,
    removeAfterUpload: false,
    queueLimit: 1
  });

  public currentUser: any = JSON.parse(localStorage.getItem('currentUser'));
  public openPopup: Function;

  constructor(private _location: Location,
    private route: ActivatedRoute,
    public socialBuzzService: SocialBuzzService,
    public sharedService: SharedService,
    public blogService: BlogService,
    public toastr: ToastrService,
    private router: Router
    , private titleService: Title, public meta: Meta,
    @Inject(PLATFORM_ID) private platform: Object
  ) {
    this.setMetaTags();
    this.image_path_urls = image_path_url;
    this.topFunction();
    this.getDefaultCategory();
    this.getSocialBuzzInfo();

    // image uploader functions
    this.imageUploader.onWhenAddingFileFailed = (item, filter, options) => this.onUploadImageFailed(item, filter, options);
    this.imageUploader.onAfterAddingFile = (fileItem) => {
      fileItem.withCredentials = false;
      // console.log(fileItem.file.type);
      //this.uploadImages();
    };

    this.imageUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      //this.setLatestImage(response);
      this.setLatestImage();
      //console.log(JSON.stringify(response));
    };

  }

  setPopupAction(fn: any) {
    this.openPopup = fn;
    console.log(this.openPopup);
  }
  bindedVariable:any;

  filee(event) {
    // console.log(this.imageUploader.queue);
  }

  ngOnInit() {
    if (isPlatformBrowser(this.platform)) {
    $(document).on("scroll", function () {
      if ($(document).scrollTop() > 10) {
        document.getElementById("myBtn").style.display = "block";
      } else {
        document.getElementById("myBtn").style.display = "none";
      }
    });
  }

    this.repoUrl = window.location.href;

    this.imageUploader.onErrorItem = (item, response, status, headers) => this.onErrorItem(item, response, status, headers);
    this.imageUploader.onSuccessItem = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers);

    if (isPlatformBrowser(this.platform)) {
    var emojiEl = $('#Emojii').emojioneArea({
      pickerPosition:"Left"
    });
  }

  }

  setMetaTags() {
    this.titleService.setTitle("Future Star Social Buzz : What do you like to Promote?");
    this.meta.updateTag({ name: 'description', content: "Join Future Starr & Create Online Buzz of your Product Launch. Future Starr is the best platform for you, where we help you to launch a new product with maximum sales and make your own product launch a huge success." });
    this.meta.updateTag({ name: 'keywords', content: 'generate online buzz, product buzz, go viral, product launch strategy, launch a product' });
    
  }

  onSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    let data = JSON.parse(response); //success server response
    // console.log("data");
    // console.log(data);
    this.imageUploader.queue = [];
    this.loading = false;
    this.toastr.success('Post Added successfully', 'Success');
    this.getSocialBuzzInfo();

  }

  onErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any {
    let error = JSON.parse(response); //error server response
    // console.log("error");
    // console.log(error);
    this.loading = false;
    this.toastr.success(error.message);
  }

  goBack() {
    this._location.back();
  }

  topFunction() {
    if (isPlatformBrowser(this.platform)) 
    $('html, body').animate({ scrollTop: 0 }, 'fast');
  }

  isImage(new_product_img_path) {
    let images = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'GIF', 'gif'];
    if (new_product_img_path) {
      let fileExtension = new_product_img_path.replace(/^.*\./, '');
      if (images.includes(fileExtension)) {
        return true;
      }
    }
  }

  isVideo(new_product_img_path) {
    if (new_product_img_path) {
      let fileExtension = new_product_img_path.replace(/^.*\./, '');
      if (fileExtension == 'mp4' || fileExtension == 'MP4' || fileExtension == 'mov' || fileExtension == 'MOV') {
        return true;
      }
    }
  }

  openProductLink(ads) {
    if (this.currentUser == null) {
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();
    }
    else {
      if (ads && ads.product_id) {
        // console.log(ads)
        let dataToSend = {
          seller_plan_id: ads.seller_plan_id,
          plan_id: ads.plan_id,
          ad_id: ads.ad_id,
          user_id: this.sharedService.currentUser.id,
        }
        this.sharedService.insertAdsViews(dataToSend).then(data => {

        })
          .catch(error => {

          })
        let url = this.sharedService.web_url + "talent-mall/product-info/" + ads.product_id;
        window.open(url, "_blank");
      }
    }
  }


  // shareModal(){
  //   $('#shareModal').modal({backdrop: 'static'});
  // }


  callCategorySocialBuzz(e) {
    this.category = e;
    this.category = this.category.name;
    this.categoryId = e.id;
    this.getSocialBuzzInfo();
    this.getCommercialAds();
    // console.log('inside the social buzz function :::: ' +this.category.name);
  }

  getDefaultCategory() {
    this.loading = true;
    this.blogService.getDefaultCategory().then(res => {
      this.defaultCategory = res;
      this.category = this.defaultCategory.default_category.name;
      this.categoryId = this.defaultCategory.default_category.id;
      this.getSocialBuzzInfo();
      this.getCommercialAds();
      this.loading = false;
    }).catch(error => {
      // console.log("error value: " + error);
      this.loading = false;
    });
  }

  addSocialBuzzPost(formData, comment) {
    // console.log(comment);
    formData.value.comment = comment;
    // console.log(formData);
    this.toastr.clear();
    if (this.currentUser == null) {
      // console.log("not loggedin")
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();
    } else {
      // console.log('logged in users ');
      if (comment) {


        this.loading = true;
        this.socilaBuzzValue = formData.value;
        this.socilaBuzzValue.category_id = this.categoryId;
        //  console.log(this.socilaBuzzValue);
        this.socialBuzzService.addSocialBuzz(this.socilaBuzzValue).then(res => {
          this.loading = false;
          this.uploadImages(res);
          formData.reset();
          $('#Emojii').emojioneArea().data("emojioneArea").setText('');

          if (this.imageUploader.queue.length == 0) {
            this.getSocialBuzzInfo();
            this.toastr.success('Post Added successfully', 'Success');
          }
        }).catch(error => {
          // console.log("Recalling...")
          this.loading = false;
          this.toastr.error('Post not added', 'Error');
        });
      }
      else {
        this.toastr.warning("write somrthing in textbox")
      }
    }
  }

  clearAllToast() {
    setTimeout(() => {
      this.toastr.clear();
    }, 3000)
  }

  getCommercialAds() {
    this.commercial_ads = null;
    this.socialBuzzService.getCommercialAds(this.categoryId).then(data => {
      if (data['success']) {
        this.commercial_ads = data['commercial_ads'];
      }
      else {

      }
    }).catch(error => {

    })
  }

  getSocialBuzzInfo() {
    this.loading = true;
    this.socialBuzzService.getSocialBuzz(this.categoryId).then(res => {
      this.buzzs = res;
      this.buzzs = this.buzzs.buzz_post;
      //  console.log('social buzz detauls ' + JSON.stringify(this.buzzs));
      this.loading = false;
    }).catch(error => {
      this.loading = false;
      // console.log('error in social buzz detailed' + error);
    })
  }

  giveAwardModal(id) {
    // console.log("Checking User ...");
    if (this.currentUser != null) {
      if (isPlatformBrowser(this.platform)) 
      $('#giveAward').modal({ backdrop: 'static' });
      this.awardsObj.post_id = id;
      this.getSocialBuzzAwards();
      this.getSocialBuzzInfo();
    }
    else {
      // console.log("Please Login first!");
      if (isPlatformBrowser(this.platform)) 
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();
    }
  }

  awardSocialBuzzPost(flag) {
    // console.log(flag );
    this.awardsObj.award = flag;
    //  console.log(this.awardsObj);
    this.socialBuzzService.addSocialBuzzAwards(this.awardsObj).then(res => {
      if (isPlatformBrowser(this.platform)) 
      $('#giveAward').modal('toggle');
      this.getSocialBuzzAwards();
      this.getSocialBuzzInfo();
      this.clearAllToast();
      this.toastr.success('Award Added successfully', 'Success');
    }).catch(error => {
      // console.log("error value: " + error);
      this.toastr.error('Award not added', 'Error');
    });
  }

  getSocialBuzzAwards() {
    this.getAwardObj = this.awardsObj.post_id;
    this.socialBuzzService.getSocialBuzzAwardDetails(this.getAwardObj).then(res => {
      this.userAssetResponse = res;
      //  console.log(this.userAssetResponse);
      //  this.fanBaseUsersCount = this.userAssetResponse.fan_base.length;

    }).catch(error => {
      // console.log("error value: " + error);
    });
  }

  purchaseModal(id, product_link) {
    // console.log("purchases product ***** ..." + id + product_link);
    if (this.currentUser != null) {
      this.product_url = product_link;

      // $('#purchaseModal').modal({backdrop: 'static'});
      if (isPlatformBrowser(this.platform)) 
      $('#purchaseModal').modal('show');

    }
    else {
      // console.log("Please Login first!");
      if (isPlatformBrowser(this.platform)) 
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();
    }
  }

  goToPurchaseProduct() {
    if (isPlatformBrowser(this.platform)) 
    $('#purchaseModal').modal('hide');
    //  document.getElementById("url").target = "_blank";
  }

  reportUserModal(id) {
    // console.log("report product ***** ..." + id);
    if (this.currentUser != null) {
      this.reportObj.post_id = id;
      if (isPlatformBrowser(this.platform)) 
      $('#reportUser').modal({ backdrop: 'static' });
    }
    else {
      // console.log("Please Login first!");
      if (isPlatformBrowser(this.platform)) 
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();
    }
  }

  reportSocialBuzzPost(flag) {
    this.loading = true;
    // console.log(flag );
    this.reportObj.report = flag;
    //  console.log(this.reportObj);
    this.socialBuzzService.postSocialBuzzReport(this.reportObj).then(res => {
      if (isPlatformBrowser(this.platform)) 
      $('#reportUser').modal('toggle');
      this.getSocialBuzzAwards();
      this.toastr.success('report Added successfully', 'Success');
      this.getSocialBuzzInfo();
      this.loading = false;
    }).catch(error => {
      this.loading = false;
      // console.log("error value: " + error);
      this.toastr.error('Report not added', 'Error');
    });
  }

  ridePost() {
    this.socialBuzzService.addSocialBuzzRider(this.rider.social_buzz_id).then(data => {
      if (data['success']) {
        this.toastr.success(data['message']);
        if (isPlatformBrowser(this.platform)) 
        $('#riderModal').modal('hide');
        this.getSocialBuzzInfo();
      }
      else {
        this.toastr.error(data['message']);
      }
    })
      .catch(error => {

      });
  }

  riderModal(buzz) {
    if (this.currentUser != null) {
      if (this.allRiders.length)
        this.allRiders.length = 0;
      this.rider = buzz;
      this.getAllRiders();
      if (isPlatformBrowser(this.platform)) 
      $('#riderModal').modal({ backdrop: 'static' });
    }
    else {
      // console.log("Please Login first!");
      if (isPlatformBrowser(this.platform)) 
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();
    }
  }

  getAllRiders() {
    // console.log(this.rider);
    this.socialBuzzService.getAllRidersdDetails(this.rider.social_buzz_id).then(data => {
      if (data['success']) {
        this.allRiders = data['allRiders'];
        this.isAlreadyRider = data['isAlreadyRider'];
      }
      else {

      }
    })
      .catch(error => {

      })
  }

  shareModal(buzz) {
    // console.log(buzz); 

    if (this.currentUser != null) {
      if (buzz.product_link) {
        this.repoUrl = buzz.product_link;
        this.shareUrl = buzz.product_link;
        this.shareTitle = buzz.comment;
        this.shareTags = (buzz.comment).replace(" ", ",");

      }
      else {
        this.repoUrl = window.location.href;
      }
      this.shareObj.post_id = buzz.social_buzz_id;
      if (isPlatformBrowser(this.platform)) 
      $('#shareModal').modal({ backdrop: 'static' });

    }
    else {
      // console.log("Please Login first!");
      if (isPlatformBrowser(this.platform)) 
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();
    }
  }

  getSocialShareMediaName(name) {
    this.socialName = name;
    //  console.log('>>>>>>>>>>>>>>>>>>>>>>> ' + this.socialName);
  }

  shareOnSocialMedia(formData) {
    let id = this.socialId;
    // console.log(id);
    // this.loading = true;
    this.promoteProducts = formData.value;
    this.promoteProducts.social_name = this.socialName;
    // console.log("social buzzzzz:::::::::::  " + JSON.stringify(this.promoteProducts));
    //    this.socialBuzzService.addPromoteProduct(id, this.promoteProducts).then(res => {
    //     $('#socialMediaShare').modal('hide');
    //     this.loading = false;
    //     this.toastr.success('Shared product successfully!');
    //
    // }).catch(error => {
    //     //console.log("error value: " + error);
    //     this.toastr.error('Get error!');
    //     this.loading = false;
    // });
  }

  commentModal(id) {
    // console.log("comment product ***** ..." + id);
    if (this.currentUser != null) {
      this.postVals = null;
      this.postId = id;
      this.getSocialBuzzComment();
      if (isPlatformBrowser(this.platform)) 
      $('#comment').modal({ backdrop: 'static' });
    }
    else {
      // console.log("Please Login first!");
      if (isPlatformBrowser(this.platform)) 
      $('#askToLogin').modal({ backdrop: 'static' });
      this.setTimeOut();
    }
  }

  addSocialBuzzComment(formData) {
    this.toastr.clear();
    this.loading = true;
    this.posts = formData.value;
    this.posts.post_id = this.postId;
    // this.posts.reply = this.reply;
    // console.log(this.posts);

    //  console.log("*****" + JSON.stringify(this.posts));
    // this.getSocialBuzzComment();
    this.socialBuzzService.addSocialBuzzComment(this.posts).then(res => {
      this.loading = false;
      formData.reset();
      if (isPlatformBrowser(this.platform)) 
      $('#comment').modal('hide');
      this.getSocialBuzzInfo();

      // this.getSocialBuzzComment();
      this.toastr.success('Comment Added successfully', 'Success');
    }).catch(error => {
      // console.log("Recalling...")
      this.loading = false;
      this.toastr.error('Post not added', 'Error');
    });
  }

  toggle(id, i) {
    // console.log("toggle id " + id);
    if (id) {
      // console.log('***********');
      this.showReplyInputBox = id;
      if (i) {
        this.flag = true;
      }
    } else {
      this.flag = false;
      this.showReplyInputBox = false;
    }
  }

  // getSocialBuzzComment(){
  //   this.loading = true;
  //   console.log('show comments ...' + this.postId);
  //   this.socialBuzzService.getSocialBuzzComment(this.postId).then(res => {
  //     this.loading = false;
  //     this.postVals = res;
  //     this.postVals = this.postVals.post;
  //     console.log('coments >>>> ' + JSON.stringify(  this.postVals ));
  //     this.getSocialBuzzInfo();
  //   }).catch(error => {
  //     console.log("Recalling...")
  //     this.loading = false;
  //   });
  // }

  getSocialBuzzComment() {
    this.loading = true;

    this.socialBuzzService.getSocialBuzzComment(this.postId).then(res => {
      this.loading = false;
      // console.log(res['success']);
      if (res['success']) {
        this.postVals = res['post'];
        // this.postVals = this.postVals;
      }

      this.postVals.forEach(obj => {
        obj.reply = '';
      });

      this.getSocialBuzzInfo();
    }).catch(error => {
      this.loading = false;
    });
  }

  onSelect(val, index) {
    // console.log("calledeklllll");
    var obj = { comment_id: 0, reply: '' };
    //  console.log(val.comment_id);
    // console.log(val.reply);
    obj.comment_id = val.comment_id;
    obj.reply = val.reply;

    // console.log(obj);

    this.reply22[val.comment_id] = obj;

    //  console.log(this.reply22);

    // var chek = localStorage.setItem('rerer', JSON.stringify(this.reply22));
    //   console.log(localStorage.setItem('rerer', JSON.stringify(this.reply22)));

  }

  setTimeOut() {
    // console.log("set tim eout")
    var t = setTimeout(() => {
      if (isPlatformBrowser(this.platform)) 
      $('#askToLogin').modal('toggle');
      this.sharedService.announceAskToLogIn(window.location.href);
    }, 1000);
  }

  goToRegister() {
    this.sharedService.announcelogOut();
  }

  onUploadImageFailed(item: FileLikeObject, filter: any, options: any) {
    //console.log(item.type);
    switch (filter.name) {
      case 'fileSize':
        this.errorMessage = 'Maximum upload size 8 MB allowed';
        //  console.log(this.errorMessage);
        break;
      case 'mimeType':
        const allowedTypes = this.allowedImageType.join();
        this.errorMessage = 'Type ' + item.type + ' is not allowed. Allowed types:' + allowedTypes;
        //console.log(this.errorMessage);
        break;
      default:
        this.errorMessage = 'Unknown error filter is ' + filter.name;
      //    console.log(this.errorMessage);
    }
  }


  uploadImages(post_id) {

    this.imageUploader.onBuildItemForm = function (fileItem, form) {
      form.append('post_id', post_id);
      return { fileItem, form }
    };
    this.loading = true;
    this.imageUploader.uploadAll();

  }

  setLatestImage() {
    this.sharedService.announceImagePath();
  }

}
