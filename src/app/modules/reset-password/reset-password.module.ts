import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ResetPasswordComponent } from './reset-password.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {ResetPasswordService} from './service/reset-password.service';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    ResetPasswordComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
    {
      path: '',
      component: ResetPasswordComponent

    }
  ])
],
providers: [ResetPasswordService],

})
export class ResetPasswordModule { }
