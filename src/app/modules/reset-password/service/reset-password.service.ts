import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { SharedService } from '../../../services/shared.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
declare var $ :any;

@Injectable()
export class ResetPasswordService {

  headers: Headers;
  options: RequestOptions;

  constructor(public http:Http,public sharedService:SharedService) {
    // console.log('data service connected');
    this.headers      = new Headers({ 'Content-Type': 'application/json'});
    this.options       = new RequestOptions({ headers: this.headers });
  }

  validateURL(token:string): Promise<any> {
       return this.http.get(this.sharedService.apiUrl+'/api/validate_url/'+token, this.options).toPromise()
                        .then(this.extractData)
                        .catch(this.handleError);
   }

   resetPassword(user:Object): Promise<Object>{

      //  console.log("reset password :: " + JSON.stringify(user));

     return this.http.post(this.sharedService.apiUrl+'/api/reset_password',user, this.options).toPromise()
                      .then(this.extractData)
                      .catch(this.handleError);
   }

   private extractData(res: Response) {
        let body = res.json();
         // console.log("below is the extracted data");
          // console.log(body);
           return body || {};
       }

   private handleError(error: any): Promise<any> {
    // console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
