import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { SharedService } from '../../services/shared.service';
import { ToastrService } from 'ngx-toastr';
import {ResetPasswordService} from './service/reset-password.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  public user_info = {token:''};
  constructor(private activatedRoute: ActivatedRoute,public resetPasswordService:ResetPasswordService,public router: Router,public toastr:ToastrService) {

  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
        let token = params['token'];
        this.validate(token);
        // console.log(token);
      });
  }

  validate(token){
      this.resetPasswordService.validateURL(token).then(res =>{
        // console.log(res);
        }).catch(error =>{
        // console.log(error._body);
         this.toastr.error(error._body+'!', 'Oops!');
         this.router.navigate(['']);
      });
  }

  reset(formData){
    this.user_info = formData.value;
    this.activatedRoute.params.subscribe((params: Params) => {
        let token = params['token'];
        this.user_info.token = token;
      });

      //console.log(this.user_info);
      this.resetPasswordService.resetPassword(this.user_info).then(res =>{
      this.toastr.success(res+'!', 'Success!');
      //this.router.navigate(['']);
           this.router.navigate(['/']);
    }).catch(error =>{
      this.toastr.error(error._body+'!', 'Oops!');
     // this.router.navigate(['']);
           this.router.navigate(['/']);

    });

  }


}
