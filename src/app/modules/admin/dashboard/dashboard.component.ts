import { Component, OnInit } from '@angular/core';
declare var $:any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  blurToggle:boolean = false;
  constructor() { }

  ngOnInit() {
  }

toggleMenu(){
  $("#sidebar-wrapper").toggleClass("active");
  if(this.blurToggle == true){
    this.blurToggle = false;
    return;
  }
  if(this.blurToggle == false){
    this.blurToggle = true;
    return;
  }

  }
}
