import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { PLATFORM_ID, APP_ID, Inject } from '@angular/core';
import { isPlatformBrowser } from '@angular/common'; 
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LayoutComponent } from './components/layout/layout.component';
// import { LoginComponent } from './modules/login/login.component';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
// import { CustomOption } from './custom-option';
import { SharedService } from './services/shared.service';
import { LoginService } from './modules/login/service/login.service';
import { EmailVerificationComponent } from './components/email-verification/email-verification.component';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { BuyerLayoutComponent } from './components/buyer-layout/buyer-layout.component';
import { FileDropDirective, FileSelectDirective } from 'ng2-file-upload';
import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { SellerLayoutComponent } from './components/seller-layout/seller-layout.component';

import { SellerSidebarComponent } from './components/seller-sidebar/seller-sidebar.component';
import { BuyerSidebarComponent } from './components/buyer-sidebar/buyer-sidebar.component';
import { AdminSidebarComponent } from './components/admin-sidebar/admin-sidebar.component';
import { AdminLayoutComponent } from './components/admin-layout/admin-layout.component';
import { DownloadComponent } from './modules/seller/download/download.component';
import { AwardComponent } from './modules/seller/award/award.component';
import { ManageProductComponent } from './modules/seller/manage-product/manage-product.component';

import { SellerSidebarService } from './components/seller-sidebar/services/seller-sidebar.service';
import { CommercialAdsService } from './modules/seller/commercial-ads/service/commercial-ads.service';
import { MyProductService } from './modules/seller/my-product/service/my-product.service';
import { PromoteProductService } from './modules/seller/promote-product/service/promote-product.service';
import { AccountSettingService } from './modules/seller/account-setting/service/account-setting.service';
import { AddProductService } from './modules/seller/add-product/service/add-product.service';
import { ChangePasswordService } from './modules/seller/change-password/service/change-password.service';
import { NotificationService } from './modules/seller/notification/service/notification.service';
import { MessageService } from './modules/message/service/message.service';
import { AuthGuardService } from './components/guard/auth-guard.service';
import { SellerguardGuard } from './components/sellerguard/sellerguard.guard';
import { AuthGuardUserService } from './components/auth-guard-user/auth-guard-user.service';

//LogIn Library
import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider, LinkedinLoginProvider } from 'ng-social-login-module';
// import { StarSearchComponent } from './modules/star-search/star-search.component';
// import { AboutCategoryComponent } from './modules/star-search/about-category/about-category.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { NguiMapModule } from '@ngui/map';
import { SellerMessageSidebarComponent } from './components/buyer-message-sidebar/buyer-message-sidebar.component';
import { UserCartComponent } from './components/user-cart/user-cart.component';
import { LoadingModule } from 'ngx-loading';
import { StarSearchService } from './modules/star-search/service/star-search.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { BlogService } from './modules/blog/service/blog.service';
import { TrendingCategorySidebarService } from './components/trending-category-sidebar/service/trending-category-sidebar.service';
import { SharedModule } from './modules/shared/shared.module';
import { ThankYouComponent } from './components/user-cart/thank-you/thank-you.component';
import { ErrorComponent } from './components/user-cart/error/error.component';
import { ThankyoucontactComponent } from './components/thankyoucontact/thankyoucontact.component';
// import { StompConfig, StompService } from '@stomp/ng2-stompjs';
import * as SockJS from 'sockjs-client';
import { PrivacyPolicyComponent } from './components//privacy-policy/privacy-policy.component';
import { RefundPolicyComponent } from './components/refund-policy/refund-policy.component';
import { TermConditionComponent } from './components/term-condition/term-condition.component';
// import { ProfileLayoutComponent } from './components/profile-layout/profile-layout.component';
import { PublicProfileComponent } from './components/public-profile/public-profile.component';
import { ShareButtonsModule } from 'ngx-sharebuttons';
import {ShareButtonModule} from '@ngx-share/button';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';

import { faFacebookF } from '@fortawesome/free-brands-svg-icons/faFacebookF';
import { faTwitter } from '@fortawesome/free-brands-svg-icons/faTwitter';
import { faRedditAlien } from '@fortawesome/free-brands-svg-icons/faRedditAlien';
import { faLinkedinIn } from '@fortawesome/free-brands-svg-icons/faLinkedinIn';
import { faGooglePlusG } from '@fortawesome/free-brands-svg-icons/faGooglePlusG';
import { faTumblr } from '@fortawesome/free-brands-svg-icons/faTumblr';
import { faPinterestP } from '@fortawesome/free-brands-svg-icons/faPinterestP';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons/faWhatsapp';
import { faVk } from '@fortawesome/free-brands-svg-icons/faVk';
import { faFacebookMessenger } from '@fortawesome/free-brands-svg-icons/faFacebookMessenger';
import { faTelegramPlane } from '@fortawesome/free-brands-svg-icons/faTelegramPlane';
import { faStumbleupon } from '@fortawesome/free-brands-svg-icons/faStumbleupon';
import { faXing } from '@fortawesome/free-brands-svg-icons/faXing';

import { faCommentAlt } from '@fortawesome/free-solid-svg-icons/faCommentAlt';
import { faMinus } from '@fortawesome/free-solid-svg-icons/faMinus';
import { faEllipsisH } from '@fortawesome/free-solid-svg-icons/faEllipsisH';
import { faLink } from '@fortawesome/free-solid-svg-icons/faLink';
import { faExclamation } from '@fortawesome/free-solid-svg-icons/faExclamation';
import { faPrint } from '@fortawesome/free-solid-svg-icons/faPrint';
import { faCheck } from '@fortawesome/free-solid-svg-icons/faCheck';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons/faEnvelope';
// import {EmojiPickerModule} from 'ng-emoji-picker';
import { environment } from '../environments/environment.prod';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';


const icons = [
  faFacebookF, faTwitter, faLinkedinIn, faGooglePlusG, faPinterestP, faRedditAlien, faTumblr,
  faWhatsapp, faVk, faFacebookMessenger, faTelegramPlane, faStumbleupon, faXing, faCommentAlt,
  faEnvelope, faCheck, faPrint, faExclamation, faLink, faEllipsisH, faMinus
];

library.add(...icons);

import { NotFoundComponent } from './components/not-found/not-found.component';

import { LoginModule } from './modules/login/login.module';
//social media configuration

const CONFIG = new AuthServiceConfig([
  { 
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('251657474771-u4pggqdod5l540t535l5li7hdr24455e.apps.googleusercontent.com')
    // test provider: new GoogleLoginProvider('851657962144-0r2phimsmjhmt4nopktiovscmholkt6t.apps.googleusercontent.com')

  },
  {
    id: LinkedinLoginProvider.PROVIDER_ID,
    provider: new LinkedinLoginProvider('anxs83ga2ys8')
    // test provider: new LinkedinLoginProvider('81wa9jz5xtc6mn') test
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    // test provider: new FacebookLoginProvider('1742851089293151')
    provider: new FacebookLoginProvider('628081704233410')
                                         
  }

], false);

export function provideConfig() {
  return CONFIG;
}

//for chat module

// const stompConfig: StompConfig = {
//   // Which server?
//   url: 'ws://127.0.0.1:4200/ws',

//   // Headers
//   // Typical keys: login, passcode, host
//   headers: {
//     login: 'guest',
//     passcode: 'guest'
//   },

//   // How often to heartbeat?
//   // Interval in milliseconds, set to 0 to disable
//   heartbeat_in: 0, // Typical value 0 - disabled
//   heartbeat_out: 20000, // Typical value 20000 - every 20 seconds
//   // Wait in milliseconds before attempting auto reconnect
//   // Set to 0 to disable
//   // Typical value 5000 (5 seconds)
//   reconnect_delay: 5000,

//   // Will log diagnostics on console
//   debug: true
// };

const appRoutes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    loadChildren: './modules/home/home.module#HomeModule',
    data: {
      title: 'Future Starr | Talent Marketplace | Sell your Talent Online',
      description: 'Future Starr promotes promising talent from around the world. Our platform helps you in becoming a famous celebrity. Only requirement is; just upload your talent videos on Future Starr and showcase your talent to the world. Sell your Talent Free - Make Sales and Be your Own Boss!',
      keywords: 'advertise yourself, future rising stars, entertainment careers, online portfolios, online talent marketplace',
    }
  }, 
  {
    path: 'register',
    component: LayoutComponent,
    loadChildren: './modules/register/register.module#RegisterModule',
    data: { title: 'Register' }
  },
  {
    path: 'reset_password/:token',
    component: LayoutComponent,
    loadChildren: './modules/reset-password/reset-password.module#ResetPasswordModule'
  },
  {
    path: 'message',
    component: LayoutComponent,
    loadChildren: './modules/message/message.module#MessageModule'
  },
  {
    path: 'talent-mall',
    component: LayoutComponent,
    loadChildren: './modules/talent-mall/talent-mall.module#TalentMallModule',
    data: {
      title: 'Future Starr Talent Mall: Browse and Purchase Talent',
      description: 'Sign Up with Future Starr Today! We will help you to get started. Whatever you are espired in, we will give you inspiration to fulfill your respiration.',
      keywords: "Rising Star, future star talent show, purchase talent, talent browse, explore talent, america's got talent",
    }
  },
  {
    path: 'star-search',
    component: LayoutComponent,
    loadChildren: './modules/star-search/star-search.module#StarSearchModule',
  },
  {
    path: 'contact-us',
    component: ContactUsComponent,
    // loadChildren: ContactUsComponent,
  },
  {
    path: 'cart',
    component: UserCartComponent,
    canActivate: [AuthGuardUserService],
    // loadChildren: UserCartComponent
  },
  {
    path: 'cart/thankyou',
    component: ThankYouComponent,
     canActivate: [AuthGuardUserService],
    // loadChildren: ThankYouComponent
  },
  {
    path: 'thank-you',
    component: ThankyoucontactComponent,
    
    // loadChildren: ThankYouComponent
  },
  {
    path: 'cart/error',
    component: ErrorComponent,
    canActivate: [AuthGuardUserService],
    // loadChildren: ErrorComponent
  },
  {
    path: 'social-buzz',
    component: LayoutComponent,
    loadChildren: './modules/social-buzz/social-buzz.module#SocialBuzzModule',
    data: {
      title: 'Future Star Social Buzz : What do you like to Promote?',
      description: 'Join Future Starr & Create Online Buzz of your Product Launch. Future Starr is the best platform for you, where we help you to launch a new product with maximum sales and make your own product launch a huge success.',
      keywords: "generate online buzz, product buzz, go viral, product launch strategy, launch a product",
    }
  },
  {
    path: 'blog',
    component: LayoutComponent,
    loadChildren: './modules/blog/blog.module#BlogModule',
    data: {
      title: 'Official Future Starr Blog | A Global Talent Platform ',
      description: "FutureStarr.com - Create your Unique Blogs and it's pretty easy. For targeted traffic Contact our Unique Talent Marketplace at Future Starr.",
      keywords: "",
    }
  },
  {
    path: 'privacy-policy',
    component: PrivacyPolicyComponent,
    // loadChildren: PrivacyPolicyComponent
  },
  {
    path: 'refund-policy',
    component: RefundPolicyComponent,
    // loadChildren: RefundPolicyComponent
  },
  {
    path: 'term-conditions',
    component: TermConditionComponent,
    // loadChildren: TermConditionComponent
  },
  {
    path: 'users-profile/:user_id',
    component: PublicProfileComponent,
    // loadChildren: PublicProfileComponent
  },
  {
    path: 'blog/detailed/:id',
    component: LayoutComponent,
    loadChildren: './modules/blog/blog-detailed/blog-detailed.module#BlogDetailedModule'
  },    
  {
    path: 'verify_user/:token',
    component: EmailVerificationComponent
  },
  {
    path: 'seller',
    canActivate: [SellerguardGuard],
    component: SellerLayoutComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: './modules/seller/dashboard/dashboard.module#DashBoardModule'
      },
      {
        path: 'add-product',
        loadChildren: './modules/seller/add-product/add-product.module#AddProductModule'
      },
      {
        path: 'promote-product',
        loadChildren: './modules/seller/promote-product/promote-product.module#PromoteProductModule'
      },
      {
        path: 'account-setting',
        loadChildren: './modules/seller/account-setting/account-setting.module#AccountSettingModule'
      },
      {
        path: 'commercial-ads',
        loadChildren: './modules/seller/commercial-ads/commercial-ads.module#CommercialAdsModule'
      },
      {
        path: 'commercial-ad-dashboard',
        loadChildren: './modules/seller/commercial-add-dashboard/commercial-add-dashboard.module#CommercialAddDashboardModule'
      },
      {
        path: 'my-product',
        loadChildren: './modules/seller/my-product/my-product.module#MyProductModule'
      },
      {
        path: 'sale',
        loadChildren: './modules/seller/sale/sale.module#SaleModule'
      },
      {
        path: 'change-password',
        loadChildren: './modules/seller/change-password/change-password.module#ChangePasswordModule'
      },
      {
        path: 'notification',
        loadChildren: './modules/seller/notification/notification.module#NotificationModule'
      }
    ],

  },
    //
  {
    path: 'buyer',
    canActivate: [AuthGuardService],
    component: BuyerLayoutComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: './modules/buyer/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'account-setting',
        loadChildren: './modules/buyer/buyer-profile/buyer-profile.module#BuyerProfileModule'
      },
      {
        path: 'change-password-buyer',
        loadChildren: './modules/buyer/change-password/change-password.module#ChangePasswordBuyerModule'
      }
    ]
  },
    //
  {
    path: 'admin',
    component: AdminLayoutComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: './modules/admin/dashboard/dashboard.module#DashBoardModule'
      }
    ]
  },
  { 
    path: '**', 
    component: NotFoundComponent ,
  }
  
];



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LayoutComponent,
    // LoginComponent,
    EmailVerificationComponent,
    AdminDashboardComponent,
    SellerLayoutComponent,
    BuyerLayoutComponent,
    SellerSidebarComponent,
    BuyerSidebarComponent,
    AdminSidebarComponent,
    AdminLayoutComponent,
    DownloadComponent,
    AwardComponent,
    ManageProductComponent,
    // StarSearchComponent,
    // AboutCategoryComponent,
    ContactUsComponent,
    SellerMessageSidebarComponent,
    UserCartComponent,
    ThankYouComponent,
    ThankyoucontactComponent,
    ErrorComponent,
    PrivacyPolicyComponent,
    RefundPolicyComponent,
    TermConditionComponent,
    PublicProfileComponent,
    // ProfileLayoutComponent,
    NotFoundComponent
  ],
  imports: [
    SharedModule,
    BrowserModule.withServerTransition({appId:"future2018"}),
    // BrowserTransferStateModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
    HttpModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    SocialLoginModule,
    LoginModule,
    AngularFireModule.initializeApp(environment.firebase, 'angular-auth-firebase'),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    ShareButtonModule,
    FontAwesomeModule,
    FileUploadModule,
    // EmojiPickerModule,
    ShareButtonsModule.forRoot(),
    LoadingModule.forRoot({
      primaryColour: '#ff503f',
      secondaryColour: '#ff503f',
      tertiaryColour: '#ff503f'
    }),
    ToastrModule.forRoot(),
    // RouterModule.forRoot(appRoutes),
    RouterModule.forRoot(appRoutes),


    // AgmCoreModule.forRoot({
    //   apiKey: 'AIzaSyDnZ9Heyt0iqem4jcgONdVPpa77B07IOQ0'
    // })
    NguiMapModule.forRoot({ apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyDnZ9Heyt0iqem4jcgONdVPpa77B07IOQ0' })
  ],
  providers: [
    { provide: AuthServiceConfig, useFactory: provideConfig },

    SharedService, AuthGuardService, AuthGuardUserService, LoginService, SellerSidebarService, MessageService, CommercialAdsService, MyProductService, PromoteProductService, AccountSettingService, AddProductService,
    NotificationService, StarSearchService, BlogService, TrendingCategorySidebarService, SellerguardGuard,
    // { provide: ToastOptions, useClass: CustomOption },
    //  StompService,
    // {
    //   provide: StompConfig,
    //   useValue: stompConfig
    // }
  ],
  bootstrap: [AppComponent]
  
})
export class AppModule {
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string) {
    const platform = isPlatformBrowser(platformId) ?
      'in the browser' : 'on the server';
    console.log(`Running ${platform} with appId=${appId}`);
  }
}
