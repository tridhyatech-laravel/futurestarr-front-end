import { FuturePage } from './app.po';

describe('future App', () => {
  let page: FuturePage;

  beforeEach(() => {
    page = new FuturePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
