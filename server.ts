// These are important and needed before anything else
import 'zone.js/dist/zone-node';
import 'reflect-metadata';
import 'localstorage-polyfill'
 
import { enableProdMode } from '@angular/core';
 
// import * as express from 'express';
import { join } from 'path';
import * as fs from 'fs';
import * as https from 'https';
import * as http from 'http';
// var compression = require('compression')
global['XMLHttpRequest']  = require('xmlhttprequest').XMLHttpRequest;

// import * as compression from 'compression';
 
var options = {
  key: fs.readFileSync('/etc/apache2/cert-files/futurestarr.key'),
  cert : fs.readFileSync('/etc/apache2/cert-files/87ef15d272053dd8.crt')
}
// Faster server renders w/ Prod mode (dev mode never needed)
enableProdMode();
 
// // Express server
// const app = express()
// var compression = require('compression')

// // var compression = require('compression')
// // var express = require('express')
// // // var router = express.Router();
// // var app = express()

// app.use(compression())

var compression = require('compression')
var express = require('express')
var router = express.Router();
var app = express()
 
// compress all requests 
app.use(compression())

app.all(/.*/, (req, res, next)=>{
  var host = req.header("host");
  if (host.match(/^www\..*/i)) {
    next();
    // res.redirect(301, "https://" + host + req.url);
  } else {
    res.redirect(301, "https://www." + host + req.url);
  }
})

https.createServer(options, app).listen(443);

http.createServer((req,res)=>{
 
  /*if ( req.headers.host.search(/^www/) !== 0) { 
 res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
 else  
 res.writeHead(301, { "Location": "https://www." + req.headers['host'] + req.url });*/
 res.writeHead(301, { "Location": "https://www.futurestarr.com"});

  res.end();
}).listen(80);

// http.createServer((req,res)=>{
//   res.writeHead(301, { "Location": "https://www." + req.headers['host'] + req.url });
//   res.end();
// }).listen(80);
 
const PORT = process.env.PORT || 443;
const DIST_FOLDER = join(process.cwd(), 'dist');
global['localStorage'] = localStorage;
 
// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const { AppServerModuleNgFactory, LAZY_MODULE_MAP } = require('./dist/server/main');
 
// Express Engine
import { ngExpressEngine } from '@nguniversal/express-engine';
// Import module map for lazy loading
import { provideModuleMap } from '@nguniversal/module-map-ngfactory-loader';
 
app.engine('html', ngExpressEngine({
  bootstrap: AppServerModuleNgFactory,
  providers: [
    provideModuleMap(LAZY_MODULE_MAP)
  ]
}));
 
app.set('view engine', 'html');
app.set('views', join(DIST_FOLDER, 'browser'));
 
// TODO: implement data requests securely
app.get('/api/*', (req, res) => {
  res.status(404).send('data requests are not supported');
});
 
// Server static files from /browser
app.get('*.*', express.static(join(DIST_FOLDER, 'browser')));
 
// All regular routes use the Universal engine
app.get('*', (req, res) => {
  res.render('index', { req });
});
 
// Start up the Node server
// app.listen(PORT, () => {
//   console.log(`Node server listening on http://localhost:${PORT}`);
// });